<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">

		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">

		<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

		<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
		<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>



	</head>

	
	<body class="default-header-p">

	
 
		<!-- backtotop - start -->
		<div id="thetop" class="thetop"></div>
		<div class='backtotop'>
			<a href="#thetop" class='scroll'>
				<i class="fas fa-angle-double-up"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		<div id="preloader"></div>
		<!-- preloader - end -->





		<!-- header-section - start
		================================================== -->
		               <header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
            
            <!-- header-bottom - start -->
            <div class="header-bottom" >
                <div class="container" >
                    <div class="row" style="margin-right: : -1px; margin-left: -1px;">

                        <!-- site-logo-wrapper - start -->
                        <div class="col-lg-3" style="padding-left: 0px;">
                            <div class="site-logo-wrapper">
                                <a href="index-1.html" class="logo">
                                    <h3  class="title" style="color: white; margin-top: 8px;">
                                        KOTAK EVENTS

                                    </h3>
                                    {{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
                                </a>
                            </div>
                        </div>
                        <!-- site-logo-wrapper - end -->

                        <!-- mainmenu-wrapper - start -->
                        <div class="col-lg-9">
                            <div class="mainmenu-wrapper">
                                <div class="row">                                                       
                                    <!-- menu-item-list - start -->
                                    <div class="col-lg-12" style="padding-right: 0px; float: right;">
                                        <div class="menu-item-list ul-li clearfix">
                                            <ul>
                                                <li >
                                                    <a href="/">Home</a>
                                                </li>
                                                <li >
                                                    <a href="/cari">Cari Event</a>
                                                </li>
                                                <li>
                                                    <a href="/buat-acara">Buat Event</a>
                                                </li>
                                                <li class="active"><a href="/acara-saya">Event</a></li>
                                                <li><a <a href="/undangan-saya">Undangan</a></li>
                                                
                                                <li class="menu-item-has-children">
                                                    
                                                    <a href="" style="color: white;"><i style="color: white;" class="fas fa-user"></i> {{Auth::user()->name}}</a>
                                                    <ul class="sub-menu">
                                                        
                                                        <li>
                                                            <a  style="" href="{{    route('logout') }}"
                                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">{{('Logout')}}
                                                            
                                                        </a>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>

                                                    </li>
                                                        
                                                    </ul>                                               
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                    <!-- menu-item-list - end -->                                   
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <!-- mainmenu-wrapper - end -->

                    </div>
                </div>
            </div>
            <!-- header-bottom - end -->
        </header>

		
		
		<!-- header-section - end
		================================================== -->
		<!-- altranative-header - start
		================================================== -->
		@include('layouts.altranative-header')
		<!-- altranative-header - end
		================================================== -->

		
		<style>
			.dtr-data{
				display: inline-block;
				float: right;
			}
		</style>
		<!-- cari-event-section - start
		================================================== -->
		<!-- booking-section - start
		================================================== -->
<section id="main" class="wrapper " style="padding-top: 2em;">
	<div class="container">                            
                <a href="{{ url('/')}}"><p>Home ></p></a>
                <a href="/acara-saya"><p>Event ></p></a>
                <a href="/listhadir/{{$acara->id}}"><p>Daftar Hadir {{$acara->nama}}</p></a>
            
            </div>  
				<div class="container">
					<header class="major special">
						<h2 align="center" style="font-family: comic sans; text-transform: uppercase;">Daftar Hadir Pada Event</h2>
						<h2 align="center" style="font-family: comic sans; text-transform: uppercase;">{{$acara->nama}}</h2>
					</header>
					<div class="table-responsive">
						<table id="example" class="table table-striped table-bordered" style="width:100%">
							<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Alamat</th>
								<th>Waktu Hadir</th>
								
							</tr>
							</thead>
							<tbody>
							@foreach($events as $event)
							<tr align="center">
								<td></td>
								<td>
									@if($event->name=='Admin')
										{{$event->nama_tamu}}
									@else
										{{$event->name}}										
									@endif
									
								</td>
								<td>
									@if($event->name=='Admin')
										{{$event->alamat_tamu}}
									@else
										{{$event->alamat}}										
									@endif
								</td>
								<td>{{$event->waktu_hadir}}</td>
								
					
							</tr>
							@endforeach
							</tbody>
							<tfoot>
							<tr>
								<th>No</th>
								<th>Nama</th>					
								<th>Alamat</th>
								<th>Waktu Hadir</th>
							</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</section>
		<!-- booking-section - end
		================================================== -->
		<br><br><br>
		
<script src="{{asset('assets/assets/js/atc.min.js')}}"></script>

		<!-- others jquery include -->
		<script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
		
		
		<script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
	

		<!-- custom jquery include -->
		<script src="{{asset('assets/assets/js/custom.js')}}"></script>

		

		<script>
			$(document).ready(function() {
				var t = $('#example').DataTable( {
					"columnDefs": [ {
						"searchable": false,
						"orderable": false,
						"targets": 0
					} ],
					"order": [[ 1, 'asc' ]],
					responsive: true
				} );
				t.on( 'order.dt search.dt', function () {
					t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
						cell.innerHTML = i+1;
					} );
				} ).draw();
				
			} );
		</script>
		
		<script>
			
			$(document).ready(function() {
				$('#example').DataTable();
			} );
			
			function tolak(id){
				$.get("{{route('tolak.pengajuan.saya',['id'=>''])}}/"+id, function(data, status){
				});
			}
			function konfirmasi(id){
				$.get("{{route('konfirmasi.pengajuan.saya',['id'=>''])}}/"+id, function(data, status){
				});
				window.location.reload(true);
			}
		</script>





	</body>
</html>