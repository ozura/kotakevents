<!DOCTYPE HTML>
<!--
	Retrospect by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		{{-- <title>Invit - Bengkulu</title> --}}
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<!--[if lte IE 8]><script src="{{asset('assets/js/ie/html5shiv.js')}}"></script><![endif]-->
		<link rel="stylesheet" href="{{asset('assets/css/main.css')}}" />
		<!--[if lte IE 8]><link rel="stylesheet" href="{{asset('assets/css/ie8.css')}}" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="{{asset('assets/css/ie9.css')}}" /><![endif]-->
		<link rel="stylesheet" href="{{asset('asset/css/login-register.css')}}">
	</head>
	<body>

		@include('layouts.nav')

		<!-- Main -->
			<section id="main" class="wrapper">
				<div class="container">
                    @if ($message = Session::get('message'))
                        <span style="background-color: red"><strong>Kode verifikas salah</strong></span>
                    @endif
                    <p>kode verifikasi telah dikirim ke email anda</p>
					<h3>Masukkan Kode Verifikasi</h3>
					<form method="post" action="{{route('verif.email')}}">
                        {{csrf_field()}}
						<div class="row uniform 50%">
							<div class="6u 12u$(xsmall)">
								<input type="text" name="kode_verif" id="name" value="" minlength="6" maxlength="6" placeholder="Kode Verifikasi" required />
							</div>
							<div class="2u$ 12u$(xsmall)">
								<ul class="actions">
									<li><input type="submit" value="Submit" class="special" /></li>
								</ul>
							</div>
						</div>
					</form>
				</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<div class="inner">
					<ul class="icons">
						<li><a href="#" class="icon fa-facebook">
							<span class="label">Facebook</span>
						</a></li>
						<li><a href="#" class="icon fa-twitter">
							<span class="label">Twitter</span>
						</a></li>
						<li><a href="#" class="icon fa-instagram">
							<span class="label">Instagram</span>
						</a></li>
						<li><a href="#" class="icon fa-linkedin">
							<span class="label">LinkedIn</span>
						</a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled.</li>
						<li>Images: <a href="http://unsplash.com">Unsplash</a>.</li>
						<li>Design: <a href="http://templated.co">TEMPLATED</a>.</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
		<script src="{{asset('assets/js/jquery.min.js')}}"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script src="{{asset('assets/js/skel.min.js')}}"></script>
		<script src="{{asset('assets/js/util.js')}}"></script>
		<!--[if lte IE 8]><script src="{{asset('assets/js/ie/respond.min.js')}}"></script><![endif]-->
		<script src="{{asset('assets/js/main.js')}}"></script>
		<script src="{{asset('assets/js/login-register.js')}}"></script>

	</body>
</html>