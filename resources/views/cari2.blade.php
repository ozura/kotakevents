<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/stylenav.css')}}"> --}}
        <link rel="stylesheet" type="text/css" href="https://kotakevents.com/css/styleB.css">
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/bulma-quickview.min.css')}}"> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/bulma.css')}}"> --}}
		{{-- <link rel="stylesheet" href="{{asset('assets/css/box.css')}}" /> --}}
		

		<!-- fraimwork - css include -->
        <link rel="stylesheet" href="{{asset('assets/css/login-register.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/flaticon.css')}}"> --}}



		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">


		

		<!-- custom css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">
	


		
		<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
		{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> --}}
		<link rel="stylesheet" type="text/css" href="{{asset('css/tabel.min.css')}}">
		
		{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"> --}}
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('css/hover.dataTables.min.css')}}"> --}}




		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

		{{-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> --}}
		
		<script type="text/javascript" src="{{asset('css/jqueryDatatable2.min.js')}}"></script>
		<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
		<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
		<link type="text/css" rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}">
        {{-- <link type="text/css" rel="stylesheet" href="{{asset('css/bulma.css')}}"> --}}

        <script type="text/javascript" src="{{asset('css/sweetalert2.min.js')}}"></script>
		



	</head>

	
	<body>

	
	
 
		<!-- backtotop - start -->
		<div id="thetop" class="thetop"></div>
		<div class='backtotop'>
			<a href="#thetop" class='scroll'>
				<i class="fas fa-angle-double-up"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		<div id="preloader"></div>
		<!-- preloader - end -->

        <style>
            .modal-open {
                
                overflow: scroll;
                width: 100%;
                padding-right: 0!important;
            }
        </style>
            <style scoped>
        @media (max-width:400px){
            .login .modal-dialog{
                width: 95%;

            }
        }
        </style>
		<!-- header-section - start
		================================================== -->
		<header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
			
			<!-- header-bottom - start -->
			<div class="header-bottom" >
                <div class="container" >
                    <div class="row" style="margin-right: : -1px; margin-left: -1px;">

                        <!-- site-logo-wrapper - start -->
                        <div class="col-lg-3" style="padding-left: 0px;">
                            <div class="site-logo-wrapper">
                                <a href="index-1.html" class="logo">
                                    <h3  class="title" style="color: white; margin-top: 8px; font-weight: 700;">
                                        KOTAK EVENT

                                    </h3>
                                    {{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
                                </a>
                            </div>
                        </div>
                        <!-- site-logo-wrapper - end -->

                        <!-- mainmenu-wrapper - start -->
                        <div class="col-lg-9">
                            <div class="mainmenu-wrapper">
                                <div class="row">
                                    @if(!Auth::user())
                                                                    <!-- menu-item-list - start -->
                                    <div class="col-lg-12" style="padding-right: 0px; float: right;">
                                        <div class="menu-item-list ul-li clearfix">
                                            <ul>
                                                <li >
                                                    <a href="/">Home</a>
                                                </li>
                                                <li class="active">
                                                    <a href="/cari">Cari Event</a>
                                                </li>
                                                <li>
                                                    <a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">
                                                        Buat Event
                                                    </a>
                                                </li>

                                                <li>
                                                    <a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">
                                                    <i class="fas fa-user"></i> Login
                                                    </a>
                                                </li>                                                                                                                                                   
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- menu-item-list - end -->

                                    @else
                                    <!-- menu-item-list - start -->
                                    <div class="col-lg-12" style="padding-right: 0px; float: right;">
                                        <div class="menu-item-list ul-li clearfix">
                                            <ul>
                                                <li >
                                                    <a href="/">Home</a>
                                                </li>
                                                <li class="active">
                                                    <a href="/cari">Cari Event</a>
                                                </li>
                                                <li>
                                                    <a href="/buat-acara">Buat Event</a>
                                                </li>
                                                <li><a href="/acara-saya">Event Saya</a></li>
                                                <li><a <a href="/undangan-saya">Tiket</a></li>                                           
                                                <li class="menu-item-has-children">
                                                    
                                                    <a href="" style="color: white;"><i style="color: white;" class="fas fa-user"></i> {{Auth::user()->name}}</a>
                                                    <ul class="sub-menu">
                                                        
                                                        <li>
                                                            <a  style="" href="{{    route('logout') }}"
                                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">{{('Logout')}}
                                                            
                                                        </a>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>

                                                    </li>
                                                        
                                                    </ul>                                               
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                    <!-- menu-item-list - end -->                                   
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                        <!-- mainmenu-wrapper - end -->

                    </div>
                </div>
            </div>
			<!-- header-bottom - end -->
		</header>

		
		<!-- header-section - end
		================================================== -->
        <div class="modal fade login" id="loginModal">
            <div class="modal-dialog login animated">
                <div class="modal-content text-center"><br>
                    <div class="container text-center">
                        <div class="modal-header text-center">
                            <h4 align="center" class="modal-title text-center" style="color: orange; text-transform: uppercase; font-weight: 600;" >LOGIN</h4>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div id="box1" class="box">
                            <div class="content">
                                @if(session('id')==10)
                                <h3  align="center" style="color: red; text-transform: capitalize;">Password Salah</h3>
                                @endif
                                @if(session('id')==11)
                                <h3 align="center" style=" text-transform: capitalize; color: red;">Email Tidak Terdaftar</h3>
                                @endif
                                <div class="form loginBox">                                    
                                    <form id="masuk" name="masuk" method="POST" action="{{route('login')}}" accept-charset="UTF-8">
                                        {{csrf_field()}}
                                        <input id="email" class="form-control" type="text" placeholder="Email" name="email" required=""><br>
                                        <input id="password" class="form-control" type="password" placeholder="Password" name="password" required=""><br>
                                        <div align="center">
                                            <button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Login</button>
                                        </div>
                                    </form>                                 
                                </div>
                                
                            </div>
                        </div>
                        <div id="box2" class="box">
                            <div class="content registerBox" style="display:none;">
                                @if(session('id')==1)
                                <h3 align="center" style="color: red;">Email Sudah Terdaftar</h3>
                                @endif
                                @if(session('id')==2)
                                <h3 align="center" style="color: red;">Password Tidak Cocok</h3>
                                @endif
                                <div class="form">
                                    <form method="POST" html="{:multipart=>true}" data-remote="true" action="{{route('register')}}" accept-charset="UTF-8">
                                        {{csrf_field()}}
                                        <input id="name" class="form-control" type="text" minlength="4" maxlength="100" placeholder="Nama" name="name" required><br>
                                        <input id="alamat" class="form-control" type="text" minlength="4" maxlength="255" placeholder="Alamat" name="alamat" required><br>
                                        <input id="email" class="form-control" type="text" placeholder="Email" name="email" required><br>
                                        <input id="password" class="form-control" type="password" minlength="6" maxlength="15" placeholder="Password" name="password" required><br>
                                        <input id="password_confirmation" class="form-control" type="password" minlength="6" maxlength="15" placeholder="Repeat Password" name="password_confirmation" required><br>
                                        <div align="center">
                                            <button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Buat Akun</button>
                                        </div>
                                    </form>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="box3" class="box">
                            <div class="content">
                                @if(session('id')==10)
                                <h3  align="center" style="color: red; text-transform: capitalize;">Password Salah</h3>
                                @endif
                                @if(session('id')==11)
                                <h3 align="center" style=" text-transform: capitalize; color: red;">Email Tidak Terdaftar</h3>
                                @endif
                                <div class="form loginBox">
                                    <form id="masuk" name="masuk" method="POST" action="{{route('login')}}" accept-charset="UTF-8">
                                        {{csrf_field()}}
                                        <input id="email" class="form-control" type="text" placeholder="Email" name="email" required=""><br>
                                        <div align="center">
                                            <button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Reset Password</button>
                                        </div>
                                    </form>                                 
                                </div>
                                
                            </div>
                        </div>
                    <div class="modal-footer">
                        <div class="forgot login-footer">
                            <span>
                                Lupa Password?
                                 <a id="regis" href="javascript: showLupaForm();">Reset</a>
                            </span>
                            <span>
                                Belum punya akun?
                                 <a id="regis" href="javascript: showRegisterForm();"><strong>Daftar Disini!</strong></a>
                            </span>
                            
                        </div>
                        <div class="forgot register-footer" style="display:none">
                            <span>Sudah punya akun?</span>
                            <a href="javascript: showLoginForm();"><strong>Login</strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<!-- altranative-header - start
		================================================== -->
				@include('layouts.altranative-header')
		<!-- altranative-header - end
		================================================== -->

		

		<!-- cari-event-section - start
		================================================== -->
		<!-- booking-section - start
		================================================== -->
		<style>
			.dtr-data{
				display: inline-block;
				float: right;
			}
			
			p{
				margin-bottom: 0px;
			}

			td{
				text-align: center;
                /*border:none;*/
                padding: 0;
			}
            table.dataTable tbody td{
                padding: 0;
            }
            .dataTables_filter input { width: 300px; height:  40px; border:2px solid #878787; margin-top: 0px; text-align: center;  }

            .dataTables_filter label { font-size: 25px; font-family: 'comic sans'; text-transform: uppercase;  }
            
            .sorting_1{
                display: none;
            }
            
		</style>
        
		<section id="main" class="wrapper">    
		<div id="absolute-eventmake-section" class="absolute-eventmake-section sec-ptb-77 bg-gray-light clearfix"  >
            <div class="eventmaking-wrapper"   >

                <div class="tab-content" >
                    <div id="conference" class="tab-pane fade in active show"  >
                        <form method="post" action="/cari" >
                            {{csrf_field()}}

                            <ul >
                                <li>
                                    <input style="font-size: 14px; text-align: center;" type="text" name="kunci" placeholder="Kode Event" required="">
                                    <input type="hidden" name="kategori" value="private">
                                </li>                                                           
                                {{-- <li>
                                    <select name="kategori">
                                        
                                        <option value="public"><p style="font-size: 10px;">Nama Event</p></option>
                                        <option value="private">Kode Event</option>
                                        
                                    </select>
                                </li> --}}
                                <li>
                                    <button type="submit" value="cari" style="background: #d9534f;" class="kastem-btn" >Cari</button>
                                </li>

                            </ul>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
          

			</section>
              <div class="container" style="padding-bottom: 2em;">
                    {{-- <header class="major special">
                        <h2 align=""style=" font-family: 'comic sans'; text-transform: uppercase;">CARI EVENT</h2>
                    </header> --}}
                    <div id="page1" style="display: block;">

                        <div class="content">

                            <table class="table is-hoverable is-fullwidth cards" id="primary_table">
                                
                                    <tr>
                                       <th>No</th>
                                       <th>Jadwal</th>
                                       <th>Nama</th>                         
                                       <th>Action</th>

                                   </tr>
                               
                           </table>
                       </div>
                   </div>
               </div>
		<!-- booking-section - end
		================================================== -->

<style type="text/css">
    h4 span {
            /*border:4px solid linear-gradient(45deg, red, orange, yellow);        */
          /*box-shadow: 10px 0 0px 0px #EDC330, -10px 0 0px 0px #EDC330;*/
          
          background: linear-gradient(140deg,  orange,red,red,red);

          /*width: 46px;*/
          padding-left: 15px;
          padding-right: 15px;  
          text-align: center;
          font-weight: 900;
            font-size: 20px;
            font-family: "Roboto", sans-serif;
            margin: 0;


    }
    h4{
        font-weight: 900;
            font-size: 16px;
            font-family: "Roboto", sans-serif;
            max-height: 60px;
                min-height: 60px;
                text-overflow: ellipsis; overflow: hidden;
    }
        .bulan{
            font-family: "Open Sans", sans-serif;
            font-size: 18px;
            font-weight: 400;
            padding: 0;
            margin: 0;             
        }
.dataTables_filter {
                margin-top: 100px;

                float: left!important;
                margin-left: 260px;
                }
.lihat{
    z-index: 1;
      font-weight: 600;
      overflow: hidden;
      padding: 15px 45px;
      position: relative;
      border-radius: 30px;
      color: #878787;
      font-size: 14px;
      text-transform: uppercase;
      background: #f7f7f7;
}
.lihat:hover{
    color: #ffffff;
        background: #ffbe30;
}

</style>
	<script>
        
        var primary_table = $('#primary_table').DataTable({
        responsive: true,                
        processing: true,
        serverSide: true,
        // language: {searchPlaceholder: "Nama Event"},
        ajax: {
            url:  'cari-event',
            type: 'get'
        },
        columns: [
        	{ data: 'nama', name: 'nama',
	        	render: function(data) {
	                    return '<p></p>';
	                }

        	 },
             { data: 'jadwal_mulai', name: 'jadwal_mulai',
                render: function(data) {
                        return '<p></p>';
                    }

             },
        	{ data: 'gambar', name: 'gambar',
                render: function(data) {
                        return '<a href="acara/'+data.id+'" >'+'<h4  style="position:absolute;text-transform:uppercase; text-align:left;color:white; "><span><div>'+data.hari+'</div><x class="bulan">'+data.bulan+'</x></span></h4>'+
                        '<figure class="image" style="margin: 0; height:100%; width:100px;">'+
                        '<img src="'+data.src+'" style="width:200px; height:200px;">'+
                        '</figure>'+'</a>';
                    }

             },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    if(data.jenis_daftar=='bayar'){
                    return '<a href="acara/'+data.id+'" >'+'<h4 style="text-align:left; padding-left:3.5em; padding-top:1.3em;text-transform: uppercase; margin-bottom:0;">'+data.nama+'</h4>'+
                    '<p style="color:orange; text-align:left; padding-left:3.5em;">Berbayar</p>'+
                    '<p style="font-size:12px;  text-align:left; margin-top:1em; padding-left:5em;text-overflow: ellipsis; overflow: hidden; height:50px;"><i class="far fa-clock"></i> '+data.waktu+'    <br>   <i class="fas fa-map-marker-alt"></i> '+data.lokasi+'  </p>'+
                    '</a>';
                }else{
                    return '<a style="padding-bottom:0;" href="acara/'+data.id+'" >'+'<h4 style="text-align:left; padding-left:3.5em; padding-top:1.3em;text-transform: uppercase; margin-bottom:0;">'+data.nama+'</h4>'+
                    '<p style="color:orange; text-align:left; padding-left:3.5em;">Free</p>'+
                    '<p style="font-size:12px;  text-align:left; margin-top:1em; padding-left:5em;text-overflow: ellipsis; overflow: hidden; height:50px;"><i class="far fa-clock"></i> '+data.waktu+'    <br>   <i class="fas fa-map-marker-alt"></i> '+data.lokasi+'  </p>'+
                        '</a>';
                }
                     
                }
            },
            
            
            
        ]
    });


    
primary_table.on( 'draw', function () {
        
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            
        } );
    } ).draw();
     

        

        </script>   
		<br><br><br>
		{{-- @include('layouts.footer') --}}




		<!-- cari-event-section - end
		================================================== -->




		

		
        <script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>

        

        <!-- calendar jquery include -->
        <script src="{{asset('assets/assets/js/atc.min.js')}}"></script>

        <!-- others jquery include -->
        <script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
        
        <script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
        <!-- custom jquery include -->
        <script src="{{asset('assets/assets/js/custom.js')}}"></script>
        <script src="{{asset('assets/js/login-register.js')}}"></script>	



	</body>
</html>