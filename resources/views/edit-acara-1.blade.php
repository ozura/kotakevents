<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">


		<link rel="stylesheet" href="{{asset('assets/css/main.css')}}" />

		<!-- fraimwork - css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/flaticon.css')}}">

		<!-- carousel css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/slick.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/slick-theme.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/animate.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/owl.carousel.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/owl.theme.default.min.css')}}">

		<!-- others css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/magnific-popup.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">



		<!-- custom css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">



		
		
		
		
		
		<script src="{{asset('assets/js/jquery.min.js')}}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
		<style>
			#kode-unik{
				display: none;
			}
			.bayar{
				display: none;
			}
		</style>

	</head>


	<body class="default-header-p">




		
		<!-- backtotop - start -->
		<div id="thetop" class="thetop"></div>
		<div class='backtotop'>
			<a href="#thetop" class='scroll'>
				<i class="fas fa-angle-double-up"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		<div id="preloader"></div>
		<!-- preloader - end -->




		<!-- header-section - start
		================================================== -->
				<header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
			<div class="header-bottom" >
				<div class="container" >
					<div class="row" style="margin-right: : -1px; margin-left: -1px;">

						<!-- site-logo-wrapper - start -->
						<div class="col-lg-3" style="padding-left: 0px;">
							<div class="site-logo-wrapper">
								<a href="index-1.html" class="logo">
									<h3  class="title" style="color: white; margin-top: 8px; font-weight: 700;">
										KOTAK EVENT

									</h3>
									{{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
								</a>
							</div>
						</div>
						<!-- site-logo-wrapper - end -->

						<!-- mainmenu-wrapper - start -->
						<div class="col-lg-9">
							<div class="mainmenu-wrapper">
								<div class="row">														
									<!-- menu-item-list - start -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li >
													<a href="/">Home</a>
												</li>
												<li >
													<a href="/cari">Cari Event</a>
												</li>
												<li>
													<a href="/buat-acara">Buat Event</a>
												</li>
												<li><a href="/acara-saya">Event Saya</a></li>
												<li><a href="/undangan-saya">Tiket</a></li>
												
												
												<li class="menu-item-has-children">													
													<a href="" style="color: white;"><i style="color: white;" class="fas fa-user"></i> {{Auth::user()->name}}</a>
													<ul class="sub-menu">
														
														<li>
															<a  style="" href="{{    route('logout') }}"
															onclick="event.preventDefault();
															document.getElementById('logout-form').submit();">{{('Logout')}}
															
														</a>
														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>

													</li>
														
													</ul>												
												</li>

											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->									
									
									
								</div>
							</div>
						</div>
						<!-- mainmenu-wrapper - end -->

					</div>
				</div>
			</div>
			<!-- header-bottom - end -->
		</header>
		<!-- header-section - end
		================================================== -->




		<div class="container" style="padding-top: 1.5em;">							
			<a  href="{{ url('/')}}"><p>Home ></p></a>
			<a href="/acara-saya"><p>Event ></p></a>
			<a href="/acara/{{$detail->id}}"><p>{{$detail->nama}} ></p></a>
			<a href=""><p>Edit</p></a>
		
		</div>


		<!-- booking-section - start
		================================================== -->
					<section id="main" class="wrapper">
				<div class="container">
					<h3>Edit Acara</h3>
	
					<form method="post" action="{{route('edit.acara.post')}}">
						{{csrf_field()}}
						<input type="hidden" name="id" value="{{$detail->id}}">
						<div class="row uniform 50%">
							<div class="6u 12u$(xsmall)">
								<input type="text" name="name" value="{{$detail->nama}}" id="name" value="" placeholder="Nama Acara" required />
							</div>
							<div class="6u$ 12u$(xsmall)">
								<input type="text" name="lokasi"  id="email" value="{{$detail->lokasi}}" placeholder="Lokasi Acara" required />
							</div>
							<div class="6u 12u$(xsmall)">
								<div class="input-group date" id="datetimepicker1" data-target-input="nearest">
									<input type="text" name="jadwal_mulai" value="{{$detail->jadwal_mulai}}" class="form-control datetimepicker-input" data-target="#datetimepicker1" placeholder="Jadwal Mulai" required/>
									<div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
							<div class="6u$ 12u$(xsmall)">
								<div class="input-group date" id="datetimepicker2" data-target-input="nearest">
									<input type="text" name="jadwal_selesai" value="{{$detail->jadwal_selesai}}" class="form-control datetimepicker-input" data-target="#datetimepicker2" placeholder="Jadwal Selesai" required />
									<div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
							<div class="3u 12u$(xsmall)">
							<p>Jenis Event</p>
						</div>
						<div class="4u 12u$(xsmall)">
							<input class="open" type="radio" id="low" value="open" name="jenis_event" @if($detail->jenis_event=='open')checked @endif>
							<label for="low">Open (tamu dapat mendaftar event melalui akun Kotakevent) </label>
						</div>
						<div class="5u 12u$(xsmall)">
							<input class="close" type="radio" id="normal" value="close" name="jenis_event" 
							@if($detail->jenis_event=='close') checked @endif>
							<label for="normal">Close (tamu hanya dapat ditambahkan oleh pemilik event)</label>
						</div>
						<c class="hilang" @if($detail->jenis_event=='close') style ="width: 100%;  display: none;" @else style ="width: 100%;"@endif>
							<div class="3u 12u$(xsmall)">
								<p>Pilih Jenis Pendaftaran Acara </p>
							</div>
							<div class="3u 12u$(xsmall)">
								<input class="bukan-bayar" type="radio" id="priority-low" value="siapapun" name="jenis_daftar" @if($detail->jenis_daftar=='siapapun')
								checked
										@endif>
								<label for="priority-low">siapapun yang mendaftar secara langsung akan terverifikasi dan mendapatkan QR Code sebagai bukti daftar</label>
							</div>
							<div class="3u 12u$(xsmall)">
								<input class="bukan-bayar" type="radio" id="priority-normal" value="memilih" name="jenis_daftar" @if($detail->jenis_daftar=='memilih')
								checked
										@endif>
								<label for="priority-normal">saya akan memilih siapa saja yang boleh ikut dalam acara ini</label>
							</div>
							<div class="3u$ 12u$(xsmall)">
								<input class="dibayar" type="radio" id="priority-high" value="bayar" name="jenis_daftar" @if($detail->jenis_daftar=='bayar')
								checked
										@endif>
								<label for="priority-high"> acara ini berbayar sehingga pendaftar harus mengunggah bukti pembayaran</label>
								<input class="bayar" name="bank" type="text" @if($detail->jenis_daftar!='bayar')
								style="display: none;"
									   @else
									   value="{{$detail->bank}}"
									   @endif placeholder="Bank">
								<input class="bayar" name="norek" type="text" @if($detail->jenis_daftar!='bayar')
										style="display: none;"
									   @else
									   value="{{$detail->norek}}"
								@endif placeholder="No Rekening">
								<input class="bayar" name="atasnama" type="text" @if($detail->jenis_daftar!='bayar')
										style="display: none;"
									   @else
									   value="{{$detail->atasnama}}"
								@endif placeholder="Atas Nama">
								<input class="bayar" name="jumlah_bayar" type="text" @if($detail->jenis_daftar!='bayar')
										style="display: none;"
									   @else
									   value="{{$detail->jumlah_bayar}}"
								@endif placeholder="Harga Tiket">
							</div>
							<div class="3u 12u$(xsmall)">
								<p>Pilih Siapa Yang Dapat Melihat</p>
							</div>
							<div class="4u 12u$(small)">
								<input type="radio" id="copy" name="tipe" value="public" @if($detail->tipe=='public')
								checked
										@endif>
								<label for="copy">Siapapun dapat mencari acara ini</label>
							</div>
							<div class="5u$ 12u$(small)">
								<input type="radio" id="human" value="private" name="tipe"  @if($detail->tipe=='private')
								checked
										@endif>
								<label for="human">Acara ini hanya dapat dicari menggunakan kode</label>
								<input id="kode-unik" type="text" @if($detail->tipe=='private')
								value="{{$detail->kode_unik}}"
								@endif @if($detail->tipe=='private')
									style="display: block;"
								@endif name="kode_unik" placeholder="Kode unik" minlength="6" maxlength="15">
							</div>
							</c>
							<div class="12u$">
								<textarea name="deskripsi" id="deskripsi" placeholder="Masukkan deskripsi acara" rows="6">{{$detail->deskripsi}}</textarea>
							</div>
							<div class="12u$">
							<ul class="actions">
								<li><input style="background-color: orange;" type="submit" value="Simpan Acara" class="special" /></li>
							</ul>
							</div>
							
						</div>
					</form>
						
						
				</div>
			</section>
		<!-- booking-section - end
		================================================== -->





		<!-- default-footer-section - start
		================================================== -->
		<br><br><br>
		@include('layouts.footer')
		<!-- default-footer-section - end
		================================================== -->


			<script>
				$(function () {
					

				});
				$('#datetimepicker1').datetimepicker({
						format: 'YYYY-MM-DD HH:mm:ss'
					});

				$('#datetimepicker2').datetimepicker({
						format: 'YYYY-MM-DD HH:mm:ss'
					});
				$('#human').change(function() {
					if(this.checked) {
						$('#kode-unik').show();
					}
				});
				$('#copy').change(function() {
					if(this.checked) {
						$('#kode-unik').hide();
					}
				});
				$('.dibayar').change(function() {
					if(this.checked) {
						$('.bayar').show();
					}
				});
				$('.bukan-bayar').change(function() {
					if(this.checked) {
						$('.bayar').hide();
					}
				});
					$('.close').change(function() {
					if(this.checked) {
						$('.hilang').hide();
					}
				});
				$('.open').change(function() {
					if(this.checked) {
						$('.hilang').show();
					}
				});
			</script>











				<!-- fraimwork - jquery include -->
		<script src="{{asset('assets/assets/js/jquery-3.3.1.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/popper.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>

		<!-- carousel jquery include -->
		<script src="{{asset('assets/assets/js/slick.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/owl.carousel.min.js')}}"></script>

		<!-- map jquery include -->
		<script src="{{asset('assets/assets/js/gmap3.min.js')}}"></script>
		<script src="http://maps.google.com/maps/api/js?key=AIzaSyC61_QVqt9LAhwFdlQmsNwi5aUJy9B2SyA"></script>



		<!-- others jquery include -->
		<script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/isotope.pkgd.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/jarallax.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>		

		
		<!-- custom jquery include -->
		<script src="{{asset('assets/assets/js/custom.js')}}"></script>




	</body>
</html>