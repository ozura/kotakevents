<!DOCTYPE HTML>
<!--
	Retrospect by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		{{-- <title>Invit - Bengkulu</title> --}}
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<!--[if lte IE 8]><script src="{{asset('assets/js/ie/html5shiv.js')}}"></script><![endif]-->
		<link rel="stylesheet" href="{{asset('assets/css/main.css')}}" />
		<!--[if lte IE 8]><link rel="stylesheet" href="{{asset('assets/css/ie8.css')}}" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="{{asset('assets/css/ie9.css')}}" /><![endif]-->
		<script src="assets/js/jquery.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
		<style>
			#kode-unik{
				display: none;
			}
			.bayar{
				display: none;
			}
		</style>
	</head>
	<body>

		@include('layouts.nav')

		<!-- Main -->
			<section id="main" class="wrapper">
				<div class="container">
					<h3>Edit Acara</h3>
					@if($detail->status=="Belum Dikonfirmasi")
					<form method="post" action="{{route('edit.acara.post')}}">
						{{csrf_field()}}
						<input type="hidden" name="id" value="{{$detail->id}}">
						<div class="row uniform 50%">
							<div class="6u 12u$(xsmall)">
								<input type="text" name="name" value="{{$detail->nama}}" id="name" value="" placeholder="Nama Acara" required />
							</div>
							<div class="6u$ 12u$(xsmall)">
								<input type="text" name="lokasi"  id="email" value="{{$detail->lokasi}}" placeholder="Lokasi Acara" required />
							</div>
							<div class="6u 12u$(xsmall)">
								<div class="input-group date" id="datetimepicker1" data-target-input="nearest">
									<input type="text" name="jadwal_mulai" value="{{$detail->jadwal_mulai}}" class="form-control datetimepicker-input" data-target="#datetimepicker1" placeholder="Jadwal Mulai" required/>
									<div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
							<div class="6u$ 12u$(xsmall)">
								<div class="input-group date" id="datetimepicker2" data-target-input="nearest">
									<input type="text" name="jadwal_selesai" value="{{$detail->jadwal_selesai}}" class="form-control datetimepicker-input" data-target="#datetimepicker2" placeholder="Jadwal Selesai" required />
									<div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-calendar"></i></div>
									</div>
								</div>
							</div>
							<div class="3u 12u$(xsmall)">
								<p>Pilih Jenis Pendaftaran Acara </p>
							</div>
							<div class="3u 12u$(xsmall)">
								<input class="bukan-bayar" type="radio" id="priority-low" value="siapapun" name="jenis_daftar" @if($detail->jenis_daftar=='siapapun')
								checked
										@endif>
								<label for="priority-low">siapapun yang mendaftar secara langsung akan terverifikasi dan mendapatkan QR Code sebagai bukti daftar</label>
							</div>
							<div class="3u 12u$(xsmall)">
								<input class="bukan-bayar" type="radio" id="priority-normal" value="memilih" name="jenis_daftar" @if($detail->jenis_daftar=='memilih')
								checked
										@endif>
								<label for="priority-normal">saya akan memilih siapa saja yang boleh ikut dalam acara ini</label>
							</div>
							<div class="3u$ 12u$(xsmall)">
								<input class="dibayar" type="radio" id="priority-high" value="bayar" name="jenis_daftar" @if($detail->jenis_daftar=='bayar')
								checked
										@endif>
								<label for="priority-high"> acara ini berbayar sehingga pendaftar harus mengunggah bukti pembayaran</label>
								<input class="bayar" name="bank" type="text" @if($detail->jenis_daftar!='bayar')
								style="display: none;"
									   @else
									   value="{{$detail->bank}}"
									   @endif placeholder="Bank">
								<input class="bayar" name="norek" type="text" @if($detail->jenis_daftar!='bayar')
										style="display: none;"
									   @else
									   value="{{$detail->norek}}"
								@endif placeholder="No Rekening">
								<input class="bayar" name="atasnama" type="text" @if($detail->jenis_daftar!='bayar')
										style="display: none;"
									   @else
									   value="{{$detail->atasnama}}"
								@endif placeholder="Atas Nama">
								<input class="bayar" name="jumlah_bayar" type="text" @if($detail->jenis_daftar!='bayar')
										style="display: none;"
									   @else
									   value="{{$detail->jumlah_bayar}}"
								@endif placeholder="Jumlah Biaya">
							</div>
							<div class="3u 12u$(xsmall)">
								<p>Pilih Siapa Yang Dapat Melihat</p>
							</div>
							<div class="4u 12u$(small)">
								<input type="radio" id="copy" name="tipe" value="public" @if($detail->tipe=='public')
								checked
										@endif>
								<label for="copy">Siapapun dapat mencari acara ini</label>
							</div>
							<div class="5u$ 12u$(small)">
								<input type="radio" id="human" value="private" name="tipe"  @if($detail->tipe=='private')
								checked
										@endif>
								<label for="human">Acara ini hanya dapat dicari menggunakan kode</label>
								<input id="kode-unik" type="text" @if($detail->tipe=='private')
								value="{{$detail->kode_unik}}"
								@endif @if($detail->tipe=='private')
									style="display: block;"
								@endif name="kode_unik" placeholder="Kode unik" minlength="6" maxlength="15">
							</div>
							<div class="12u$">
								<textarea name="deskripsi" id="deskripsi" placeholder="Masukkan deskripsi acara" rows="6">{{$detail->deskripsi}}</textarea>
							</div>
							<div class="12u$">
								<ul class="actions">
									<li><input type="submit" value="Simpan Acara" class="special" /></li>
								</ul>
							</div>
						</div>
					</form>
						@else
							<form method="post" action="{{route('edit.acara.post')}}">
								{{csrf_field()}}
								<input type="hidden" name="id" value="{{$detail->id}}">
								<div class="row uniform 50%">
									<div class="6u 12u$(xsmall)">
										<input type="text" name="name" value="{{$detail->nama}}" id="name" value="" placeholder="Nama Acara" required readonly />
									</div>
									<div class="6u$ 12u$(xsmall)">
										<input type="text" name="lokasi"  id="email" value="{{$detail->lokasi}}" placeholder="Lokasi Acara" required readonly/>
									</div>
									<div class="6u 12u$(xsmall)">
										<div class="input-group date" id="datetimepicker1" data-target-input="nearest">
											<input type="text" name="jadwal_mulai" value="{{$detail->jadwal_mulai}}" class="form-control datetimepicker-input" readonly data-target="#datetimepicker1" placeholder="Jadwal Mulai" required/>
											<div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
												<div class="input-group-text"><i class="fa fa-calendar"></i></div>
											</div>
										</div>
									</div>
									<div class="6u$ 12u$(xsmall)">
										<div class="input-group date" id="datetimepicker2" data-target-input="nearest">
											<input type="text" name="jadwal_selesai" value="{{$detail->jadwal_selesai}}" class="form-control datetimepicker-input" readonly data-target="#datetimepicker2" placeholder="Jadwal Selesai" required />
											<div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
												<div class="input-group-text"><i class="fa fa-calendar"></i></div>
											</div>
										</div>
									</div>
									<div class="3u 12u$(xsmall)">
										<p>Pilih Jenis Pendaftaran Acara </p>
									</div>
									<div class="3u 12u$(xsmall)">
										<input class="bukan-bayar" type="radio" id="priority-low" value="siapapun" name="jenis_daftar" disabled="" @if($detail->jenis_daftar=='siapapun')
										checked
												@endif>
										<label for="priority-low">siapapun yang mendaftar secara langsung akan terverifikasi dan mendapatkan QR Code sebagai bukti daftar</label>
									</div>
									<div class="3u 12u$(xsmall)">
										<input class="bukan-bayar" type="radio" id="priority-normal" value="memilih" disabled="" name="jenis_daftar" @if($detail->jenis_daftar=='memilih')
										checked
												@endif>
										<label for="priority-normal">saya akan memilih siapa saja yang boleh ikut dalam acara ini</label>
									</div>
									<div class="3u$ 12u$(xsmall)">
										<input class="dibayar" type="radio" id="priority-high" value="bayar" disabled="" name="jenis_daftar" @if($detail->jenis_daftar=='bayar')
										checked
												@endif>
										<label for="priority-high"> acara ini berbayar sehingga pendaftar harus mengunggah bukti pembayaran</label>
										<input class="bayar" name="bank" type="text" readonly @if($detail->jenis_daftar!='bayar')
										style="display: none;"
											   @else
											   value="{{$detail->bank}}"
											   @endif placeholder="Bank">
										<input class="bayar" name="norek" type="text" readonly @if($detail->jenis_daftar!='bayar')
										style="display: none;"
											   @else
											   value="{{$detail->norek}}"
											   @endif placeholder="No Rekening">
										<input class="bayar" name="atasnama" type="text" readonly @if($detail->jenis_daftar!='bayar')
										style="display: none;"
											   @else
											   value="{{$detail->atasnama}}"
											   @endif placeholder="Atas Nama">
										<input class="bayar" name="jumlah_bayar" type="text" readonly="" @if($detail->jenis_daftar!='bayar')
										style="display: none;"
											   @else
											   value="{{$detail->jumlah_bayar}}"
											   @endif placeholder="Jumlah Biaya">
									</div>
									<div class="3u 12u$(xsmall)">
										<p>Pilih Siapa Yang Dapat Melihat</p>
									</div>
									<div class="4u 12u$(small)">
										<input type="radio" id="copy" name="tipe" value="public" disabled="" @if($detail->tipe=='public')
										checked
												@endif>
										<label for="copy">Siapapun dapat mencari acara ini</label>
									</div>
									<input type="hidden" name="jenis_daftar" value="{{$detail->jenis_daftar}}">
									<input type="hidden" name="tipe" value="{{$detail->tipe}}">
									<div class="5u$ 12u$(small)">
										<input type="radio" id="human" value="private" name="tipe" disabled=""  @if($detail->tipe=='private')
										checked
												@endif>
										<label for="human">Acara ini hanya dapat dicari menggunakan kode</label>
										<input id="kode-unik" type="text" @if($detail->tipe=='private')
										value="{{$detail->kode_unik}}"
											   @endif @if($detail->tipe=='private')
											   style="display: block;"
											   @endif name="kode_unik" placeholder="Kode unik" readonly minlength="6" maxlength="15">
									</div>
									<div class="12u$">
										<textarea name="deskripsi" id="deskripsi" placeholder="Masukkan deskripsi acara" rows="6">{{$detail->deskripsi}}</textarea>
									</div>
									<div class="12u$">
										<ul class="actions">
											<li><input type="submit" value="Simpan Acara" class="special" /></li>
										</ul>
									</div>
								</div>
							</form>
						@endif
				</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<div class="inner">
					<ul class="icons">
						<li><a href="#" class="icon fa-facebook">
							<span class="label">Facebook</span>
						</a></li>
						<li><a href="#" class="icon fa-twitter">
							<span class="label">Twitter</span>
						</a></li>
						<li><a href="#" class="icon fa-instagram">
							<span class="label">Instagram</span>
						</a></li>
						<li><a href="#" class="icon fa-linkedin">
							<span class="label">LinkedIn</span>
						</a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled.</li>
						<li>Images: <a href="http://unsplash.com">Unsplash</a>.</li>
						<li>Design: <a href="http://templated.co">TEMPLATED</a>.</li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->

		<script src="{{asset('assets/js/skel.min.js')}}"></script>
		<script src="{{asset('assets/js/util.js')}}"></script>
		<!--[if lte IE 8]><script src="{{asset('assets/js/ie/respond.min.js')}}"></script><![endif]-->
		<script src="{{asset('assets/js/main.js')}}"></script>
		<script src="{{asset('assets/js/login-register.js')}}"></script>
			<script>
				$(function () {
					$('#datetimepicker1').datetimepicker({
						format: 'YYYY-MM-DD HH:mm:ss'
					});
					$('#datetimepicker2').datetimepicker({
						format: 'YYYY-MM-DD HH:mm:ss'
					});

				});
				$('#human').change(function() {
					if(this.checked) {
						$('#kode-unik').show();
					}
				});
				$('#copy').change(function() {
					if(this.checked) {
						$('#kode-unik').hide();
					}
				});
				$('.dibayar').change(function() {
					if(this.checked) {
						$('.bayar').show();
					}
				});
				$('.bukan-bayar').change(function() {
					if(this.checked) {
						$('.bayar').hide();
					}
				});
			</script>
	</body>
</html>