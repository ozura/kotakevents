<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		
		
<!-- fraimwork - css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/flaticon.css')}}"> --}}



		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">


		

		<!-- custom css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">
	

		
		<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="{{asset('css/tabel.min.css')}}">





		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

		<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
		<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>



	</head>

	
	<body>

	
	
 
		<!-- backtotop - start -->
		<div id="thetop" class="thetop"></div>
		<div class='backtotop'>
			<a href="#thetop" class='scroll'>
				<i class="fas fa-angle-double-up"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		<div id="preloader"></div>
		<!-- preloader - end -->





		<!-- header-section - start
		================================================== -->
		<header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
			<div class="header-bottom" >
				<div class="container" >
					<div class="row" style="margin-right: : -1px; margin-left: -1px;">

						<!-- site-logo-wrapper - start -->
						<div class="col-lg-3" style="padding-left: 0px;">
							<div class="site-logo-wrapper">
								<a href="index-1.html" class="logo">
									<h3  class="title" style="color: white; margin-top: 8px;">
										KOTAK EVENTS

									</h3>
									{{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
								</a>
							</div>
						</div>
						<!-- site-logo-wrapper - end -->

						<!-- mainmenu-wrapper - start -->
						<div class="col-lg-9">
							<div class="mainmenu-wrapper">
								<div class="row">														
									<!-- menu-item-list - start -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li >
													<a href="/">Home</a>
												</li>
												<li >
													<a href="/cari">Cari Event</a>
												</li>
												<li>
													<a href="/buat-acara">Buat Event</a>
												</li>
												<li><a href="/acara-saya">Event</a></li>
												<li class="active"><a href="/undangan-saya">Undangan</a></li>
												
												
												<li class="menu-item-has-children">													
													<a href="" style="color: white;"><i style="color: white;" class="fas fa-user"></i> {{Auth::user()->name}}</a>
													<ul class="sub-menu">
														
														<li>
															<a  style="" href="{{    route('logout') }}"
															onclick="event.preventDefault();
															document.getElementById('logout-form').submit();">{{('Logout')}}
															
														</a>
														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>

													</li>
														
													</ul>												
												</li>

											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->									
									
									
								</div>
							</div>
						</div>
						<!-- mainmenu-wrapper - end -->

					</div>
				</div>
			</div>
			<!-- header-bottom - end -->
		</header>

		
		
		<!-- header-section - end
		================================================== -->
		<!-- altranative-header - start
		================================================== -->
		@include('layouts.altranative-header')
		<!-- altranative-header - end
		================================================== -->

		
		<style>
			.dtr-data{
				display: inline-block;
				float: right;
			}
			p{
				margin-bottom: 0px;
			}
			
		</style>
		<!-- cari-event-section - start
		================================================== -->
		<!-- booking-section - start
		================================================== -->
		<section id="main" class="wrapper sec-ptb-75" >
				<div class="container">
					<header class="major special">

						<h2 align="center" style="font-family: comic sans; text-transform: uppercase; color: #878787; padding-top: 0.1em;">UNDANGAN</h2>
					</header>
					@include('notification')
					<div class="table-responsive">
						<table id="example" class="table table-striped table-bordered" style="width:100%">
							<thead>
							<tr align="center" style="color: white;">
								
								<th>Nama</th>
								<th>Jadwal Event</th>								
								<th>Lokasi Event</th>
								<th>Status Pendaftaran</th>
								<th>Undangan</th>
							</tr>
							</thead>
							<tbody>
								
							@foreach($events as $event)
							<tr align="center">
								
								<td><a style="text-transform: uppercase; color: #d9534f;"  href="{{route('detail.acara',['id'=>$event->id_event])}}">{{$event->nama}}</a></td>
																
								<td>
									<?php 
										$mulai = date("d-M-Y",strtotime($event['jadwal_mulai']));
									 ?>
										{{$mulai}}
								</td>								
								<td>{{$event['lokasi']}}</td>
								<td>
									@if($event->status=='Belum Dikonfirmasi')
									<p style="color: red;">Belum Dikonfirmasi</p>
									@elseif($event->status=='Menunggu Pembayaran')
									<p style="color: red;">Menunggu Pembayaran</p>
									@else
									<p style="color: green;">Sudah Dikonfirmasi</p>
									@endif
								</td>
								@if($event['status']=='Sudah Dikonfirmasi')
								<td>
									<a href="{{route('download',['ev'=>$event->id_event,'us'=>Auth::user()->id,'sc'=>$event->id_scan])}}">
								<button  type="button" class="btn btn-success btn-sm"><i class="fas fa-download"></i> Undangan</button>
								</a>
								</td>
								@else
								<td></td>
								@endif

							</tr>
							
							@endforeach
							</tbody>
							
						</table>
					</div>
				</div>
			</section>
		<!-- booking-section - end
		================================================== -->
		<br><br><br>
		@include('layouts.footer')


		

		<!-- cari-event-section - end
		================================================== -->

		<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/atc.min.js')}}"></script>

		<!-- others jquery include -->
		<script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
		
		
		<script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
	

		<!-- custom jquery include -->
		<script src="{{asset('assets/assets/js/custom.js')}}"></script>



{{-- 
		<script>
			$(document).ready(function() {
				var table = $('#example').DataTable( {
					
					responsive: true
				} );
			} );
		</script> --}}

		

		<script>
			$(document).ready(function() {
				var table = $('#example').DataTable( {
					
					responsive: true
				} );
			} );
		</script>
		
		<script>
			$(document).ready(function() {
				$('#example').DataTable();
			} );
			function nonaktifkan(klik,id){
				$(klik).attr("disabled", true);
				$.get("{{route('nonaktifkan.acara',['id'=>''])}}/"+id, function(data, status){
					console.log('success')
				});
			}
			function aktifkan(klik,id){
				$(klik).attr("disabled", false);
				$.get("{{route('aktifkan.acara',['id'=>''])}}/"+id, function(data, status){
					console.log('success')
				});
			}
		
		</script>





	</body>
</html>