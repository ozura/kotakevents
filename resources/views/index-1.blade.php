<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		
		{{-- <link rel="stylesheet" href="{{asset('assets/css/box.css')}}" /> --}}
		<link rel="stylesheet" href="{{asset('assets/css/login-register.css')}}">

		<!-- fraimwork - css include -->
		<link rel="stylesheet" href="{{asset('assets/assets/css/bootstrap.min.css')}}">		
		{{-- <link rel="stylesheet" type="text/css" href="https://kotakevents.com/assets/assets/css/bootstrap.min.css"> --}}

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/flaticon.css')}}">

		<!-- carousel css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/slick.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/slick-theme.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/animate.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/owl.carousel.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/owl.theme.default.min.css')}}">

		<!-- others css include -->
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/magnific-popup.css')}}"> --}}
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/calendar.css')}}">
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/lightcase.css')}}"> --}}

		

		<!-- custom css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">
		
		{{-- <link rel="stylesheet" type="text/css" href="https://kotakevents.com/assets/assets/css/style.css"> --}}
		<!-- Toast -->
			
			<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

	</head>

	
	<body>

	
 
		<!-- backtotop - start -->
		<div id="thetop" class="thetop"></div>
		<div class='backtotop'>
			<a href="#thetop" class='scroll'>
				<i class="fas fa-angle-double-up"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		{{-- <div id="preloader"></div> --}}
		<!-- preloader - end -->
		<style>
			.modal-open {
				
				overflow: scroll;
				width: 100%;
				padding-right: 0!important;
			}
		</style>

		@if(session('id') == 10 || session('id') == 11 )
		<script>
			$(function() {
				$('#loginModal').modal('show');
				$('#box2').hide();
				$('#box3').hide();
			});
		</script>
		@endif
		@if(session('id') == 1 || session('id') == 2 )
		<script>


			$(function() {
				$('#reg').click();



			});

		</script>
		@endif

		<style scoped>
		@media (max-width:400px){
			.login .modal-dialog{
				width: 95%;

			}
		}
		</style>

		<!-- header-section - start
		================================================== -->
		<header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
			<!-- header-top - start -->
			{{-- <div class="header-top">
				<div class="container">
					<div class="row">

						
						
						
					</div>
				</div>
			</div> --}}
			<!-- header-top - end -->
			<!-- header-bottom - start -->
			<div class="header-bottom">
				<div class="container"  >
					<div class="row">

						<!-- site-logo-wrapper - start -->
						<div class="col-lg-3" >
							<div class="site-logo-wrapper" style="float: left;">
								{{-- <img style="height: 40px;"  src="{{asset('images/10.png')}}"> --}}
								<a href="index-1.html" class="logo">

									<h3  class="title" style="color: white; margin-top: 8px; font-weight: 700;">
										KOTAK EVENT

									</h3>
									{{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
								</a>
							</div>
						</div>
						<!-- site-logo-wrapper - end -->

						<!-- mainmenu-wrapper - start -->
						<div class="col-lg-9">
							<div class="mainmenu-wrapper">
								<div class="row">
									@if(!Auth::user())
																	<!-- menu-item-list - start -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li class="active">
													<a href="/">Home</a>
												</li>
												<li>
													<a href="/cari">Cari Event</a>
												</li>
												<li>
													<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">
														Buat Event
													</a>
												</li>

												<li>
													<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">
													<i class="fas fa-user"></i>	Login
													</a>
												</li>
												<li style="display: none;"><a id="reg" data-toggle="modal" href="javascript:void(0)" onclick="openRegisterModal();">Register</a></li>																																					
												
											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->

									@elseif(Auth::user()->email=='adebwahyu@gmail.com')
								<!-- menu-item-list - start admin -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li class="active">
													<a href="/buat-acara">Home</a>
												</li>
												<li>
													<a href="/buat-acara">Pengajuan Event</a>
												</li>
												<li>
													<a href="/buat-acara">Event Terkonfirmasi</a>
												</li>
											
												<li>
													<a href="/buat-acara">Event Ditolak</a>
												</li>
												<li>
													<a href="/buat-acara">Event Selesai</a>
												</li>											
													<li class="menu-item-has-children">
													<?php 
															$name = Auth::user()->name;
															$splitName = explode(' ', $name, 2);

															$first_name = $splitName[0];
														
														?>
													<a href="" style="color: orange;"><i style="color: white;" class="fas fa-user"></i> {{$first_name}} </a>
													<ul class="sub-menu">
														
														<li>
															<a  style="" href="{{    route('logout') }}"
															onclick="event.preventDefault();
															document.getElementById('logout-form').submit();">{{('Logout')}}
															
														</a>
														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>

													</li>
														
													</ul>												
												</li>
												
											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->
									@else
									<!-- menu-item-list - start -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li class="active">
													<a href="/">Home</a>
												</li>
												<li><a href="/cari">Cari Event</a></li>
												<li>
													<a href="/buat-acara">Buat Event</a>
												</li>
												<li><a href="/acara-saya">Event Saya</a></li>
												<li><a <a href="/undangan-saya">Tiket</a></li>
												
												
												<li class="menu-item-has-children">
													
													<a href="" style="color: white;"><i style="color: white;" class="fas fa-user"></i> {{Auth::user()->name}}</a>
													<ul class="sub-menu">
														
														<li>
															<a  style="" href="{{    route('logout') }}"
															onclick="event.preventDefault();
															document.getElementById('logout-form').submit();">{{('Logout')}}
															
														</a>
														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>

													</li>
														
													</ul>												
												</li>

											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->									
									@endif
									
								</div>
							</div>
						</div>
						<!-- mainmenu-wrapper - end -->

					</div>
				</div>
			</div>
			<!-- header-bottom - end -->
		</header>

		<style>
			#form-messages {
				background-color: rgb(255, 232, 232);
				border: 1px solid red;
				color: red;
				display: none;
				font-size: 12px;
				font-weight: bold;
				margin-bottom: 10px;
				padding: 10px 25px;	
				max-width: 250px;
			}
		</style>
		<!-- Modal awal -->
		<div class="modal fade login" id="loginModal">
			<div class="modal-dialog login animated">
				<div class="modal-content text-center"><br>
					<div class="container text-center">
						<div class="modal-header text-center">
							<h4 align="center" class="modal-title text-center" style="color: orange; text-transform: uppercase; font-weight: 600;" >LOGIN</h4>
						</div>
					</div>
					<div class="modal-body">
						<div id="box1" class="box">
							<div class="content">
								@if(session('id')==10)
								<p  align="center" style="color: red; ">PASSWORD SALAH</p>
								@endif
								@if(session('id')==11)
								<p align="center" style="color: red;">EMAIL TIDAK TERDAFTAR</p>
								@endif
								<div class="form loginBox">
									<div align="center">
									<ul id="form-messages">
										<li>Generic Error #1</li>
									</ul>
									</div>
									<form id="masuk" name="masuk" method="POST" action="{{route('login')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input id="email" class="form-control" type="text" placeholder="Email" name="email" required=""><br>
										<input id="password" class="form-control" type="password" placeholder="Password" name="password" required=""><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Login</button>
										</div>
									</form>									
								</div>
								
							</div>
						</div>
						<div id="box2" class="box">
							<div class="content registerBox" style="display:none;">
								@if(session('id')==1)
								<p align="center" style="color: red;">EMAIL TELAH TERDAFTAR</p>
								@endif
								@if(session('id')==2)
								<p align="center" style="color: red;">PASSWORD TIDAK COCOK</p>		
								@endif
								<div class="form">
									<form method="POST" html="{:multipart=>true}" data-remote="true" action="{{route('register')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input id="name" class="form-control" type="text" minlength="4" maxlength="100" placeholder="Nama" name="name" required><br>
										<input id="alamat" class="form-control" type="text" minlength="4" maxlength="255" placeholder="Alamat" name="alamat" required><br>
										<input id="email" class="form-control" type="text" placeholder="Email" name="email" required><br>
										<input id="password" class="form-control" type="password" minlength="6" maxlength="15" placeholder="Password" name="password" required><br>
										<input id="password_confirmation" class="form-control" type="password" minlength="6" maxlength="15" placeholder="Repeat Password" name="password_confirmation" required><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Buat Akun</button>
										</div>
									</form>
								
								</div>
							</div>
						</div>
					</div>
					<div id="box3" class="box">
							<div class="content">
								@if(session('id')==10)
								<h3  align="center" style="color: red; text-transform: capitalize;">Password Salah</h3>
								@endif
								@if(session('id')==11)
								<h3 align="center" style=" text-transform: capitalize; color: red;">Email Tidak Terdaftar</h3>
								@endif
								<div class="form loginBox">
									<form id="masuk" name="masuk" method="POST" action="{{route('login')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input id="email" class="form-control" type="text" placeholder="Email" name="email" required=""><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Reset Password</button>
										</div>
									</form>									
								</div>
								
							</div>
						</div>
					<div class="modal-footer">
						<div class="forgot login-footer">
                            <span>
                            	Lupa Password?
                                 <a id="regis" href="javascript: showLupaForm();">Reset</a>
                            </span>
                            <span>
                            	Belum punya akun?
                                 <a id="regis" href="javascript: showRegisterForm();"><strong>Daftar Disini!</strong></a>
                            </span>
							
						</div>
						<div class="forgot register-footer" style="display:none">
							<span>Sudah punya akun?</span>
							<a href="javascript: showLoginForm();"><strong>Login</strong></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- header-section - end
		================================================== -->


		<!-- altranative-header - start
		================================================== -->
		<div class="header-altranative">
			<div class="container">
				<div class="logo-area float-left">
					<a href="index-1.html">
						<h5  class="title" style="color: white; margin-top: 8px;">
										KOTAK EVENT
						</h5>
						{{-- <img src="{{asset('assets/assets/images/1.site-logo.png')}}" alt="logo_not_fund"> --}}
					</a>
				</div>

				<button type="button" id="sidebarCollapse" class="alt-menu-btn float-right">
					<i class="fas fa-bars"></i>
				</button>
			</div>

			<!-- sidebar menu - start -->
			<div class="sidebar-menu-wrapper">
				<div id="sidebar" class="sidebar">
					<span id="sidebar-dismiss" class="sidebar-dismiss">
						<i class="fas fa-arrow-left"></i>
					</span>

					<div class="sidebar-header">
						<a href="#!">
							<h3  class="title" style="color: white;">
										KOTAK EVENT
							</h3>
							{{-- <img src="" alt="logo_not_found"> --}}
						</a>
					</div>


					<!-- main-pages-links - start -->
					<div class="menu-link-list main-pages-links">
						@if(!Auth::user())
						<ul>
							<li>
								<a href="/">
									<span class="icon"><i class="fas fa-home"></i></span>
									 Home
								</a>
							</li>
							<li>
								<a href="/cari">
									<span class="icon"><i class="fas fa-search"></i></span>
									Cari Event
								</a>
							</li>
							<li>
								<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">
									<span class="icon"><i class="fas fa-file"></i></span>
									Buat Event
								</a>
							</li>
							
						</ul>
						
						@else
						<ul>
							<li>
								<a href="/">
									<span class="icon"><i class="fas fa-home"></i></span>
									 Home
								</a>
							</li>
							<li>
								<a href="/cari">
									<span class="icon"><i class="fas fa-search"></i></span>
									Cari Event
								</a>
							</li>
							<li>
								<a href="/buat-acara">
									<span class="icon"><i class="fas fa-file"></i></span>
									Buat Event
								</a>
							</li>
							<li>
								<a href="/acara-saya">
									<span class="icon"><i class="fas fa-boxes"></i></span>
									Event Saya
								</a>
							</li>
							<li>
								<a href="/undangan-saya">
									<span class="icon"><i class="fas fa-boxes"></i></span>
									Tiket
								</a>
							</li>
						</ul>
						@endif


					</div>
										
					@if(!Auth::user())
					<div class="login-btn-group">
						<ul>

							<li>
								<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openRegisterModal();">Register</a>
							</li>
							<li>
								<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">Login</a>				
							</li>
							
						</ul>
					</div>
					@endif
					<!-- login-btn-group - end -->
					@if(Auth::user())
					<div class="login-btn-group" >
						<ul>
							<li>
								<a>{{Auth::user()->name}}</a>				
							</li>
							<li><a  href="{{    route('logout') }}" class="login-modal-btn" 
													onclick="event.preventDefault();
													document.getElementById('logout-form').submit();">
													<i class="fas fa-user"></i> {{ __('Logout') }}
												</a></li>
												<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
													@csrf
												</form>
							
						</ul>
					</div>
					@endif

					<!-- social-links - start -->
					<div class="social-links">
						{{-- <h2 class="menu-title">get in touch</h2> --}}
						<div class="mb-15">
							<h3 class="contact-info">
								<i class="fas fa-phone"></i>
								111-2222-9999
							</h3>
							<h3 class="contact-info" style="text-transform: lowercase;">
								<i class="fas fa-envelope"></i>
								kotakevents@gmail.com
							</h3>
						</div>
						<ul>
							<li>
								<a href="#!"><i class="fab fa-facebook-f"></i></a>
							</li>
							<li>
								<a href="#!"><i class="fab fa-twitter"></i></a>
							</li>
							<li>
								<a href="#!"><i class="fab fa-twitch"></i></a>
							</li>
							<li>
								<a href="#!"><i class="fab fa-google-plus-g"></i></a>
							</li>
							<li>
								<a href="#!"><i class="fab fa-instagram"></i></a>
							</li>
						</ul>
						<a href="" class="contact-btn">Hubungi Kami</a>
					</div>
					<!-- social-links - end -->

					<div class="overlay"></div>
				</div>
			</div>
			<!-- sidebar menu - end -->
		</div>
		<!-- altranative-header - end
		================================================== -->
		<!-- breadcrumb-section - start
		================================================== -->
		<section id="breadcrumb-section" class="breadcrumb-section  clearfix" style="margin:0; ">
			<div class="jarallax"  style="background-image: url({{asset('images/18.jpg')}});"{{-- style="background-image: linear-gradient(white, grey);" --}}>

				<div class="overlay-black" >
					<div class="container" >
						<div class="row justify-content-center" style="margin-top: -10px; " >
							<div class="col-lg-6 col-md-12 col-sm-12">

								<div class="breadcrumb-list">
									<ul>
										<li >
											{{-- <img style="height: 100px;"  src="{{asset('images/10.png')}}"> --}}
										</li>
										
										
									</ul>
								</div>

								<!-- breadcrumb-title - start -->
								<div class="breadcrumb-title text-center mb-50">
									<span style="background-color: black; font-size: 22.5px; color: orange;" class="sub-title">Buat Event Terbaikmu</span>
									<span style="background-color: black; font-size: 22.5px; color: orange;" class="sub-title">Sekarang</span>
									{{-- <span class="sub-title">Lebih Sederhana</span> --}}
									<h2 class="big-title"></h2>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- breadcrumb-section - end

		<!-- absolute-eventmake-section - start
		================================================== -->
		<div id="absolute-eventmake-section" class="absolute-eventmake-section  bg-gray-light clearfix"  >
			<div class="eventmaking-wrapper"   >

				

				<div class="tab-content" >
					<div id="conference" class="tab-pane fade in active show" >
						<form method="post" action="/cari" >
							{{csrf_field()}}
							<ul >
								<li>
									<input style="font-size: 14px; text-align: center;" type="text" name="kunci" placeholder="Kode Event" required="">
									<input type="hidden" name="kategori" value="private">
								</li>															
								{{-- <li>
									<select name="kategori">
										
										<option value="public"><p style="font-size: 10px;">Nama Event</p></option>
										<option value="private">Kode Event</option>
										
									</select>
								</li> --}}
								<li>
									<button type="submit" value="cari" style="background: #d9534f;" class="kastem-btn" >Cari</button>
								</li>

							</ul>
						</form>
					</div>
				</div>
				
			</div>
		</div>

		<!-- absolute-eventmake-section - end
		================================================== -->
		<br><br>

		<!-- upcomming-event-section - start
		================================================== -->
		<style scoped>
				@media screen and (max-width: 480px) {
			.upcomming-event-section .upcomming-event-carousel .item .event-item .event-content {
				    padding-bottom: 0px;
				}
			}	
		</style>
		<style scoped>
		@media screen and (max-width: 480px) {			
			.upcomming-event-section .upcomming-event-carousel .item .event-item .event-content {
					
				
				    padding-top: 0px;
				    				    
				}


			.upcomming-event-section .upcomming-event-carousel .item .event-item .event-image {
				height: 105px; }
				
				.upcomming-event-section .upcomming-event-carousel .item .event-item .event-content .event-post-meta{
					/*display: none;*/

					margin: 0;
				}
				.upcomming-event-section .upcomming-event-carousel .item .event-item .event-content .event-title{
					/*display: none;*/

					margin: 0;
				}
				.upcomming-event-section .upcomming-event-carousel .item .event-item .event-content .event-title h3.title{
					margin: 0; 

				}
				.upcomming-event-section .upcomming-event-carousel .item .event-item .event-content .custom-btn{
				    /*font-size: 10px;
				    padding: 0.4em;*/
				    display: none;

				}
			}
		</style>
		<div style="display: inline block;">
		<section id="upcomming-event-section" class="upcomming-event-section sec-ptb-76 clearfix"  >
			<div class="container">

				<!-- section-title - start -->
				<div class="section-title text-center mb-50" style="margin-bottom: 0px;" >
					<small class="sub-title" >Event Minggu Ini</small>
					
				</div>
				<!-- section-title - end -->

				<!-- upcomming-event-carousel - start -->
				<div id="upcomming-event-carousel" class="upcomming-event-carousel owl-carousel owl-theme">

					<!-- item - start -->
					@foreach($seven as $sev)
					<div class="item">
						<a href="{{route('detail.acara',['id'=>$sev->id])}}">
						<div class="event-item">

							{{-- <div class="countdown-timer">
								<ul class="countdown-list" data-countdown="{{$sev->jadwal_mulai->format('Y/m/d')}}"></ul>
							</div> --}}

							<div class="event-image">
								
									<img   src="https://kotakevents.com/fotoupload/{{$sev->foto}}" alt="Image_not_found">	
								
								
								<div class="post-date">
									<span class="date">{{$sev->jadwal_mulai->format('d')}}</span>
									<small class="month">{{$sev->jadwal_mulai->format('M')}}</small>
								</div>
							</div>

							<div class="event-content" >
								<div class="event-title mb-30">
									<h3 class="title">
										{{$sev->nama}}
									</h3>

									{{-- @if($sev->jenis_daftar == 'bayar')
									<p class="ticket-price yellow-color"> </p>
									@else
									<p class="ticket-price yellow-color">Free</p>
									@endif --}}
								</div>
								<div class="event-post-meta ul-li-block mb-30" style="padding-top: 0;">
									<ul >
										<li style="color: grey;">
											<span class="icon">
												<i class="far fa-clock"></i>
											</span>
											{{$sev->jadwal_mulai->format('h:i')}} WIB - {{$sev->jadwal_selesai->format('h:i')}} WIB
										</li>
										<li style="color: grey;">
											<span class="icon">
												<i class="fas fa-map-marker-alt"></i>
											</span>
											{{$sev->lokasi}}
										</li>
									</ul>
								</div>
								<div align="right">
								<a href="{{route('detail.acara',['id'=>$sev->id])}}" class="custom-btn">
									Lihat Event
								</a>
								</div>
							</div>

						</div>
					</a>
					</div>
					@endforeach
					<!-- item - end -->


				</div>
				<!-- upcomming-event-carousel - end -->

			</div>
		</section>
		</div>
		<!-- upcomming-event-section - end
		================================================== -->
<!-- advertisement-section - start
		================================================== -->
		{{-- <section id="advertisement-section" class="advertisement-section clearfix" style="">
			<div class="container">
				<div class="advertisement-content text-center">

					<h2 class="title-large white-color">Are you ready to make <strong>your Own Special Events?</strong></h2>
					<p class="mb-31">“Mulailah buat acara Anda”</p>
					@if(Auth::user())
					<a href="/buat-acara"><button class="custom-btn">Buat Event</button></a>
					@else
					<a href="#login-modal" class="login-modal-btn"><button class="custom-btn">Buat Event</button></a>
					@endif
					
					
				</div>
			</div>
		</section> --}}
		<!-- advertisement-section - end
		================================================== -->



		<!-- event-section - start
		================================================== -->

		<section id="event-section" class="event-section sec-ptb-100 bg-gray-light clearfix">
			<div class="container">

				<div class="section-title text-center mb-50" style="margin-bottom: 0px;" >
					<small class="sub-title" >List Event</small>
					
				</div>

				<!-- tab-content - start -->
				<div class="tab-content">

					<!-- conference-event - start -->
					{{-- <div id="conference-event" class="tab-pane fade">
						<div class="row">
							@foreach($top8 as $top)
							<!-- event-item - start -->
							<div class="col-lg-4 col-md-6 col-sm-12">
								<div class="event-item2 clearfix">

									<!-- event-image - start -->
									<div class="event-image"  >
										<div class="post-date">
											<span class="date">{{$top->jadwal_mulai->format('d')}}</span>
											<small class="month">{{$top->jadwal_mulai->format('M')}}</small>
										</div>
										<img style="height: 200px; width: 370px;"  src="{{asset('fotoupload/'.$top->foto)}}" alt="Image_not_found">
									</div>
									<!-- event-image - end -->

									<!-- event-content - start -->
									<div class="event-content">
										<div class="event-title mb-15">
											<h3 class="title">
												{{$top->nama}}
											</h3>
											<span class="ticket-price yellow-color">{{$top->jenis_daftar}}</span>
										</div>
										<div class="event-post-meta ul-li-block mb-30">
											<ul>
												<li>
													<span class="icon">
														<i class="far fa-clock"></i>
													</span>
													{{$top->jadwal_mulai->format('h:i')}} - {{$top->jadwal_selesai->format('h:i')}}
												</li>
												<li>
													<span class="icon">
														<i class="fas fa-map-marker-alt"></i>
													</span>
													{{$top->lokasi}}
												</li>
											</ul>
										</div>
										<a href="{{route('detail.acara',['id'=>$top->id])}}" class="tickets-details-btn">
											Lihat Event
										</a>
									</div>
									<!-- event-content - end -->

								</div>
							</div>
							@endforeach
							<!-- event-item - end -->

							

							

							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="pagination ul-li clearfix">
									<ul>
										<li>
											<a href="{{route('detail.acara',['id'=>$top->id])}}" class="custom-btn">
												Selengkapnya
											</a>
										</li>
									</ul>
								</div>
							</div>

						</div>
					</div> --}}
					<!-- conference-event - end -->

					<!-- musical-event - start -->
					<div id="musical-event" class="tab-pane fade in active show">
						<div class="row">
							@foreach($top8 as $top)
							<!-- event-item - start -->
							<div class="col-lg-6 col-md-12 col-sm-12">
								<div class="event-item clearfix">

									<!-- event-image - start -->
									<div class="event-image">
										<div class="post-date">
											<span class="date">{{$top->jadwal_mulai->format('d')}}</span>
											<small class="month">{{$top->jadwal_mulai->format('M')}}</small>
										</div>
										<img src="https://kotakevents.com/fotoupload/{{$top->foto}}" alt="Image_not_found">
									</div>
									<!-- event-image - end -->

									<!-- event-content - start -->
									<div class="event-content">
										<div class="event-title mb-15">
											<h3 class="title">
												{{$top->nama}}
											</h3>
											@if($top->jenis_daftar == 'bayar')
											<span class="ticket-price yellow-color">Berbayar</span>
											@else
											<span class="ticket-price yellow-color">Free</span>
											@endif
										</div>
										<div class="event-post-meta ul-li-block mb-30">
											<ul>
												<li>
													<span class="icon">
														<i class="far fa-clock"></i>
													</span>
													{{$top->jadwal_mulai->format('h:i')}} WIB - {{$top->jadwal_selesai->format('h:i')}} WIB
												</li>
												<li style="max-height: 25px; text-overflow: ellipsis; overflow: hidden;">
													<span class="icon">
														<i class="fas fa-map-marker-alt"></i>
													</span>
													{{$top->lokasi}} 
												</li>
											</ul>
										</div>
										<a href="{{route('detail.acara',['id'=>$top->id])}}" class="tickets-details-btn">
											Lihat Event
										</a>
									</div>
									<!-- event-content - end -->

								</div>
							</div>
							<!-- event-item - end -->

							@endforeach

							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="pagination ul-li clearfix">
									<ul>
										<li class="page-item prev-item">
											<a href="/cari" class="custom-btn">
												Selengkapnya
											</a>
										</li>
									</ul>
								</div>
							</div>

						</div>
					</div>
					<!-- musical-event - end -->

					
				</div>
				<!-- tab-content - end -->

			</div>
		</section>
		<!-- event-section - end
		================================================== -->
		<br><br>



		<!-- footer-section2 - start
		================================================== -->
		<footer id="footer-section" class="footer-section footer-section2 clearfix">
			<div class="footer-bottom">
				<div class="container">
					<div class="row">

						<!-- copyright-text - start -->
						<div class="col-lg-7 col-md-12 col-sm-12">
							<div class="copyright-text">
								<p  style="font-size: 12px; color: white;" class="m-0">©2019 <a href="/" class="site-link">Kotak Event</a> </p>
							</div>
						</div>
						<!-- copyright-text - end -->

						<!-- footer-menu - start -->
						<div class="col-lg-5 col-md-12 col-sm-12">
							<div class="footer-menu">
								<ul>
									{{-- <li><a href="contact.html">Contact us</a></li>
									<li><a href="about.html">About us</a></li>
									<li><a href="#!">Site map</a></li>
									<li><a href="#!">Privacy policy</a></li> --}}
								</ul>
							</div>
						</div>
						<!-- footer-menu - end -->

					</div>
				</div>
			</div>

		</footer>
		<!-- footer-section2 - end
		================================================== -->










		<!-- fraimwork - jquery include -->
		
		{{-- <script src="{{asset('assets/assets/js/popper.min.js')}}"></script> --}}
		<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>

		<!-- carousel jquery include -->
		<script src="{{asset('assets/assets/js/slick.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/owl.carousel.min.js')}}"></script>

		<!-- map jquery include -->
		{{-- <script src="{{asset('assets/assets/js/gmap3.min.js')}}"></script> --}}
		{{-- <script src="https://maps.google.com/maps/api/js?key=AIzaSyC61_QVqt9LAhwFdlQmsNwi5aUJy9B2SyA"></script> --}}

		<!-- calendar jquery include -->
		<script src="{{asset('assets/assets/js/atc.min.js')}}"></script>

		<!-- others jquery include -->
		<script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/isotope.pkgd.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/jarallax.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
		{{-- <script src="{{asset('assets/assets/js/lightcase.js')}}"></script> --}}

		<!-- gallery img loaded - jqury include -->
		<script src="{{asset('assets/assets/js/imagesloaded.pkgd.min.js')}}"></script>

		<!-- multy count down - jqury include -->
		{{-- <script src="{{asset('assets/assets/js/jquery.countdown.js')}}"></script> --}}



		<!-- custom jquery include -->
		<script src="{{asset('assets/assets/js/custom.js')}}"></script>
		<script src="{{asset('assets/js/login-register.js')}}"></script>





	</body>
</html>