<!DOCTYPE html>
<html>
 
  
  
<body>
<style scoped>


        *{
            margin: 0px;
            padding: 0px;
        }
        body{
            font-family: arial;
        }
        .container {
  margin: 0 auto;
  max-width: 1200px; 
}
        .main{

            margin: 2%;
        }

        .card{

            width: 70%;
            display: inline-block;
            border:9px;
            border-radius: 20px; 
            margin: 2%;
            border-style: solid;
            border-color: #B03060;
            /*overflow: hidden;*/
            /*height: 350px;*/
            padding: 20px;
            box-sizing: border-box;
            border-radius: 10px;
            background: #FBFBFB;
        }


        .image img{
            width: 100%;
            
            border-top-right-radius: 5px;
            border-top-left-radius: 5px;
            
            opacity: 0.1;



        }

        .title{

            text-align: center;
            
            color: black;

        }

        h1{
            font-size: 15px;

        }

        .des{
            padding: 3px;
            text-align: center;
            color: black;
            padding-top: 10px;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        button{
            margin-top: 40px;
            margin-bottom: 10px;
            background-color: white;
            border: 1px solid black;
            border-radius: 5px;
            padding:10px;
            color: black;
        }
        button:hover{
            background-color: black;
            color: white;
            transition: .5s;
            cursor: pointer;
        }

    </style>



    <!--cards -->

    
    
    <div class="card" style="margin-top: 22px; margin-left: 22px;">

      <div class="image">
        <img style=" position: absolute; z-index: 15; margin: 0; height: 130px;" src="{{ public_path('fotoupload/'.$data['foto'])}}" alt="BTS">
      </div>
      <div class="title" style="padding-top: 20px; padding-bottom: 10px;">
        <h1 style="text-transform: uppercase; color: black; font-family:'Open Sans', sans-serif;"><strong>{{$data['nama_event']}}</strong></h1>

      </div>
      
      <div align="center" style="margin-bottom: 0px; margin-top: 10px;">
        <p style="text-transform: capitalize; color: red; font-size: 14px;"><strong>{{$data['nama_user']}}</strong></p>
        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(150)->generate($data['id_scan'])) !!} ">
      </div>
          
      <div align="center" style="padding-bottom: 10px;">
        <p style="font-size: 13px;">{{$data['jadwal_mulai']->format('d - m - Y')}}</p>
           <p style="font-size: 13px;">{{$data['jadwal_mulai']->format('h:i')}} <span style="font-size: 11px;">WIB</span> - {{$data['jadwal_selesai']->format('h:i')}} <span style="font-size: 11px;">WIB</span>
           </p>
            
        
      </div>
      <div align="center" style="padding-bottom: 10px; padding-top: 10px;">
        <p style="font-size: 13px;">{{$data['lokasi']}}</p>
      </div>
      
    </div>
    
    
    <!--cards -->
    
    


</body>
</html>

