<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/stylenav.css')}}"> --}}
        <link rel="stylesheet" type="text/css" href="https://kotakevents.com/css/styleBdua.css">
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/bulma-quickview.min.css')}}"> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/bulma.css')}}"> --}}
		{{-- <link rel="stylesheet" href="{{asset('assets/css/box.css')}}" /> --}}
		

		<!-- fraimwork - css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/flaticon.css')}}"> --}}



		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">


		

		<!-- custom css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">
	


		
		<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
		{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> --}}
		<link rel="stylesheet" type="text/css" href="{{asset('css/tabel.min.css')}}">
		
		{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"> --}}
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('css/hover.dataTables.min.css')}}"> --}}




		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

		{{-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> --}}
		
		<script type="text/javascript" src="{{asset('css/jqueryDatatable2.min.js')}}"></script>
		<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
		<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
		<link type="text/css" rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}">
        {{-- <link type="text/css" rel="stylesheet" href="{{asset('css/bulma.css')}}"> --}}

        <script type="text/javascript" src="{{asset('css/sweetalert2.min.js')}}"></script>
		



	</head>

	
	<body>

	
	
 
		<!-- backtotop - start -->
		<div id="thetop" class="thetop"></div>
		<div class='backtotop'>
			<a href="#thetop" class='scroll'>
				<i class="fas fa-angle-double-up"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		<div id="preloader"></div>
		<!-- preloader - end -->

  <div class="modal" id="modaldata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header border-bottom-0">
            <h4 class="modal-title" id="exampleModalLabel">Data Tamu</h4>

        </div>
        <input type="hidden" name="data_id" value="">
        
        <div class="modal-body">
            <div class="form-group">
                <label for="nama_tamu">Nama</label>
                <input type="text" name="nama_tamu" class="form-control" aria-describedby="emailHelp" required="">



            </div>
            <div class="form-group">
                <label for="email_tamu">Email</label>
                <input type="email" name="email_tamu" class="form-control"  aria-describedby="emailHelp" required="" > 
            </div>
            <div class="form-group">
                <label for="alamat_tamu">Alamat</label>
                <input type="text" name="alamat_tamu" class="form-control"  aria-describedby="emailHelp" required="">
            </div>
            <div class="form-group">
                <label for="kategori">Kategori</label>
                <select name="kategori" required="">

                    @foreach(\App\Category::select('id','id_event', 'nama_kategori')->get() as $kat) 
                   <option value="{{$kat->id}}">{{$kat->nama_kategori}}</option>
                   @endforeach

                </select>
            </div>



        </div>
        <div class="modal-footer border-top-0 d-flex justify-content-right">

            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" id="button" onclick="saveAction(this)" class="btn btn-info" data-dismiss="modal">Simpan</button>

        </div>

    </div>
</div>
</div>		




		<!-- header-section - start
		================================================== -->
		<header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
			
			<!-- header-bottom - start -->
			<div class="header-bottom" >
				<div class="container" >
					<div class="row" style="margin-right: : -1px; margin-left: -1px;">

						<!-- site-logo-wrapper - start -->
						<div class="col-lg-3" style="padding-left: 0px;">
							<div class="site-logo-wrapper">
								<a href="index-1.html" class="logo">
									<h3  class="title" style="color: white; margin-top: 8px; font-weight: 700;">
                                        KOTAK EVENT

                                    </h3>
									{{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
								</a>
							</div>
						</div>
						<!-- site-logo-wrapper - end -->

						<!-- mainmenu-wrapper - start -->
						<div class="col-lg-9">
							<div class="mainmenu-wrapper">
								<div class="row">														
									<!-- menu-item-list - start -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li >
													<a href="/">Home</a>
												</li>
												<li >
													<a href="/cari">Cari Event</a>
												</li>
												<li>
													<a href="/buat-acara">Buat Event</a>
												</li>
												<li><a href="/acara-saya">Event Saya</a></li>
												<li class="active"><a <a href="/undangan-saya">Tiket</a></li>
												
												<li class="menu-item-has-children">
													
													<a href="" style="color: white;"><i style="color: white;" class="fas fa-user"></i> {{Auth::user()->name}}</a>
													<ul class="sub-menu">
														
														<li>
															<a  style="" href="{{    route('logout') }}"
															onclick="event.preventDefault();
															document.getElementById('logout-form').submit();">{{('Logout')}}
															
														</a>
														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>

													</li>
														
													</ul>												
												</li>

											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->									
									
									
								</div>
							</div>
						</div>
						<!-- mainmenu-wrapper - end -->

					</div>
				</div>
			</div>
			<!-- header-bottom - end -->
		</header>

		
		<!-- header-section - end
		================================================== -->
		<!-- altranative-header - start
		================================================== -->
				@include('layouts.altranative-header')
		<!-- altranative-header - end
		================================================== -->

		

		<!-- cari-event-section - start
		================================================== -->
		<!-- booking-section - start
		================================================== -->
		<style>
        
			.dtr-data{
				display: inline-block;
				float: right;
			}
			
			p{
				margin-bottom: 0px;
                font-weight: 700;
                /*color: white;*/
                
                font-size: 16px;
			}

			td{
				text-align: center;
			}
			.sorting_1{
                display: none;
            }
            h4{
                font-weight: 900;
                font-size: 17px;
                font-family: "Roboto", sans-serif;
                max-width: 250px;
                min-width: 250px;

                max-height: 18px;
                min-height: 18px;
                text-overflow: ellipsis; overflow: hidden;
            }
            h5{
                font-weight: 900;                
                font-family: "Roboto", sans-serif;              
            }
            h6{
            	            
            	max-height: 15px;
                min-height: 15px;
                text-overflow: ellipsis; overflow: hidden;	
            }
		</style>
		<section id="main" class="wrapper sec-ptb-75">
		<div class="container">
				<a href="{{ url('/')}}"><p>Home ></p></a>
				<a href="{{ url('/acara-saya')}}"><p>Events</p></a>
				{{-- <a href="{{ url('/acara/{id}')}}">Event 1</a> --}}
		</div>
        <br>
				<div class="container">
					<div align="center">
						<h5 align="center"style="font-size: 25px; color: #878787;">TIKET SAYA</h5>
                    </div>
					

<div id="page1" style="display: block;">
    
    <div class="content">
        <table class="table is-hoverable is-fullwidth cards" id="primary_table">
            <thead>
                <tr>
                	<th>No</th>               
                    <th>Image</th>
                    <th>Action</th>
                    <th>Pendaftaran</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
				</div>

			</section>
		<!-- booking-section - end
		================================================== -->
<style type="text/css">
    .img{

    }
</style>
	<script>
        var file = null;
        var primary_table = $('#primary_table').DataTable({
        responsive: true,                
        processing: true,
        serverSide: true,
        ajax: {
            url:  'tiket-list',
            type: 'get'
        },
        columns: [
        	{ data: 'nama', name: 'nama',
	        	render: function(data) {
	                    return '<p></p>';
	                }

        	 },        	
            { data: 'image', name: 'image', searchable: false, orderable: false, 
                render: function(data) {
                    return '<a style="color:#d9534f;" href="acara/'+data.id+'" data-id="'+data.id+'" target="_blank">'+
                    '<h4 style="text-transform: uppercase;">'+data.nama+'</h4>'+
                    '</a>'+
                    '<figure class="image" style="margin: 0; width:100px;">'+
                        '<img src="'+data.src+'" style="width:400px; height:500px;" data-target="#zoom-image">'+
                        '</figure>'
                        ;
                }
            },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {   
                    if(data.status=='Belum Dikonfirmasi'){                    
                    return '<p style="padding-top:4em;padding-left:1em; text-transform: uppercase; color:red; font-size:18px;" >Belum Dikonfirmasi</p>';
                }else if(data.status=='Menunggu Pembayaran'){
                    return '<p style="padding-top:4em;padding-left:1em; text-transform: uppercase; color:red; font-size:18px;" >Menunggu Pembayaran</p>'+
                    '<div style="padding-bottom:0.5em;"><a href="acara/'+data.id+'" >'+                    
                    '    <span class="btn btn-primary btn-sm"><p style="font-size:13px; color:white;">Upload Bukti Pembayaran</p></span>'+
                    '</a></div>';
                }
                else{
                    return '<div style="max-width:215px;min-width:215px; ; padding-bottom:0.5em; padding-top:2em;">'+
                    '    <div><span><p>'+data.jadwal_mulai+'</p></span></div>'+
                    '</div>'+
                    '<div style=" padding-bottom:0.5em;">'+
                    '    <div><span><p style="font-size:14px; font-weight:500;">'+data.waktu+' WIB</p></span></div>'+
                    '</div>'+
                    '<div style=" padding-bottom:0.5em;">'+
                    '  <span><h6 style="font-size:14px; font-weight:500;">'+data.lokasi+'</h6></span>'+
                    '</div>'+
                    '<form method="POST" action="{{URL::to('viewpdf')}}">{{ csrf_field() }}<input type="hidden" name="ev" value="'+data.id_event+'"><input type="hidden" name="us" value="{{Auth::user()->id}}"><input type="hidden" name="sc" value="'+data.id_scan+'"><button  type="submit" style="background-color: #28a745;" class="kastem2-btn">UNDUH TIKET</button></form>';
             	   }    
                }
            },            
            
        ]
    });
    
primary_table.on( 'draw', function () {
        
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            
        } );
    } ).draw();
  
  function tutupPendaftaran(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "get",
                    url: 'event-tutup',
                    data: {
                       id: item.attr('data-id')
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                           
                            swal('Berhasil!', result.message, 'success')
                            primary_table.ajax.reload(null, false);
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }


  function bukaPendaftaran(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "get",
                    url: 'event-buka',
                    data: {
                       id: item.attr('data-id')
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                           
                            swal('Berhasil!', result.message, 'success')
                            primary_table.ajax.reload(null, false);
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }




     $(document).on('click', '.tabs li', function(){
        var item = $(this);
        var target = item.attr('data-target');
        $('.tabs li').each(function() {
            $(this).removeClass('is-active');
            $($(this).attr('data-target')).hide();
        });

        item.addClass('is-active');
        $(target).show();
    });
   

        </script>   
		<br><br><br>
		@include('layouts.footer')




		<!-- cari-event-section - end
		================================================== -->




		

		
		
		<script src="{{asset('assets/assets/js/atc.min.js')}}"></script>

		<!-- others jquery include -->
		<script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
		
		
		<script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
	

		<!-- custom jquery include -->
		<script src="{{asset('assets/assets/js/custom.js')}}"></script>
		

		



		<script>
			$(document).ready(function() {
				var table = $('#example').DataTable( {
					
					responsive: true
				} );
			} );
		</script>

		<script>

			var table;
			$(document).ready(function() {
				table = $('#example').DataTable();
			} );
			$('#example').on("click", "button", function(){
				console.log($(this).parent());
				table.row($(this).parents('tr')).remove().draw(false);
				
			});
			function nonaktifkan(klik,id){
				window.location.reload();			
				$.get("{{route('nonaktifkan.acara',['id'=>''])}}/"+id, function(data, status){
				});
				
			}
			function aktifkan(klik,id){
				window.location.reload();			
				$.get("{{route('aktifkan.acara',['id'=>''])}}/"+id, function(data, status){
				});
				
			}
		
		</script>


	</body>
</html>