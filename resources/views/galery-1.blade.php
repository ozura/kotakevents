<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		
		
		<script src="{{asset('assets/js/dropzone.js')}}"></script>
		<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
		

		
		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">

		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">


		
		
		
		
		
		

	</head>


	<body class="default-header-p">




		
		<!-- backtotop - start -->
		<div id="thetop" class="thetop"></div>
		<div class='backtotop'>
			<a href="#thetop" class='scroll'>
				<i class="fas fa-angle-double-up"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		<div id="preloader"></div>
		<!-- preloader - end -->




		<!-- header-section - start
		================================================== -->
<header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
			
			<!-- header-bottom - start -->
			<div class="header-bottom" >
				<div class="container" >
					<div class="row" style="margin-right: : -1px; margin-left: -1px;">

						<!-- site-logo-wrapper - start -->
						<div class="col-lg-3" style="padding-left: 0px;">
							<div class="site-logo-wrapper">
								<a href="index-1.html" class="logo">
										<h3  class="title" style="color: white; margin-top: 8px; font-weight: 700;">
										KOTAK EVENT

									</h3>
									{{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
								</a>
							</div>
						</div>
						<!-- site-logo-wrapper - end -->

						<!-- mainmenu-wrapper - start -->
						<div class="col-lg-9">
							<div class="mainmenu-wrapper">
								<div class="row">														
									<!-- menu-item-list - start -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li >
													<a href="/">Home</a>
												</li>
												<li >
													<a href="/cari">Cari Event</a>
												</li>
												<li>
													<a href="/buat-acara">Buat Event</a>
												</li>
												<li class="active"><a href="/acara-saya">Event Saya</a></li>
												<li><a <a href="/undangan-saya">Tiket</a></li>
												
												<li class="menu-item-has-children">
													
													<a href="" style="color: white;"><i style="color: white;" class="fas fa-user"></i> {{Auth::user()->name}}</a>
													<ul class="sub-menu">
														
														<li>
															<a  style="" href="{{    route('logout') }}"
															onclick="event.preventDefault();
															document.getElementById('logout-form').submit();">{{('Logout')}}
															
														</a>
														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>

													</li>
														
													</ul>												
												</li>

											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->									
									
									
								</div>
							</div>
						</div>
						<!-- mainmenu-wrapper - end -->

					</div>
				</div>
			</div>
			<!-- header-bottom - end -->
		</header>

		<!-- header-section - end
		================================================== -->





		<!-- altranative-header - start
		================================================== -->
		
		<!-- altranative-header - end
		================================================== -->

		<div class="container" style="padding-top: 1.5em;">							
			<a  href="{{ url('/')}}"><p>Home ></p></a>
			<a href="/acara-saya"><p>Event ></p></a>
			<a href="/acara/{{$detail->id}}"><p>{{$detail->nama}} ></p></a>
			<a href="/galery/{{$detail->id}}"><p>Upload Gambar</p></a>
		
		</div> 

		<!-- booking-section - start
		================================================== -->
		<section id="main" class="wrapper sec-ptb-101">
				<div class="container text-center">

					<header class="major special">
						<h2>Upload Gallery</h2>
						<p><a href="{{route('detail.acara',['id'=>$id])}}" >Liat Detail Acara</a></p>
					</header>

					<form action="{{route('galery.post',['id'=>$id])}}" method="post" enctype="multipart/form-data" class="dropzone">
						{{csrf_field()}}
						<div class="fallback" style="border: 2px solid #1b1e21;border-radius: 5px">
							<input name="file" type="file" multiple />
						</div>
					</form>

					</div>
				</div>
			</section>

		<!-- booking-section - end
		================================================== -->





		<!-- default-footer-section - start
		================================================== -->
		<br><br><br>
		@include('layouts.footer')
		<!-- default-footer-section - end
		================================================== -->













				<!-- fraimwork - jquery include -->
		<script src="{{asset('assets/assets/js/jquery-3.3.1.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/popper.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>

		<!-- carousel jquery include -->
		<script src="{{asset('assets/assets/js/slick.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/owl.carousel.min.js')}}"></script>

		<!-- map jquery include -->
		<script src="{{asset('assets/assets/js/gmap3.min.js')}}"></script>
		<script src="http://maps.google.com/maps/api/js?key=AIzaSyC61_QVqt9LAhwFdlQmsNwi5aUJy9B2SyA"></script>



		<!-- others jquery include -->
		<script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/isotope.pkgd.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/jarallax.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>		

		
		<!-- custom jquery include -->
		<script src="{{asset('assets/assets/js/custom.js')}}"></script>


		
		<script>
			Dropzone.options.dropzoneElement = {
				maxFiles: 1,
				maxFilesize: 10, // File size in Mb
				acceptedFiles: 'image/*',
				autoProcessQueue: false,
				init: function() {
					this.on("sending", function(file, xhr, formData) {
						formData.append("status", 'new');
						formData.append("user_id", 1);
					});


					this.on("success", function(file, responseText) {
						alert('Success');
					});
					var submitButton = document.querySelector("#btnUpload")
					myDropzone = this;

					submitButton.addEventListener("click", function() {

						/* Check if file is selected for upload */
						if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {
							alert('No file selected for upload');
							return false;
						}
						else {
							/* Remove event listener and start processing */
							myDropzone.removeEventListeners();
							myDropzone.processQueue();

						}
					});


					/* On Success, do whatever you want */
					this.on("success", function(file, responseText) {
						alert('Success');
					});
				}
			}
		</script>


	</body>
</html>