<!DOCTYPE html>
<html>
 
  
  <style>
@page { margin: 0; }
</style>
<body>
<style scoped>


        *{
            margin: 0px;
            padding: 0px;
        }
        body{
            font-family: arial;
            width: 100%; height: 100%;
        }
        .container {
  margin: 0 auto;
  max-width: 1200px; 
}
        .main{

            margin: 2%;
        }

        .card{

            width: 70%;
            display: inline-block;
            border:9px;
            border-radius: 20px; 
            margin: 2%;
            border-style: solid;
            border-color: #B03060;
            /*overflow: hidden;*/
            /*height: 350px;*/
            padding: 20px;
            box-sizing: border-box;
            border-radius: 10px;
  background: #FBFBFB;
  
        }

    
        .image img{
            width: 15%;
            height: 130px;
            /*border-top-right-radius: 5px;
            border-top-left-radius: 5px;*/



        }

        .title{

            text-align: center;
            
            color: black;

        }

        h1{
            font-size: 15px;

        }

        .des{
            padding: 3px;
            text-align: center;
            color: black;
            padding-top: 10px;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        
        .page-break {
            page-break-after: always;}

    </style>



    <!--cards -->

    <?php $i=0?>
    @foreach($event as $post)
    
    {{-- @if( $i % 3 == 0 ){ 
        <div class="page-break"></div> 
    }
    @endif --}}
    
    <div style="page-break-after: always;" >
    <div class="card" style="margin-left: 22px; margin-top: 22px;" >

      <div class="image" >
        {{-- <img style="margin-left: 10px; height: 30px;" src="{{ public_path('images/'.'10.png')}}" alt="BTS"> --}}
      </div>
      <div class="title" style="padding-top: 20px; padding-bottom: 10px;">
        <h1 style="text-transform: uppercase; color: black; font-family:'Open Sans', sans-serif;"><strong>{{$events['nama']}}</strong></h1>

      </div>

      <div align="center" style="margin-bottom: 0px; margin-top: 10px;">
        <p style="text-transform: capitalize; color: red; font-size: 14px;"><strong>{{$event[$i]['nama_tamu']}}</strong></p>
        <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(150)->generate($event[$i]['id_scan'])) }} ">

      </div>
          

      <div align="center" style="padding-bottom: 10px;">
        <p style="font-size: 13px;">{{$events['jadwal_mulai']->format('d - m - Y')}}</p>
           <p style="font-size: 13px;">{{$events['jadwal_mulai']->format('h:i')}} <span style="font-size: 11px;">WIB</span> - {{$events['jadwal_selesai']->format('h:i')}} <span style="font-size: 11px;">WIB</span>
           </p>
            
        
      </div>
      <div align="center" style="padding-bottom: 10px; padding-top: 10px;">
        <p style="font-size: 13px;">{{$events['lokasi']}}</p>
      </div>
      
    </div>
    </div>
    <?php $i++?>
    @endforeach
    
    
    <!--cards -->
    
    


</body>
</html>

