<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		{{-- <link rel="stylesheet" href="{{asset('assets/css/box.css')}}" /> --}}
		

		<!-- fraimwork - css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/flaticon.css')}}"> --}}



		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">


		

		<!-- custom css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">
	

		
		<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
		{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> --}}
		<link rel="stylesheet" type="text/css" href="{{asset('css/tabel.min.css')}}">
		
		{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"> --}}
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('css/hover.dataTables.min.css')}}"> --}}




		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

		{{-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> --}}
		
		<script type="text/javascript" src="{{asset('css/jqueryDatatable.min.js')}}"></script>
		<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
		<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
		<link type="text/css" rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}">
        {{-- <link type="text/css" rel="stylesheet" href="{{asset('css/bulma.css')}}"> --}}

        <script type="text/javascript" src="{{asset('css/sweetalert2.min.js')}}"></script>
		



	</head>

	
	<body>

	
	
 
		<!-- backtotop - start -->
		<div id="thetop" class="thetop"></div>
		<div class='backtotop'>
			<a href="#thetop" class='scroll'>
				<i class="fas fa-angle-double-up"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		<div id="preloader"></div>
		<!-- preloader - end -->
		




		<!-- header-section - start
		================================================== -->
		<header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
			
			<!-- header-bottom - start -->
			<div class="header-bottom" >
				<div class="container" >
					<div class="row" style="margin-right: : -1px; margin-left: -1px;">

						<!-- site-logo-wrapper - start -->
						<div class="col-lg-3" style="padding-left: 0px;">
							<div class="site-logo-wrapper">
								<a href="index-1.html" class="logo">
									<h3  class="title" style="color: white; margin-top: 8px;">
										KOTAK EVENTS

									</h3>
									{{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
								</a>
							</div>
						</div>
						<!-- site-logo-wrapper - end -->

						<!-- mainmenu-wrapper - start -->
						<div class="col-lg-9">
							<div class="mainmenu-wrapper">
								<div class="row">														
									<!-- menu-item-list - start -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li >
													<a href="/">Home</a>
												</li>
												<li >
													<a href="/cari">Cari Event</a>
												</li>
												<li>
													<a href="/buat-acara">Buat Event</a>
												</li>
												<li class="active"><a href="/acara-saya">Event</a></li>
												<li><a <a href="/undangan-saya">Undangan</a></li>
												
												<li class="menu-item-has-children">
													
													<a href="" style="color: white;"><i style="color: white;" class="fas fa-user"></i> {{Auth::user()->name}}</a>
													<ul class="sub-menu">
														
														<li>
															<a  style="" href="{{    route('logout') }}"
															onclick="event.preventDefault();
															document.getElementById('logout-form').submit();">{{('Logout')}}
															
														</a>
														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>

													</li>
														
													</ul>												
												</li>

											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->									
									
									
								</div>
							</div>
						</div>
						<!-- mainmenu-wrapper - end -->

					</div>
				</div>
			</div>
			<!-- header-bottom - end -->
		</header>

		
		<!-- header-section - end
		================================================== -->
		<!-- altranative-header - start
		================================================== -->
				@include('layouts.altranative-header')
		<!-- altranative-header - end
		================================================== -->

		

		<!-- cari-event-section - start
		================================================== -->
		<!-- booking-section - start
		================================================== -->
		<style>
			.dtr-data{
				display: inline-block;
				float: right;
			}
			
			p{
				margin-bottom: 0px;
			}

			td{
				text-align: center;
			}

			
    table.cards {
        background-color: transparent;
    }

    /*--[  This does the job of making the table rows appear as cards ]----------------*/
    .cards tbody img {
        height: 100px;
    }
    .cards tbody tr {
        float: left;
        margin: 10px;
        border: 1px solid #aaa;
        box-shadow: 3px 3px 6px rgba(0,0,0,0.25);
        background-color: white;
    }
    .cards tbody td {
        display: block;
        width: 330px;
        overflow: hidden;
        text-align: left;
    }

    /*---[ The remaining is just more dressing to fit my preferances ]-----------------*/
    .table {
        background-color: #fff;
    }
    .table tbody label {
        display: none;
        margin-right: 5px;
        width: 50px;
    }   
    .table .glyphicon {
        font-size: 20px;
    }

    .cards .glyphicon {
        font-size: 75px;
    }

    .cards tbody label {
        display: inline;
        position: relative;
        font-size: 85%;
        font-weight: normal;
        top: -5px;
        left: -3px;
        float: left;
        color: #808080;
    }
    .cards tbody td:nth-child(1) {
        text-align: center;
    }
			
		</style>
		<section id="main" class="wrapper sec-ptb-75">
		<div class="container">
				<a href="{{ url('/')}}"><p>Home ></p></a>
				<a href="{{ url('/acara-saya')}}"><p>Events</p></a>
				{{-- <a href="{{ url('/acara/{id}')}}">Event 1</a> --}}
		</div>
				<div class="container">
					<header class="major special">
						<h2 align="center"style=" font-family: 'comic sans'; text-transform: uppercase; color: #878787;">DAFTAR EVENT</h2>
					</header>

					<div>
						<table id="primary_table" class="table table-striped table-bordered" style="width:100%" >
							<thead>
							<tr align="center" style="color: white;">
								{{-- <th>Foto</th> --}}
								<th>No</th>
								<th>Nama Event</th>													
								<th>Data Tamu</th>
								<th>Pendaftar</th>														
								<th>Pendaftaran</th>
								<th>Daftar Hadir</th>
								<th>Status</th>
							</tr>
							</thead>

						
						</table>
					</div>
				</div>
			</section>
		<!-- booking-section - end
		================================================== -->

	<script>
        var primary_table = $('#primary_table').DataTable({
        responsive: true,                
        processing: true,
        serverSide: true,
        ajax: {
            url:  'event-list',
            type: 'get'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'nama', name: 'nama', searchable: false, orderable: false,
            	 render: function(data){
                 
                    		return '<a style="color:#d9534f;" href="acara/'+data.id+'" data-id="'+data.id+'" target="_blank">'+
                    '<strong>'+data.nama+'</strong>'+
                    '</a>';	                    	       
                }

             },
              { data: 'datatamu', name: 'datatamu', searchable: false, orderable: false,
            	 render: function(data){
            	 	if(data.status=='Belum Dikonfirmasi'){
                    			return '<a href="datatamu?id='+data.id+'" target="_blank">'+
                    '    <button type="button" disabled class="btn btn-info btn-sm">Kelola</button>'+
                    '</a>';	                    	       
                    	    
                    }else{
                    			return '<a href="datatamu?id='+data.id+'" target="_blank">'+
                    '    <button type="button" class="btn btn-info btn-sm">Kelola</button>'+
                    '</a>';	                    	       
                    }
                 			
                    
                }

             },            
             { data: 'datatamu2', name: 'datatamu2', searchable: false, orderable: false,
            	 render: function(data){
            	 	if(data.status=='Belum Dikonfirmasi'){
                    		return '<a href="datatamu2?id='+data.id+'" target="_blank">'+
                    '    <button type="button" disabled class="btn btn-info btn-sm">Kelola</button>'+
                    '</a>';	                    	   
                    	    
                    }else{
                    	return '<a href="datatamu2?id='+data.id+'" target="_blank">'+
                    '    <button type="button" class="btn btn-info btn-sm">Kelola</button>'+
                    '</a>';	                    	   
                    }
                 			
                    		    
                }

             },                        
            
            { data: 'pendaftaran', name: 'pendaftaran',  searchable: false, orderable: false,

            	render: function(data){
            		if(data.status=='Belum Dikonfirmasi'){
                    		if(data.pendaftaran=='Aktif'){
                    		return '<a onclick="tutupPendaftaran(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <button type="button" disabled class="btn btn-success btn-sm">Dibuka</button>'+
                    '</a>';                   
                    }else{
                    	return '<a  onclick="bukaPendaftaran(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <button type="button" disabled class="btn btn-danger btn-sm">Ditutup</button>'+
                    '</a>';	
                    }
                    	    
                    }else{
                    	if(data.pendaftaran=='Aktif'){
                    		return '<a onclick="tutupPendaftaran(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <button type="button" class="btn btn-success btn-sm">Dibuka</button>'+
                    '</a>';                   
                    }else{
                    	return '<a  onclick="bukaPendaftaran(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="typcn typcn-delete-outline"></i>'+
                    '    </span>'+
                    '    <button type="button" class="btn btn-danger btn-sm">Ditutup</button>'+
                    '</a>';	
                    }
                    }

                    
                    
                     
                }


             },
             { data: 'daftarhadir', name: 'daftarhadir', searchable: false, orderable: false,
            	 render: function(data){
            	 	if(data.status=='Belum Dikonfirmasi'){
                    				return '<a href="daftarhadir?id='+data.id+'" target="_blank">'+
                    '    <button type="button" disabled class="btn btn-info btn-sm">Lihat</button>'+
                    '</a>';	                    	       
                    	    
                    }else{
                    			return '<a href="daftarhadir?id='+data.id+'" target="_blank">'+
                    '    <button type="button" class="btn btn-info btn-sm">Lihat</button>'+
                    '</a>';	                    	       
                    }
                 			
                    
                }

             },
             { data: 'status', name: 'status', searchable: false, orderable: false,

            	  render: function(data){
                    if(data.status=='Belum Dikonfirmasi'){
                    		return '<p style="color:red;">Belum Dikonfirmasi</p>';	
                    	    
                    }else{
                    	return '<p style="color:green;">Ok</p>';	
                    }
                }
        	},
            
        ]
    });

    primary_table.on( 'draw', function () {
        
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();
		

		var labels = [];
		$('#primary_table').find('thead th').each(function() {
		    labels.push($(this).text());
		});
		 
		// Add data-label attribute to each cell
		$('#primary_table').find('tbody tr').each(function() {
		    $(this).find('td').each(function(column) {
		        $(this).attr('data-label', labels[column]);
		    });
		});

		$('#primary_table').find('td').each(function() {
			$(this).removeAttr('data-label');
		});

  
  function tutupPendaftaran(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "get",
                    url: 'event-tutup',
                    data: {
                       id: item.attr('data-id')
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                           
                            swal('Berhasil!', result.message, 'success')
                            primary_table.ajax.reload(null, false);
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }


  function bukaPendaftaran(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "get",
                    url: 'event-buka',
                    data: {
                       id: item.attr('data-id')
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                           
                            swal('Berhasil!', result.message, 'success')
                            primary_table.ajax.reload(null, false);
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }




     $(document).on('click', '.tabs li', function(){
        var item = $(this);
        var target = item.attr('data-target');
        $('.tabs li').each(function() {
            $(this).removeClass('is-active');
            $($(this).attr('data-target')).hide();
        });

        item.addClass('is-active');
        $(target).show();
    });
   

        </script>   
		<br><br><br>
		@include('layouts.footer')




		<!-- cari-event-section - end
		================================================== -->




		

		
		
		<script src="{{asset('assets/assets/js/atc.min.js')}}"></script>

		<!-- others jquery include -->
		<script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
		
		
		<script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
	

		<!-- custom jquery include -->
		<script src="{{asset('assets/assets/js/custom.js')}}"></script>
		

		



		<script>
			$(document).ready(function() {
				var table = $('#example').DataTable( {
					
					responsive: true
				} );
			} );
		</script>

		<script>

			var table;
			$(document).ready(function() {
				table = $('#example').DataTable();
			} );
			$('#example').on("click", "button", function(){
				console.log($(this).parent());
				table.row($(this).parents('tr')).remove().draw(false);
				
			});
			function nonaktifkan(klik,id){
				window.location.reload();			
				$.get("{{route('nonaktifkan.acara',['id'=>''])}}/"+id, function(data, status){
				});
				
			}
			function aktifkan(klik,id){
				window.location.reload();			
				$.get("{{route('aktifkan.acara',['id'=>''])}}/"+id, function(data, status){
				});
				
			}
		
		</script>


	</body>
</html>