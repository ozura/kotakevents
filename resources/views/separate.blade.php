<!DOCTYPE html>
<html>
<head>
    <title>Laravel 5.7 Import Export Excel to database Example - ItSolutionStuff.com</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
</head>
<body>
   
<div class="container">
    <div class="card bg-light mt-3">
        <div class="card-header">
            Laravel 5.7 Import Export Excel to database Example - ItSolutionStuff.com
        </div>
        <div class="card-body">
            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="file" class="form-control">
                <input type="hidden" name="id_event" value="{{$id_event}}">
                <br>
                <button class="btn btn-success">Import User Data</button>
                <a class="btn btn-warning" href="{{ route('export') }}">Export User Data</a>
            </form>
        </div>
        <div align="center">
            {{-- <a href="" type="button" class="kastem2-btn" style="background-color: #FF7E47;">Unduh Undangan</a> --}}

            <a href="{{route('download.tamu',['id'=>$id_event])}}">
                <button  type="button" style="background-color: #FF7E47;" class="kastem2-btn">Unduh Undangan</button>
            </a>
        </div>
    </div>
</div>
   
</body>
</html>