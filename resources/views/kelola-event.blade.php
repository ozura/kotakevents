<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        

        <<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

        <!-- icon css include -->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
        
        <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">


        

        <!-- custom css include -->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">

        
        <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">





        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        
        <link type="text/css" rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}">
        {{-- <link type="text/css" rel="stylesheet" href="{{asset('css/bulma.css')}}"> --}}

        <script type="text/javascript" src="{{asset('css/sweetalert2.min.js')}}"></script>



    </head>

    
    <body>

 
        <!-- backtotop - start -->
        <div id="thetop" class="thetop"></div>
        <div class='backtotop'>
            <a href="#thetop" class='scroll'>
                <i class="fas fa-angle-double-up"></i>
            </a>
        </div>
        <!-- backtotop - end -->

        <!-- preloader - start -->
        {{-- <div id="preloader"></div> --}}
        <!-- preloader - end -->

        <style>
            .modal-open {
                
                overflow: scroll;
                width: 100%;
                padding-right: 0!important;
            }
        </style>
 


        <!-- header-section - start
        ================================================== -->
                <header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
            
            <!-- header-bottom - start -->
            <div class="header-bottom" >
                <div class="container" >
                    <div class="row" style="margin-right: : -1px; margin-left: -1px;">

                        <!-- site-logo-wrapper - start -->
                        <div class="col-lg-3" style="padding-left: 0px;">
                            <div class="site-logo-wrapper">
                                <a href="index-1.html" class="logo">
                                        <h3  class="title" style="color: white; margin-top: 8px; font-weight: 700;">
                                        KOTAK EVENT

                                    </h3>
                                    {{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
                                </a>
                            </div>
                        </div>
                        <!-- site-logo-wrapper - end -->

                        <!-- mainmenu-wrapper - start -->
                        <div class="col-lg-9">
                            <div class="mainmenu-wrapper">
                                <div class="row">                                   
                                    
                                <!-- menu-item-list - start admin -->
                                    <div class="col-lg-12" style="padding-right: 0px; float: right;">
                                        <div class="menu-item-list ul-li clearfix">
                                            <ul>
                                                <li >
                                                    <a href="/buat-acara">Home</a>
                                                </li>
                                                <li class="active">
                                                    <a href="{{route('admin.event')}}">Kelola Event</a>
                                                </li>
                                                                                         
                                                    <li class="menu-item-has-children">
                                                    <?php 
                                                            $name = Auth::user()->name;
                                                            $splitName = explode(' ', $name, 2);

                                                            $first_name = $splitName[0];
                                                            
                                                        ?>
                                                    <a href="" style="color: orange;"><i style="color: orange;" class="fas fa-user"></i> {{$first_name}} </a>
                                                    <ul class="sub-menu">
                                                        
                                                        <li>
                                                            <a  style="" href="{{    route('logout') }}"
                                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">{{('Logout')}}
                                                            
                                                        </a>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>

                                                    </li>
                                                        
                                                    </ul>                                               
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- menu-item-list - end -->
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <!-- mainmenu-wrapper - end -->

                    </div>
                </div>
            </div>
            <!-- header-bottom - end -->
        </header>

        
        <!-- header-section - end
        ================================================== -->
        <!-- altranative-header - start
        ================================================== -->
                @include('layouts.altranative-header')
        <!-- altranative-header - end
        ================================================== -->

        

        <!-- cari-event-section - start
        ================================================== -->
        <!-- booking-section - start
        ================================================== -->
         
     
        <style>
            .dtr-data{
                display: inline-block;
                float: right;
            }
        </style>
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <div align="center"><strong>{{ $message }}!</strong></div>
        </div>
        @endif


  
  

 



<section id="main" class="wrapper " style="padding-top: 4em;">
			
				<div class="container">
					<header class="major special" >
						<h2 align="center" style="font-family: comic sans; text-transform: uppercase;" >Kelola Event</h2>												
					</header>

					
						<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#page1" >Pengajuan Event</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#page2">Status Event</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#page3">Event Selesai</a>
    </li>
    
  </ul>

<style >
	.active a::before{
		color:red;
	}
	.nav-link, .nav-tabs .nav-link.active{
		color: red;
	}
	a.nav-link.active.show{
		color:red;
	}
	a.nav-link{
		color:#000000;
	}
	.imageContainer > img:hover {
	  width: 1000px;
	  height: 250px;
	}
</style>

<div class="tab-content">
					<div class="container tab-pane active" id="page1" class="table-responsive" >
						<table id="primary_table" class="table table-striped table-bordered" style="width:100%">
							<thead>
							<tr>
								<th>No</th>
								<th>Nama Event</th>
								<th>Pembuat</th>								
								<th>Jadwal</th>
								<th>Jenis Event</th>
                                <th>Jenis Pendaftaran</th>
                                <th>Detail Event</th>
                                <th>Action</th>
							</tr>
							</thead>
							
						</table>
					</div>


					<div class="container tab-pane fade" id="page2" class="table-responsive">
						<table id="secondary_table" class="table table-striped table-bordered" style="width:100%">
							<thead>
                                
							<tr>
								<th>No</th>
								<th>Nama Event</th>
								<th>Pembuat</th>
								{{-- <th>Alamat</th> --}}
								<th>Detail</th>
                                <th>Status</th>
                                
								{{-- @if($acara->jenis_daftar == 'bayar')
								<th>Bukti_Pembayaran</th>
								@endif --}}
								
							</tr>
							</thead>
							
						</table>
					</div>

                    <div class="container tab-pane fade" id="page3" class="table-responsive">
                        <table id="tertiary_table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                
                                <tr>
                                    <th>No</th>
                                    <th>Nama Event</th>
                                    <th>Pembuat</th>
                                    {{-- <th>Alamat</th> --}}
                                    <th>Jadwal Mulai</th>
                                    <th>Jadwal Selesai</th>
                                    
                                {{-- @if($acara->jenis_daftar == 'bayar')
                                <th>Bukti_Pembayaran</th>
                                @endif --}}
                                
                            </tr>
                        </thead>

                    </table>
                </div>
</div>



				</div>
			</section>
        <!-- booking-section - end
        ================================================== -->

        <style>
            td{
                text-align: center;
                color: black;
            }
        </style>
        <script>
            var primary_table = $('#primary_table').DataTable({
        responsive: true,                
        processing: true,
        serverSide: true,
        ajax: {
            url:  'admin-list-event',
            type: 'get'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'nama', name: 'nama' },
            { data: 'name', name: 'name' },
            { data: 'jadwal_mulai', name: 'jadwal_mulai' },            
            { data: 'tipe', name: 'tipe' },
            { data: 'jenis_daftar', name: 'jenis_daftar' },
            { data: 'id', name: 'id',  searchable: false, orderable: false,

                render: function(data){
                    return '<a href="/acara/'+data.id+'" >'+
                    '    <button type="button" class="btn btn-light btn-sm"><i class="fas fa-eye"></i></button>'+
                    '</a>';  
                    
                    
                }
             },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){                    
                    return '<div><a  onclick="konfirmasi(this)" data-id="'+data.id+'">'+
                    '    <button type="button" class="btn btn-success btn-sm">Konfirmasi</button>'+
                    '</a></div>'+
                    '<div><a  onclick="tolak(this)" data-id="'+data.id+'">'+
                    '    <button type="button" class="btn btn-danger btn-sm">Tolak</button>'+
                    '</a></div>';
                
                    
                    
                    
                }
            }
            
        ]
    });

    primary_table.on( 'draw', function () {
        
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();


    var secondary_table = $('#secondary_table').DataTable({
        responsive: true,                
        processing: true,
        serverSide: true,
        ajax: {
            url:  'admin-status-event',
            type: 'get'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'nama', name: 'nama' },
            { data: 'name', name: 'name', searchable: false, orderable: false, },
            { data: 'id', name: 'id',  searchable: false, orderable: false,

                render: function(data){
                    return '<a href="/acara/'+data.id+'" >'+
                    '    <button type="button" class="btn btn-light btn-sm"><i class="fas fa-eye"></i></button>'+
                    '</a>';  
                    
                    
                }
             },                    
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){                    
                      if(data.status == 'Belum Dikonfirmasi'){                    
                    return '<p style="text-transform: uppercase; color:red; font-size:14px;" >Belum Dikonfirmasi</p>';
                }     if(data.status == 'Sudah Dikonfirmasi'){                    
                    return '<p style="text-transform: uppercase; color:green; font-size:14px;" >Sudah Dikonfirmasi</p>';
                }else{
                    return '<p style=" text-transform: uppercase; color:red; font-size:14px;" >Ditolak</p>';
                }
                
                    
                    
                    
                }
            }
     
        ]
    });
       secondary_table.on( 'draw', function () {
        secondary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    var tertiary_table = $('#tertiary_table').DataTable({
        responsive: true,       
        processing: true,
        serverSide: true,
        ajax: {
            url:  'admin-selesai-event',
            type: 'get'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'nama', name: 'nama' },
            { data: 'name', name: 'name', searchable: false, orderable: false, },
            { data: 'jadwal_mulai', name: 'jadwal_mulai',  searchable: false, orderable: false,},                    
            { data: 'jadwal_selesai', name: 'jadwal_selesai', searchable: false, orderable: false, },
        ]
    });
       tertiary_table.on( 'draw', function () {
        tertiary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * this.page.info().length;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    function konfirmasi(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "get",
                    url: 'admin-konfirmasi',
                    data: {
                       id: item.attr('data-id')
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                           
                            swal('Berhasil!', result.message, 'success')
                            primary_table.ajax.reload(null, false);
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }

        function tolak(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "get",
                    url: 'admin-tolak',
                    data: {
                       id: item.attr('data-id')
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                           
                            swal('Ditolak!', result.message, 'success')
                            primary_table.ajax.reload(null, false);
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }

        function editActionPopUp(element){


        var item = $(element);
        var id = item.attr('data-id');
        var data = JSON.parse(item.attr('data-content'));
        
        $('input[name=nama_tamu]').val(data.nama_tamu);
        $('input[name=email_tamu]').val(data.email_tamu);
        $('input[name=alamat_tamu]').val(data.alamat_tamu);
        $('select[name=kategori').val(data.id_kategori);
        $('input[name=data_id]').val(id);
        
        
    
        
        
    }
        function deleteAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Delete Data',
            text: "Apakah Anda yakin menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "get",
                        url: 'datatamu-delete',
                        data: {
                            id: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Sukses!', result.message, 'success')
                                secondary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }


    function saveAction(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "get",
                    url: 'datatamu-update',
                    data: {
                        nama_tamu: $('input[name=nama_tamu]').val(),
                        email_tamu: $('input[name=email_tamu]').val(),
                        alamat_tamu: $('input[name=alamat_tamu]').val(),
                        data_id: $('input[name=data_id]').val(),
                        kategori: $('select[name=kategori]').val(),
                        
                    },
                    success: function (result) {
                        if(result.status == 200){
                            $('input[name=nama_tamu]').val();
                            $('input[name=email_tamu]').val();
                            $('input[name=alamat_tamu]').val();
                            swal('Sukses!', result.message, 'success')
                            secondary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }


    function reset(element){

        $(':input').val('');

    }



     $(document).on('click', '.tabs li', function(){
        var item = $(this);
        var target = item.attr('data-target');
        $('.tabs li').each(function() {
            $(this).removeClass('is-active');
            $($(this).attr('data-target')).hide();
        });

        item.addClass('is-active');
        $(target).show();
    });
   

        </script>   
      




        <!-- cari-event-section - end
        ================================================== -->



        <!-- fraimwork - jquery include -->
<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>

       
        




            <script src="{{asset('assets/assets/js/atc.min.js')}}"></script>

        <!-- others jquery include -->
        <script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
        
        
        <script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    

        <!-- custom jquery include -->
        <script src="{{asset('assets/assets/js/custom.js')}}"></script>


    </body>
</html>