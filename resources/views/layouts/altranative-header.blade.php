		<div class="header-altranative">
			<div class="container">
				<div class="logo-area float-left">
					<a href="index-1.html">
						<h5  class="title" style="color: white; margin-top: 8px;">
										KOTAK EVENTS
						</h5>
						{{-- <img src="{{asset('assets/assets/images/1.site-logo.png')}}" alt="logo_not_fund"> --}}
					</a>
				</div>

				<button type="button" id="sidebarCollapse" class="alt-menu-btn float-right">
					<i class="fas fa-bars"></i>
				</button>
			</div>

			<!-- sidebar menu - start -->
			<div class="sidebar-menu-wrapper">
				<div id="sidebar" class="sidebar">
					<span id="sidebar-dismiss" class="sidebar-dismiss">
						<i class="fas fa-arrow-left"></i>
					</span>

					<div class="sidebar-header">
						<a href="#!">
							<h3  class="title" style="color: white;">
										KOTAK EVENTS
							</h3>
							{{-- <img src="" alt="logo_not_found"> --}}
						</a>
					</div>


					<!-- main-pages-links - start -->
					<div class="menu-link-list main-pages-links">
						@if(!Auth::user())
						<ul>
							<li>
								<a href="/">
									<span class="icon"><i class="fas fa-home"></i></span>
									 Home
								</a>
							</li>
							<li>
								<a href="/cari">
									<span class="icon"><i class="fas fa-search"></i></span>
									Cari Event
								</a>
							</li>
							<li>
								<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">
									<span class="icon"><i class="fas fa-file"></i></span>
									Buat Event
								</a>
							</li>
							
						</ul>
						
						@else
						<ul>
							<li>
								<a href="/">
									<span class="icon"><i class="fas fa-home"></i></span>
									 Home
								</a>
							</li>
							<li>
								<a href="/cari">
									<span class="icon"><i class="fas fa-search"></i></span>
									Cari Event
								</a>
							</li>
							<li>
								<a href="/buat-acara">
									<span class="icon"><i class="fas fa-file"></i></span>
									Buat Event
								</a>
							</li>
							<li>
								<a href="/acara-saya">
									<span class="icon"><i class="fas fa-boxes"></i></span>
									Event
								</a>
							</li>
							<li>
								<a href="/undangan-saya">
									<span class="icon"><i class="fas fa-boxes"></i></span>
									Undangan
								</a>
							</li>
						</ul>
						@endif


					</div>
										
					@if(!Auth::user())
					<div class="login-btn-group">
						<ul>

							<li>
								<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openRegisterModal();">Register</a>
							</li>
							<li>
								<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">Login</a>				
							</li>
							
						</ul>
					</div>
					@endif
					<!-- login-btn-group - end -->
					@if(Auth::user())
					<div class="login-btn-group" >
						<ul>
							<li>
								<a>{{Auth::user()->name}}</a>				
							</li>
							<li><a  href="{{    route('logout') }}" class="login-modal-btn" 
													onclick="event.preventDefault();
													document.getElementById('logout-form').submit();">
													<i class="fas fa-user"></i> {{ __('Logout') }}
												</a></li>
												<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
													@csrf
												</form>
						</ul>
					</div>
					@endif

					<!-- social-links - start -->
					<div class="social-links">
						{{-- <h2 class="menu-title">get in touch</h2> --}}
						<div class="mb-15">
							<h3 class="contact-info">
								<i class="fas fa-phone"></i>
								111-2222-9999
							</h3>
							<h3 class="contact-info" style="text-transform: lowercase;">
								<i class="fas fa-envelope"></i>
								kotakevents@gmail.com
							</h3>
						</div>
						<ul>
							<li>
								<a href="#!"><i class="fab fa-facebook-f"></i></a>
							</li>
							<li>
								<a href="#!"><i class="fab fa-twitter"></i></a>
							</li>
							<li>
								<a href="#!"><i class="fab fa-twitch"></i></a>
							</li>
							<li>
								<a href="#!"><i class="fab fa-google-plus-g"></i></a>
							</li>
							<li>
								<a href="#!"><i class="fab fa-instagram"></i></a>
							</li>
						</ul>
						<a href="" class="contact-btn">Hubungi Kami</a>
					</div>
					<!-- social-links - end -->

					<div class="overlay"></div>
				</div>
			</div>
			<!-- sidebar menu - end -->
		</div>