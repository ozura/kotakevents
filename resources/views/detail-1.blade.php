<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		{{-- <link rel="stylesheet" href="{{asset('assets/css/box.css')}}" /> --}}
		<link rel="stylesheet" href="{{asset('assets/css/login-register.css')}}">		

		<!-- fraimwork - css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/flaticon.css')}}">

		<!-- carousel css include -->
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/slick.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/slick-theme.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/animate.css')}}"> --}}
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/owl.carousel.min.css')}}">
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/owl.theme.default.min.css')}}"> --}}

		<!-- others css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/magnific-popup.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/calendar.css')}}"> --}}

		

		<!-- custom css include -->
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}"> --}}
		<link rel="stylesheet" type="text/css" href="https://kotakevents.com/assets/assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="{{asset('css/lightbox.min.css')}}">
  		<script type="text/javascript" src="{{asset('js/lightbox-plus-jquery.min.js')}}"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

	</head>


	<body class="default-header-p">




		
		<!-- backtotop - start -->
		<div id="thetop" class="thetop"></div>
		<div class='backtotop'>
			<a href="#thetop" class='scroll'>
				<i class="fas fa-angle-double-up"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		<div id="preloader"></div>
		<!-- preloader - end -->
		<style>
			.modal-open {
				
				overflow: scroll;
				width: 100%;
				padding-right: 0!important;
			}
		</style>

		<style scoped>
		@media (max-width:400px){
			.login .modal-dialog{
				width: 95%;

			}
		}
		</style>


		<!-- header-section - start
		================================================== -->
		<header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
			<!-- header-top - start -->
			{{-- <div class="header-top">
				<div class="container">
					<div class="row">

						
						
						
					</div>
				</div>
			</div> --}}
			<!-- header-top - end -->
			<!-- header-bottom - start -->
<div class="header-bottom" >
				<div class="container" >
					<div class="row" style="margin-right: : -1px; margin-left: -1px;">

						<!-- site-logo-wrapper - start -->
						<div class="col-lg-3" style="padding-left: 0px;">
							<div class="site-logo-wrapper">
								<a href="index-1.html" class="logo">
									<h3  class="title" style="color: white; margin-top: 8px;">
										KOTAK EVENT

									</h3>
									{{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
								</a>
							</div>
						</div>
						<!-- site-logo-wrapper - end -->

						<!-- mainmenu-wrapper - start -->
						<div class="col-lg-9">
							<div class="mainmenu-wrapper">
								<div class="row">
									@if(!Auth::user())
																	<!-- menu-item-list - start -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li >
													<a href="/">Home</a>
												</li>
												<li >
													<a href="/cari">Cari Event</a>
												</li>
												<li>
													<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">
														Buat Event
													</a>
												</li>

												<li>
													<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">
													<i class="fas fa-user"></i>	Login
													</a>
												</li>																																					
												
											</ul>
										</div>
									</div>
									
									@else
									<!-- menu-item-list - start -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li >
													<a href="/">Home</a>
												</li>
												<li >
													<a href="/cari">Cari Event</a>
												</li>
												<li>
													<a href="/buat-acara">Buat Event</a>
												</li>
												<li class="active"><a href="/acara-saya">Event Saya</a></li>
												<li><a <a href="/undangan-saya">Tiket</a></li>
												
												<li class="menu-item-has-children">
													
													<a href="" style="color: white;"><i style="color: white;" class="fas fa-user"></i> {{Auth::user()->name}}</a>
													<ul class="sub-menu">
														
														<li>
															<a  style="" href="{{    route('logout') }}"
															onclick="event.preventDefault();
															document.getElementById('logout-form').submit();">{{('Logout')}}
															
														</a>
														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>

													</li>
														
													</ul>												
												</li>

											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->									
									@endif
									
								</div>
							</div>
						</div>
						<!-- mainmenu-wrapper - end -->

					</div>
				</div>
			</div>
			<!-- header-bottom - end -->
		</header>
		<!-- header-section - end
		================================================== -->



		<!-- altranative-header - start
		================================================== -->
		@include('layouts.altranative-header')
		<!-- altranative-header - end
		================================================== -->





		<!-- breadcrumb-section - start
		================================================== -->
		
		<!-- breadcrumb-section - end
		================================================== -->
		<style>
			#form-messages {
				background-color: rgb(255, 232, 232);
				border: 1px solid red;
				color: red;
				display: none;
				font-size: 12px;
				font-weight: bold;
				margin-bottom: 10px;
				padding: 10px 25px;	
				max-width: 250px;
			}
		</style>
		<!-- Modal awal -->
		<div class="modal fade login" id="loginModal">
			<div class="modal-dialog login animated">
				<div class="modal-content text-center"><br>
					<div class="container text-center">
						<div class="modal-header text-center">
							<h4 align="center" class="modal-title text-center" style="color: orange; text-transform: uppercase; font-weight: 600;" >LOGIN</h4>
						</div>
					</div>
					<div class="modal-body">
						<div id="box1" class="box">
							<div class="content">
								@if(session('id')==10)
								<h3  align="center" style="color: red; text-transform: capitalize;">Password Salah</h3>
								@endif
								@if(session('id')==11)
								<h3 align="center" style=" text-transform: capitalize; color: red;">Email Tidak Terdaftar</h3>
								@endif
								<div class="form loginBox">
									<div align="center">
									<ul id="form-messages">
										<li>Generic Error #1</li>
									</ul>
									</div>
									<form id="masuk" name="masuk" method="POST" action="{{route('login')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input id="email" class="form-control" type="text" placeholder="Email" name="email" required=""><br>
										<input id="password" class="form-control" type="password" placeholder="Password" name="password" required=""><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Login</button>
										</div>
									</form>									
								</div>
								
							</div>
						</div>
						<div id="box2" class="box">
							<div class="content registerBox" style="display:none;">
								@if(session('id')==1)
								<h3 align="center" style="color: red;">Email Sudah Terdaftar</h3>
								@endif
								@if(session('id')==2)
								<h3 align="center" style="color: red;">Password Tidak Cocok</h3>
								@endif
								<div class="form">
									<form method="POST" html="{:multipart=>true}" data-remote="true" action="{{route('register')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input id="name" class="form-control" type="text" minlength="4" maxlength="100" placeholder="Nama" name="name" required><br>
										<input id="alamat" class="form-control" type="text" minlength="4" maxlength="255" placeholder="Alamat" name="alamat" required><br>
										<input id="email" class="form-control" type="text" placeholder="Email" name="email" required><br>
										<input id="password" class="form-control" type="password" minlength="6" maxlength="15" placeholder="Password" name="password" required><br>
										<input id="password_confirmation" class="form-control" type="password" minlength="6" maxlength="15" placeholder="Repeat Password" name="password_confirmation" required><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Buat Akun</button>
										</div>
									</form>
								
								</div>
							</div>
						</div>
					</div>
					<div id="box3" class="box">
							<div class="content">
								@if(session('id')==10)
								<h3  align="center" style="color: red; text-transform: capitalize;">Password Salah</h3>
								@endif
								@if(session('id')==11)
								<h3 align="center" style=" text-transform: capitalize; color: red;">Email Tidak Terdaftar</h3>
								@endif
								<div class="form loginBox text-center">
									<form  id="reset" name="masuk" method="POST" action="{{route('login')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input  id="email" class="form-control" type="text" placeholder="Email" name="email" required=""><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Reset Password</button>
										</div>
									</form>									
								</div>
								
							</div>
						</div>
					<div class="modal-footer">
						<div class="forgot login-footer">
                            <span>
                            	Lupa Password?
                                 <a id="regis" href="javascript: showLupaForm();">Reset</a>
                            </span>
                            <span>
                            	Belum punya akun?
                                 <a id="regis" href="javascript: showRegisterForm();"><strong>Daftar Disini!</strong></a>
                            </span>
							
						</div>
						<div class="forgot register-footer" style="display:none">
							<span>Sudah punya akun?</span>
							<a href="javascript: showLoginForm();"><strong>Login</strong></a>
						</div>
					</div>
				</div>
			</div>
		</div>

	<div class="modal" id="modaldata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header border-bottom-0">
												<h4 class="modal-title" id="exampleModalLabel">Pilih Kategori</h4>

											</div>
											<input type="hidden" name="data_id" value="">

											<div class="modal-body">
												<form id="bayar-daftar-form" action="{{ route('daftar.acara') }}" method="POST" >
												<input type="hidden" name="id" value="{{$detail['id']}}">												

												<div class="form-group">
													<label for="kategori">Kategori</label>
													<select name="kategori" required="">

														@foreach(\App\Category::select('id','id_event', 'nama_kategori')->where('id_event',$detail->id)->get() as $kat) 
														<option value="{{$kat->id}}">{{$kat->nama_kategori}}</option>
														@endforeach

													</select>
												</div>

													@csrf
												</form>

											</div>
											<div class="modal-footer border-top-0 d-flex justify-content-right">

												<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
												<button type="submit" id="button" onclick="document.getElementById('bayar-daftar-form').submit();" class="btn btn-info" data-dismiss="modal">Daftar</button>

											</div>

										</div>
									</div>
								</div>



		<!-- event-details-section - start
		================================================== -->
		@if($pemilik=='pemilik')				
		<div class="container" style="padding-top: 1.5em;">							
			<a  href="{{ url('/')}}"><p>Home ></p></a>
			<a href="/acara-saya"><p>Event ></p></a>
			<a href="/acara/{{$detail->id}}"><p>{{$detail->nama}}</p></a>
		
		</div> 
		@endif
		<section id="event-details-section" class="event-details-section sec-ptb-100 clearfix">
			<div class="container">

				<div class="row">

					<!-- col - event-details - start -->
					<div class="col-lg-8 col-md-12 col-sm-12">

						<!-- event-details - start -->
						<div class="event-details mb-80">


							<img style="height: 360px; width: 100%;" src="{{asset('fotoupload/'.$detail->foto)}}">
							<div class="event-title mb-30">								
								
								<h2 class="event-title"><strong>{{$detail['nama']}}</strong>
									@if($pemilik=='pemilik')
									<a href="/edit-acara/{{$detail->id}}">									   
										{{-- <button type="button" class="btn btn-info btn-sm">Edit</button>	 --}}
									</a>

									
									@endif
								</h2>	
								@if(!empty($detail->kode_unik))
								<p style="font-size: 16px;">Kode Event : <span style="color: red;">{{$detail->kode_unik}}</span> </p>							
								@endif
							</div>
							@if($pemilik!='pemilik')
							@if($pemilik == 1)
							<div class="button" align="right" style="margin-bottom: 1em;" >
								@if(Auth::user()->email=='adebwahyu@gmail.com')

								@elseif($detail->pendaftaran == 'Non Aktif')
								
								@elseif($detail->pesan == 'selesai')

								@elseif($detail->status == 'Sudah Dikonfirmasi' && $pesan == 'ada' && $detail->jenis_daftar=='bayar')
								<a class="custom-btn" data-target="#modaldata" data-toggle="modal">Daftar Event</a>
								@elseif($detail->status == 'Sudah Dikonfirmasi' && $pesan == 'ada')
								<a class="custom-btn" onclick="document.getElementById('daftar-form').submit();">Daftar Event</a>
								{{-- <input type="button" class="custom-btn" onclick="document.getElementById('daftar-form').submit();" value="Daftar"> --}}
								@endif
								{{-- <a href="" class="custom-btn" onclick="document.getElementById('daftar-form').submit();">Daftar Event</a> --}}

									<form id="daftar-form" action="{{ route('daftar.acara') }}" method="POST" style="display: none;">
									<input type="hidden" name="id" value="{{$detail['id']}}">
									@csrf
								</form>
							</div>
							@endif
							@endif
							
							

							<p style="text-align: justify;" class="black-color mb-30">
								{{$detail['deskripsi']}} 
							</p>

							<div class="event-info-list ul-li clearfix mb-50">
								<ul>
									<li>
										<span class="icon">
											<i class="far fa-calendar"></i>
										</span>
										<div class="event-content">
											<small class="event-title">Tanggal Event</small>
											<h3 class="event-date">
												@if($detail['jadwal_mulai']->format(' d m Y ') ==$detail['jadwal_selesai']->format(' d m Y '))
													{{$detail['jadwal_mulai']->format(' d M Y ')}}
												@else
												{{$detail['jadwal_mulai']->format(' d m Y ')}} - {{$detail['jadwal_selesai']->format(' d m Y ')}}
												@endif
											</h3>
										</div>
									</li>
									<li>
										<span class="icon">
											<i class="far fa-clock"></i>
										</span>
										<div class="event-content">
											<small class="event-title">Waktu Event</small>
											<h3 class="event-date">{{$detail['jadwal_mulai']->format('h:i')}} WIB - {{$detail['jadwal_selesai']->format('h:i')}} WIB</h3>
										</div>
									</li>
									<li>
										<span class="icon">
											<i class="fas fa-map-marker-alt"></i>
										</span>
										<div class="event-content">
											<small class="event-title">Lokasi Event</small>
											<h3 class="event-date">{{$detail['lokasi']}}</h3>
										</div>
									</li>

									
								</ul>
								<div>
								{{-- <ul>
									<li>
										<span class="icon">
											<i class="fas fa-times"></i>
										</span>
										<div class="event-content">
											<small class="event-title">Max Seats</small>
											<h3 class="event-date">2,250 seats</h3>
										</div>
									</li>
								</ul> --}}
								</div>

							</div>
<style scoped>
    .barisp {margin: 0 -5px;}
    .kolomp {
              float: left;
              width: 20%;
              /*padding: 0 10px;*/
              color: white;
            }
          .hehe{
                height: 100px;
            }

            @media (max-width: 500px) { /* or 301 if you want really the same as previously.  */
                .hehe{   
                    height: 70px;
                }
                .kolomp{
                    width: 33.3%;
                }
                
            }

</style>
							<section >
																			
										
										{{-- @if($galery!=null)
										<div class="barisp" style="background-color: white;  ">
											@foreach($galery as $galeri)

											<div class="kolomp" style="background-color: white;" align="center">
												<br>
												<a href="{{asset('fotoupload/'.$galeri->file)}}" data-lightbox="mygallery"> 
													<div class="hehe" style="overflow: hidden;  width: 95%; display: flex;
													flex-direction: column;
													justify-content: center;
													align-items: center;">

													<img width="100%"  src="{{asset('fotoupload/'.$galeri->file)}}">
												</div>
											</a>       

										</div>
										@endforeach
									</div>	
									@endif --}}							
						</section>
{{-- 							<div id="event-details-carousel" class="event-details-carousel owl-carousel owl-theme">
								<div class="item">
									<img src="{{asset('fotoupload/'.$detail->foto)}}">
								</div>
								@if(!empty($galery))
								
								@foreach($galery as $galeri)
								<div class="item">
									<img src="{{asset('fotoupload/'.$galeri->file)}}" alt="Image_not_found">
								</div>
								@endforeach
								@endif
							</div> --}}

							{{-- <p class="black-color m-0">
								Lorem ipsum dollor site amet the best  consectuer diam adipiscing elites sed diam nonummy nibh the euismod tincidunt ut laoreet dolore magna aliquam erat volutpat insignia the consectuer adipiscing elit. 
							</p> --}}

						</div>
						
						<!-- event-details - end -->



					



					</div>
					<!-- col - event-details - end -->

					<!-- sidebar-section - start -->
					<div class="col-lg-4 col-md-12 col-sm-12">
						<div class="sidebar-section">

							

							<!-- map-wrapper - start -->
							<div class="map-wrapper mb-30">
								@if($pemilik==1)
								@if($detail->pendaftaran == 'Non Aktif')
									<div style="background-color: red;" align="center">
									<h4 class="event-title" style="color: white; text-transform: uppercase;">Pendaftaran Ditutup</h4>
								</div>
								@elseif($pesan == 'selesai')
									<div style="background-color: red;" align="center">
									<h4 class="event-title" style="color: white; text-transform: uppercase;">Event Telah Selesai</h4>
								</div>
								@endif
								@endif
								@if($pemilik==false)
								@if($detail->pendaftaran == 'Non Aktif')
								<div style="background-color: red;" align="center">
									<h4 class="event-title" style="color: white; text-transform: uppercase;">Pendaftaran Ditutup</h4>
								</div>
								@elseif($pesan == 'ada')
								<div class="button" align="left" style="margin-bottom: 1em;" >
									<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();" class="custom-btn">Daftar Event</a>		
								</div>
								@elseif($pesan == 'selesai')
								<div style="background-color: red;" align="center">
									<h4 class="event-title" style="color: white; text-transform: uppercase;">Event Telah Selesai</h4>
								</div>
								@endif
								@endif	

								@if($pemilik=='pemilik')									
								<div class="sec-ptb-80">
									<div align="center" >

									{{-- 	<p style="font-size: 16px;">Jumlah Pendaftar : <span style="color: red;">{{$jumlah}}</span> </p> --}}
									</div>
									<div align="center" >
										{{-- <p style="font-size: 16px;">Jumlah Terdaftar : <span style="color: red;">{{$jumlah1}}</span></p>								 --}}		
									</div>
								</div>			
																
								@endif

								@if($pemilik=='Menunggu Pembayaran')
								<div class="sec-ptb-80">
								<p align="center">Status :</p>
								<div align="center" style="background-color: red; border-radius: 5px;" >
									<p style="font-size: 22px; color: white;">Menunggu Pembayaran</p>
								</div>
								</div>
								{{-- <a href="/undangan-saya"><h6 style="color: orange;" >Upload Bukti Pembayaran</h6></a> --}}
								<div align="center">
									<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Upload Bukti Bayar</button>
								</div>
								<div class="modal fade" id="myModal" role="dialog">
									<div class="modal-dialog modal-lg">
										<br><br><br><br>
										<div class="modal-content">
											<div class="modal-header">
												{{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
												<h4 class="modal-title">UPLOAD BUKTI PEMBAYARAN</h4>
											</div>
											<div class="modal-body">
												<form action="{{route('upload.bukti')}}" method="post"  enctype="multipart/form-data">
													{{csrf_field()}}
													<input type="hidden" name="idA" value="{{$attendance->id}}">
													<input name="bukti" id="bukti" type="file">

													<div align="right">
														<input type="submit" class="btn btn-primary" value="Simpan">
													</div>		
												</form>

											</div>

										</div>
											{{-- <div class="modal-footer">
												<button type="button" class="btn btn-default-sm" data-dismiss="modal">Close</button>
											</div> --}}
									</div>
								</div>

								@endif
								@if($pemilik=='Belum Dikonfirmasi')
								<div class="sec-ptb-80">
								<p align="center">Status :</p>									
								<div align="center" style="background-color: red; border-radius: 5px;" >

									<p style="font-size: 22px; color: white; margin-bottom: 0;">Belum Dikonfirmasi</p>
									
								</div>
								</div>
								@endif
								<!-- section-title - start -->
								@if($pemilik=='Sudah Dikonfirmasi')
								<div class="sec-ptb-80">
								<div align="center" style="background-color: green; border-radius: 5px;" >
									<p style="font-size: 22px; color: white;">Anda Sudah Terdaftar</p>
								</div>
								</div>
								{{-- <div class="section-title mb-30" align="center">
									<h2 class="big-title">event <strong>QR Code</strong></h2>
								</div> --}}
								<!-- section-title - end -->
								<div align="center">
									{!! QrCode::size(250)->generate($attendance['id_scan']); !!}
								</div>								
								<div align="center">
									{{-- <a href="" type="button" class="kastem2-btn" style="background-color: #FF7E47;">Unduh Undangan</a> --}}
					
                        <form method="POST" action="{{URL::to('viewpdf')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="ev" value="{{$detail->id}}">
                            <input type="hidden" name="us" value="{{Auth::user()->id}}">
                            <input type="hidden" name="sc" value="{{$attendance->id_scan}}">
                            <button  type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Unduh Undangan</button>
                        </form>
                    

						{{-- 			<a href="{{route('download',['ev'=>$detail->id,'us'=>Auth::user()->id,'sc'=>$attendance->id_scan])}}">
								<button  type="button" style="background-color: #FF7E47;" class="kastem2-btn">Unduh Undangan</button>
								</a> --}}
								</div>
								@endif
								<br><br><br>
								@if($detail->jenis_daftar=='bayar' && $pemilik!='Sudah Dikonfirmasi' )
								<div align="center" style="background-color: white; ">
									<hr>
								<h6 style="color: black;" ><strong>{{$detail->bank}}</strong>  </h6>
								<h6 style="color: black;" >{{$detail->norek}}</h6>						
								<h6 style="color: black;" >{{$detail->atasnama}}</h6>				
								<hr>
								<h6 style="color: red;" > {{$detail->jumlah_bayar}}</h6>
								<hr>
								</div>
								@endif

								{{-- <div id="google-map">
									<div id="googleMaps" class="google-map-container"></div>
								</div> --}}

							</div>
							<!-- map-wrapper - end -->

							

							
							
						</div>
					</div>
					<!-- sidebar-section - end -->
					
				</div>
			</div>
		</section>
		<!-- event-details-section - end
		================================================== -->
		@if($pemilik=='pemilik')
										<div align="center">
											<a href="{{route('galery',['id'=>$detail->id])}}">
												{{-- <button type="button" class="btn btn-info btn-sm">Upload Gambar</button>						 --}}
											</a>
										</div>
										
										@endif
		@if($galery!=null)
		<section id="event-gallery-section" class="event-gallery-section sec-ptb-100 clearfix">
			<div class="container">				

				<div class="grid zoom-gallery clearfix mb-80" data-isotope="{ &quot;masonry&quot;: { &quot;columnWidth&quot;: 0 } }">
					@foreach($galery as $galeri)
					<div class="grid-item photo-gallery " data-category="photo-gallery" >
						<a class="popup-link" href="{{asset('fotoupload/'.$galeri->file)}}">
							<img style="width: 292.5px; height: 243.75px;"  src="{{asset('fotoupload/'.$galeri->file)}}" alt="Image_not_found">
						</a>						
					</div>
					@endforeach

				</div>

			</div>
		</section>
		@endif





		<!-- default-footer-section - start
		================================================== -->
		<br><br>
		@include('layouts.footer')
		<!-- default-footer-section - end
		================================================== -->










		<!-- fraimwork - jquery include -->
		<script src="{{asset('assets/assets/js/jquery-3.3.1.min.js')}}"></script>
		{{-- <script src="{{asset('assets/assets/js/popper.min.js')}}"></script> --}}
		<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>

		<!-- carousel jquery include -->
		<script src="{{asset('assets/assets/js/slick.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/owl.carousel.min.js')}}"></script>

		

		<!-- calendar jquery include -->
		<script src="{{asset('assets/assets/js/atc.min.js')}}"></script>

		<!-- others jquery include -->
		<script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/isotope.pkgd.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/jarallax.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>

		<!-- gallery img loaded - jqury include -->
		<script src="{{asset('assets/assets/js/imagesloaded.pkgd.min.js')}}"></script>

		<!-- multy count down - jqury include -->
		{{-- <script src="{{asset('assets/assets/js/jquery.countdown.js')}}"></script> --}}

		

		<!-- custom jquery include -->
		<script src="{{asset('assets/assets/js/custom.js')}}"></script>

		<script src="{{asset('assets/js/login-register.js')}}"></script>





	</body>
</html>