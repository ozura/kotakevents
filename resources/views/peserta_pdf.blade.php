<html>
<head>
	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Daftar Tamu {{$event->nama}}</h4>	
	</center>
 	<br>
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Nama</th>
				<th>Email</th>
				<th>Alamat</th>
				<th>Kategori</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($data as $p)
			<tr>
				<td>{{ $i++ }}</td>
				@if($p->name == 'Admin')
				<td>{{$p->nama_tamu}}</td>
				@else
				<td>{{$p->name}}</td>
				@endif
				@if($p->name == 'Admin')
				<td>{{$p->email_tamu}}</td>
				@else
				<td>{{$p->email}}</td>
				@endif
				@if($p->name == 'Admin')
				<td>{{$p->alamat_tamu}}</td>
				@else
				<td>{{$p->alamat}}</td>
				@endif	
				@if($p->id_kategori == 1)			
				<td>{{$p->kategori_tamu}}</td>
				@else
				<td>{{$p->nama_kategori}}</td>
				@endif				
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>