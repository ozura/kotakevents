<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		{{-- <link rel="stylesheet" href="{{asset('assets/css/box.css')}}" /> --}}
		<link rel="stylesheet" href="{{asset('assets/css/login-register.css')}}">

				<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
		{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/flaticon.css')}}"> --}}



		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">


		

		<!-- custom css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">
		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

	</head>

	
	<body>

	
		
 
		<!-- backtotop - start -->
		<div id="thetop" class="thetop"></div>
		<div class='backtotop'>
			<a href="#thetop" class='scroll'>
				<i class="fas fa-angle-double-up"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		<div id="preloader"></div>
		<!-- preloader - end -->

		<style>
			.modal-open {
				
				overflow: scroll;
				width: 100%;
				padding-right: 0!important;
			}
		</style>

		<style scoped>
		@media (max-width:400px){
			.login .modal-dialog{
				width: 95%;

			}
		}
		</style>



		<!-- header-section - start
		================================================== -->
		<header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
			<!-- header-top - start -->
			{{-- <div class="header-top">
				<div class="container">
					<div class="row">

						
						
						
					</div>
				</div>
			</div> --}}
			<!-- header-top - end -->
			<!-- header-bottom - start -->
			<div class="header-bottom" >
				<div class="container" >
					<div class="row" style="margin-right: : -1px; margin-left: -1px;">

						<!-- site-logo-wrapper - start -->
						<div class="col-lg-3" style="padding-left: 0px;">
							<div class="site-logo-wrapper">
								<a href="index-1.html" class="logo">
									<h3  class="title" style="color: white; margin-top: 8px;">
										KOTAK EVENT

									</h3>
									{{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
								</a>
							</div>
						</div>
						<!-- site-logo-wrapper - end -->

						<!-- mainmenu-wrapper - start -->
						<div class="col-lg-9">
							<div class="mainmenu-wrapper">
								<div class="row">
									@if(!Auth::user())
																	<!-- menu-item-list - start -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li >
													<a href="/">Home</a>
												</li>
												<li class="active">
													<a href="/cari">Cari Event</a>
												</li>
												<li>
													<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">
														Buat Event
													</a>
												</li>

												<li>
													<a id="loginShow" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">
													<i class="fas fa-user"></i>	Login
													</a>
												</li>																																					
												
											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->

									@else
									<!-- menu-item-list - start -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li >
													<a href="/">Home</a>
												</li>
												<li class="active">
													<a href="/cari">Cari Event</a>
												</li>
												<li>
													<a href="/buat-acara">Buat Event</a>
												</li>
												<li><a href="/acara-saya">Event</a></li>
												<li><a <a href="/undangan-saya">Undangan</a></li>											
												<li class="menu-item-has-children">
													
													<a href="" style="color: white;"><i style="color: white;" class="fas fa-user"></i> {{Auth::user()->name}}</a>
													<ul class="sub-menu">
														
														<li>
															<a  style="" href="{{    route('logout') }}"
															onclick="event.preventDefault();
															document.getElementById('logout-form').submit();">{{('Logout')}}
															
														</a>
														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>

													</li>
														
													</ul>												
												</li>

											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->									
									@endif
									
								</div>
							</div>
						</div>
						<!-- mainmenu-wrapper - end -->

					</div>
				</div>
			</div>
			<!-- header-bottom - end -->
		</header>

		<style>
			#form-messages {
				background-color: rgb(255, 232, 232);
				border: 1px solid red;
				color: red;
				display: none;
				font-size: 12px;
				font-weight: bold;
				margin-bottom: 10px;
				padding: 10px 25px;	
				max-width: 250px;
			}
		</style>
		<!-- Modal awal -->
		<div class="modal fade login" id="loginModal">
			<div class="modal-dialog login animated">
				<div class="modal-content text-center"><br>
					<div class="container text-center">
						<div class="modal-header text-center">
							<h4 align="center" class="modal-title text-center" style="color: orange; text-transform: uppercase; font-weight: 600;" >LOGIN</h4>
						</div>
					</div>
					<div class="modal-body">
						<div id="box1" class="box">
							<div class="content">
								@if(session('id')==10)
								<h3  align="center" style="color: red; text-transform: capitalize;">Password Salah</h3>
								@endif
								@if(session('id')==11)
								<h3 align="center" style=" text-transform: capitalize; color: red;">Email Tidak Terdaftar</h3>
								@endif
								<div class="form loginBox">
									<div align="center">
									<ul id="form-messages">
										<li>Generic Error #1</li>
									</ul>
									</div>
									<form id="masuk" name="masuk" method="POST" action="{{route('login')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input id="email" class="form-control" type="text" placeholder="Email" name="email" required=""><br>
										<input id="password" class="form-control" type="password" placeholder="Password" name="password" required=""><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Login</button>
										</div>
									</form>									
								</div>
								
							</div>
						</div>
						<div id="box2" class="box">
							<div class="content registerBox" style="display:none;">
								@if(session('id')==1)
								<h3 align="center" style="color: red;">Email Sudah Terdaftar</h3>
								@endif
								@if(session('id')==2)
								<h3 align="center" style="color: red;">Password Tidak Cocok</h3>
								@endif
								<div class="form">
									<form method="POST" html="{:multipart=>true}" data-remote="true" action="{{route('register')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input id="name" class="form-control" type="text" minlength="4" maxlength="100" placeholder="Nama" name="name" required><br>
										<input id="alamat" class="form-control" type="text" minlength="4" maxlength="255" placeholder="Alamat" name="alamat" required><br>
										<input id="email" class="form-control" type="text" placeholder="Email" name="email" required><br>
										<input id="password" class="form-control" type="password" minlength="6" maxlength="15" placeholder="Password" name="password" required><br>
										<input id="password_confirmation" class="form-control" type="password" minlength="6" maxlength="15" placeholder="Repeat Password" name="password_confirmation" required><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Buat Akun</button>
										</div>
									</form>
								
								</div>
							</div>
						</div>
					</div>
					<div id="box3" class="box">
							<div class="content">
								@if(session('id')==10)
								<h3  align="center" style="color: red; text-transform: capitalize;">Password Salah</h3>
								@endif
								@if(session('id')==11)
								<h3 align="center" style=" text-transform: capitalize; color: red;">Email Tidak Terdaftar</h3>
								@endif
								<div class="form loginBox">
									<form id="masuk" name="masuk" method="POST" action="{{route('login')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input id="email" class="form-control" type="text" placeholder="Email" name="email" required=""><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Reset Password</button>
										</div>
									</form>									
								</div>
								
							</div>
						</div>
					<div class="modal-footer">
						<div class="forgot login-footer">
                            <span>
                            	Lupa Password?
                                 <a id="regis" href="javascript: showLupaForm();">Reset</a>
                            </span>
                            <span>
                            	Belum punya akun?
                                 <a id="regis" href="javascript: showRegisterForm();"><strong>Daftar Disini!</strong></a>
                            </span>
							
						</div>
						<div class="forgot register-footer" style="display:none">
							<span>Sudah punya akun?</span>
							<a href="javascript: showLoginForm();"><strong>Login</strong></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- header-section - end
		================================================== -->
		<!-- altranative-header - start
		================================================== -->
		@include('layouts.altranative-header')
		<!-- altranative-header - end
		================================================== -->


		

		<!-- absolute-eventmake-section - start
		================================================== -->
		<div id="absolute-eventmake-section" class="absolute-eventmake-section sec-ptb-77  bg-gray-light clearfix"  >
			<div class="eventmaking-wrapper"   >

				

				<div class="tab-content" >
					<div id="conference" class="tab-pane fade in active show" >
						<form method="post" action="/cari" >
							{{csrf_field()}}
							<ul >
								<li>
									<input style="font-size: 14px; text-align: center;" type="text" name="kunci" placeholder="Kode Event" required="">
									<input type="hidden" name="kategori" value="private">
								</li>															
								{{-- <li>
									<select name="kategori">
										
										<option value="public"><p style="font-size: 10px;">Nama Event</p></option>
										<option value="private">Kode Event</option>
										
									</select>
								</li> --}}
								<li>
									<button type="submit" value="cari" style="background: #d9534f;" class="kastem-btn" >Cari</button>
								</li>

							</ul>
						</form>
					</div>
				</div>
				
			</div>
		</div>

		<!-- absolute-eventmake-section - end
		================================================== -->
		

		<!-- cari-event-section - start
		================================================== -->
		<!-- event-section - start
		================================================== -->
		<section id="event-section" class="event-section bg-gray-light sec-ptb-100 clearfix">
			<div class="container">
				<div class="row justify-content-center">

					<!-- - start -->
					<div class="col-lg-9 col-md-12 col-sm-12">

						

						<div class="tab-content">
							<div id="list-style" class="tab-pane fade in active show">						
								@if(isset($cari))
								@foreach($cari as $subcari)
								<!-- event-item - start -->
								<div class="event-list-item clearfix">
									<!-- event-image - start -->
									<div class="event-image">
										<div class="post-date">
											<span class="date">{{$subcari->jadwal_mulai->format('d')}}</span>
											<small class="month">{{$subcari->jadwal_mulai->format('M')}}</small>
										</div>
										<img src="{{asset('fotoupload/'.$subcari->foto)}}" alt="Image_not_found">
									</div>
									<!-- event-image - end -->

									<!-- event-content - start -->
									<div class="event-content">
										<div class="event-title mb-15">
											<h3 class="title">
												 <strong>{{$subcari->nama}}</strong>
											</h3>
											@if($subcari->jenis_daftar == 'bayar')
											<span class="ticket-price yellow-color">Berbayar</span>
											@else
											<span class="ticket-price yellow-color">Free</span>
											@endif
											
										</div>
										
										<p class="discription-text mb-30" style="text-overflow: ellipsis; overflow: hidden; height: 100px; ">
											{{$subcari->deskripsi}}
											<div class="clearfix"></div>
										</p>


										<div class="event-info-list ul-li clearfix">
											<ul>
												<li>
													<span class="icon">
														<i class="fas fa-map-marker-alt"></i>
													</span>
													<div class="info-content">
														<small>Lokasi</small>
														<h6>
															{{$subcari->lokasi}}
														</h6>
													</div>
												</li>
												
												<li>
													<span class="icon">
														<i class="far fa-clock"></i>
													</span>
													<div class="info-content">
														<small>Waktu</small>
														<h6>
															{{$subcari->jadwal_mulai->format('h:i')}} WIB
														</h6>	
													</div>
												</li>
												
												
												<li style="float: right; display: inline-block;">
													<a href="{{route('detail.acara',['id'=>$subcari->id])}}" class="tickets-details-btn">
														Lihat Event
													</a>
												</li>
												
												
												
											</ul>
										</div>
									</div>
									<!-- event-content - end -->

								</div>
								<!-- event-item - end -->
								@endforeach
								{{ $cari->links() }}
								
								@endif
								@if($jumlah == 0)
								<h3 align="center">Event Tidak Ditemukan</h3>
								<div align="center">
								<a href="/cari"><h5 style="color: orange;">cari event lain?</h5></a>
								</div>
								@endif
								

							</div>

							
						</div>

					</div>
					<!-- - end -->
					
				</div>
			</div>

		</section>
		<!-- event-section - end
		================================================== -->



		<!-- cari-event-section - end
		================================================== -->


		<!-- fraimwork - jquery include -->
		
		
		<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>

		

		<!-- calendar jquery include -->
		<script src="{{asset('assets/assets/js/atc.min.js')}}"></script>

		<!-- others jquery include -->
		<script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
		
		<script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
		<!-- custom jquery include -->
		<script src="{{asset('assets/assets/js/custom.js')}}"></script>
		<script src="{{asset('assets/js/login-register.js')}}"></script>





	</body>
</html>