<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		<link rel="stylesheet" href="{{asset('assets/css/box.css')}}" />
		<link rel="stylesheet" href="{{asset('asset/css/login-register.css')}}">

		<!-- fraimwork - css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/flaticon.css')}}">

		<!-- carousel css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/slick.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/slick-theme.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/animate.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/owl.carousel.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/owl.theme.default.min.css')}}">

		<!-- others css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/magnific-popup.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/calendar.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/lightcase.css')}}">

		

		<!-- custom css include -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">
		<!-- Toast -->
			<link rel="stylesheet" href="{{asset('asset/css/login-register.css')}}">
			<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

	</head>

	
	<body>

	
		{!! Toastr::render() !!}
		
		@if(session('id') == 10 || session('id') == 11 )
		<script>
			$(function() {
				$('#loginModal').modal('show');
				$('#box2').hide();
				$('#box3').hide();
			});
		</script>
		@endif
 
		<!-- backtotop - start -->
		<div id="thetop" class="thetop"></div>
		<div class='backtotop'>
			<a href="#thetop" class='scroll'>
				<i class="fas fa-angle-double-up"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		{{-- <div id="preloader"></div> --}}
		<!-- preloader - end -->





		<!-- header-section - start
		================================================== -->
		<header id="header-section" class="header-section default-header-section auto-hide-header clearfix">			
			<div class="header-bottom" >
				<div class="container" >
					<div class="row" style="margin-right: : -1px; margin-left: -1px;">

						<!-- site-logo-wrapper - start -->
						<div class="col-lg-3" style="padding-left: 0px;">
							<div class="site-logo-wrapper">
								<a href="index-1.html" class="logo">
									<h3  class="title" style="color: white; margin-top: 8px;">
										KOTAK EVENT

									</h3>
									{{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
								</a>
							</div>
						</div>
						<!-- site-logo-wrapper - end -->

						<!-- mainmenu-wrapper - start -->
						<div class="col-lg-9">
							<div class="mainmenu-wrapper">
								<div class="row">									
									
								<!-- menu-item-list - start admin -->
									<div class="col-lg-12" style="padding-right: 0px; float: right;">
										<div class="menu-item-list ul-li clearfix">
											<ul>
												<li class="active">
													<a href="/buat-acara">Home</a>
												</li>
												<li>
													<a href="{{route('admin.event')}}">Kelola Event</a>
												</li>
																					
													<li class="menu-item-has-children">
													<?php 
															$name = Auth::user()->name;
															$splitName = explode(' ', $name, 2);

															$first_name = $splitName[0];
															
														?>
													<a href="" style="color: orange;"><i style="color: orange;" class="fas fa-user"></i> {{$first_name}} </a>
													<ul class="sub-menu">
														
														<li>
															<a  style="" href="{{    route('logout') }}"
															onclick="event.preventDefault();
															document.getElementById('logout-form').submit();">{{('Logout')}}
															
														</a>
														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>

													</li>
														
													</ul>												
												</li>
												
											</ul>
										</div>
									</div>
									<!-- menu-item-list - end -->
									
									
								</div>
							</div>
						</div>
						<!-- mainmenu-wrapper - end -->

					</div>
				</div>
			</div>
			<!-- header-bottom - end -->
		</header>

		<style>
			#form-messages {
				background-color: rgb(255, 232, 232);
				border: 1px solid red;
				color: red;
				display: none;
				font-size: 12px;
				font-weight: bold;
				margin-bottom: 10px;
				padding: 10px 25px;	
				max-width: 250px;
			}
		</style>
		<!-- Modal awal -->
		<div class="modal fade login" id="loginModal">
			<div class="modal-dialog login animated">
				<div class="modal-content text-center"><br>
					<div class="container text-center">
						<div class="modal-header text-center">
							<h4 align="center" class="modal-title text-center" style="color: orange; text-transform: uppercase; font-weight: 600;" >LOGIN</h4>
						</div>
					</div>
					<div class="modal-body">
						<div id="box1" class="box">
							<div class="content">
								@if(session('id')==10)
								<h3  align="center" style="color: red; text-transform: capitalize;">Password Salah</h3>
								@endif
								@if(session('id')==11)
								<h3 align="center" style=" text-transform: capitalize; color: red;">Email Tidak Terdaftar</h3>
								@endif
								<div class="form loginBox">
									<div align="center">
									<ul id="form-messages">
										<li>Generic Error #1</li>
									</ul>
									</div>
									<form id="masuk" name="masuk" method="POST" action="{{route('login')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input id="email" class="form-control" type="text" placeholder="Email" name="email" required=""><br>
										<input id="password" class="form-control" type="password" placeholder="Password" name="password" required=""><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Login</button>
										</div>
									</form>									
								</div>
								
							</div>
						</div>
						<div id="box2" class="box">
							<div class="content registerBox" style="display:none;">
								@if(session('id')==1)
								<h3 align="center" style="color: red;">Email Sudah Terdaftar</h3>
								@endif
								@if(session('id')==2)
								<h3 align="center" style="color: red;">Password Tidak Cocok</h3>
								@endif
								<div class="form">
									<form method="POST" html="{:multipart=>true}" data-remote="true" action="{{route('register')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input id="name" class="form-control" type="text" minlength="4" maxlength="100" placeholder="Nama" name="name" required><br>
										<input id="alamat" class="form-control" type="text" minlength="4" maxlength="255" placeholder="Alamat" name="alamat" required><br>
										<input id="email" class="form-control" type="text" placeholder="Email" name="email" required><br>
										<input id="password" class="form-control" type="password" minlength="6" maxlength="15" placeholder="Password" name="password" required><br>
										<input id="password_confirmation" class="form-control" type="password" minlength="6" maxlength="15" placeholder="Repeat Password" name="password_confirmation" required><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Buat Akun</button>
										</div>
									</form>
								
								</div>
							</div>
						</div>
					</div>
					<div id="box3" class="box">
							<div class="content">
								@if(session('id')==10)
								<h3  align="center" style="color: red; text-transform: capitalize;">Password Salah</h3>
								@endif
								@if(session('id')==11)
								<h3 align="center" style=" text-transform: capitalize; color: red;">Email Tidak Terdaftar</h3>
								@endif
								<div class="form loginBox">
									<form id="masuk" name="masuk" method="POST" action="{{route('login')}}" accept-charset="UTF-8">
										{{csrf_field()}}
										<input id="email" class="form-control" type="text" placeholder="Email" name="email" required=""><br>
										<div align="center">
											<button type="submit" style="background-color: #FF7E47;" class="kastem2-btn">Reset Password</button>
										</div>
									</form>									
								</div>
								
							</div>
						</div>
					<div class="modal-footer">
						<div class="forgot login-footer">
                            <span>
                            	Lupa Password?
                                 <a id="regis" href="javascript: showLupaForm();">Reset</a>
                            </span>
                            <span>
                            	Belum punya akun?
                                 <a id="regis" href="javascript: showRegisterForm();"><strong>Daftar Disini!</strong></a>
                            </span>
							
						</div>
						<div class="forgot register-footer" style="display:none">
							<span>Sudah punya akun?</span>
							<a href="javascript: showLoginForm();"><strong>Login</strong></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- header-section - end
		================================================== -->
		<!-- altranative-header - start
		================================================== -->
		@include('layouts.altranative-header-admin')
		<!-- altranative-header - end
		================================================== -->

		

		<!-- cari-event-section - start
		================================================== -->
		<!-- booking-section - start
		================================================== -->
			<section id="main" class="wrapper sec-ptb-102">
				<div class="container">
					<header class="major special">
						<h2>Dashboard Admin</h2>
					</header>
					<div>
						<div class="row">
							<div class="col-md-4 mt-5 mb-3">
								<div class="card">
									<div class="seo-fact sbg1">
										<div class="p-4 d-flex justify-content-between align-items-center">
											<div class="seofct-icon">
											<a href="/list-acara-pengajuan" style="color: #878787;">
												<i class="fa fa-spinner"></i> Pengajuan Event <br> yang belum dikonfirmasi
											</a>
										</div>
											<h2>{{$data[0]}}</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 mt-md-5 mb-3">
								<div class="card">
									<div class="seo-fact sbg2">
										<div class="p-4 d-flex justify-content-between align-items-center">
											<div class="seofct-icon">
											<a href="/list-acara-terkonfirmasi" style="color: #878787;">
												<i class="fa fa-check"></i> Pengajuan Event <br> yang disetujui
											</a>
											</div>
											<h2>{{$data[1]}}</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 mt-md-5 mb-3">
								<div class="card">
									<div class="seo-fact sbg2">
										<div class="p-4 d-flex justify-content-between align-items-center">
											<div class="seofct-icon">
											<a href="/list-acara-ditolak" style="color: #878787;">
												<i class="fa fa-warning"></i> Pengajuan Event <br> yang ditolak
											</a>
											</div>
											<h2>{{$data[2]}}</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 mt-md-5 mb-3">
								<div class="card">
									<div class="seo-fact sbg2">
										<div class="p-4 d-flex justify-content-between align-items-center">
											<div class="seofct-icon">
												<a href="/list-acara-selesai" style="color: #878787;">
												<i class="fa fa-warning"></i> Event Selesai
												</a>
											</div>
											<h2>{{$data[2]}}</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		<!-- booking-section - end
		================================================== -->
		<br><br>
		@include('layouts.footer')




		<!-- cari-event-section - end
		================================================== -->



		<!-- fraimwork - jquery include -->
		
		<script src="{{asset('assets/assets/js/popper.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>

		<!-- carousel jquery include -->
		<script src="{{asset('assets/assets/js/slick.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/owl.carousel.min.js')}}"></script>

		<!-- map jquery include -->
		<script src="{{asset('assets/assets/js/gmap3.min.js')}}"></script>
		<script src="http://maps.google.com/maps/api/js?key=AIzaSyC61_QVqt9LAhwFdlQmsNwi5aUJy9B2SyA"></script>

		<!-- calendar jquery include -->
		<script src="{{asset('assets/assets/js/atc.min.js')}}"></script>

		<!-- others jquery include -->
		<script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/isotope.pkgd.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/jarallax.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
		<script src="{{asset('assets/assets/js/lightcase.js')}}"></script>

		<!-- gallery img loaded - jqury include -->
		<script src="{{asset('assets/assets/js/imagesloaded.pkgd.min.js')}}"></script>

		<!-- multy count down - jqury include -->
		<script src="{{asset('assets/assets/js/jquery.countdown.js')}}"></script>

		

		<!-- custom jquery include -->
		<script src="{{asset('assets/assets/js/custom.js')}}"></script>
		<script src="{{asset('assets/js/login-register.js')}}"></script>


		<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

		<script>
			$(document).ready(function() {
				$('#example').DataTable();
			} );
			function nonaktifkan(klik,id){
				$(klik).attr("disabled", true);
				$.get("{{route('nonaktifkan.acara',['id'=>''])}}/"+id, function(data, status){
					console.log('success')
				});
			}
			function aktifkan(klik,id){
				$(klik).attr("disabled", false);
				$.get("{{route('aktifkan.acara',['id'=>''])}}/"+id, function(data, status){
					console.log('success')
				});
			}
		
		</script>





	</body>
</html>