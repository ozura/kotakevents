
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head><script src="//archive.org/includes/analytics.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app39.us.archive.org';v.server_ms=313;archive_analytics.send_pageview({});});</script><script type="text/javascript" src="/_static/js/ait-client-rewrite.js" charset="utf-8"></script>
<script type="text/javascript">
WB_wombat_Init("http://web.archive.org/web/", "20170605170544", "azguys.com:80");
</script>
<script type="text/javascript" src="/_static/js/wbhack.js" charset="utf-8"></script>
<script type="text/javascript">
__wbhack.init('http://web.archive.org/web');
</script>
<link rel="stylesheet" type="text/css" href="/_static/css/banner-styles.css" />
<link rel="stylesheet" type="text/css" href="/_static/css/iconochive.css" />
<!-- End Wayback Rewrite JS Include -->

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style>
    body {
    color: #000 !important;
    background-color: rgba(0,0,0,0.1) !important;
    }


    table.cards {
        background-color: transparent;
    }

    /*--[  This does the job of making the table rows appear as cards ]----------------*/
    .cards tbody img {
        height: 100px;
    }
    .cards tbody tr {
        float: left;
        margin: 10px;
        border: 1px solid #aaa;
        box-shadow: 3px 3px 6px rgba(0,0,0,0.25);
        background-color: white;
    }
    .cards tbody td {
        display: block;
        width: 330px;
        overflow: hidden;
        text-align: left;
    }

    /*---[ The remaining is just more dressing to fit my preferances ]-----------------*/
    .table {
        background-color: #fff;
    }
    .table tbody label {
        display: none;
        margin-right: 5px;
        width: 50px;
    }   
    .table .glyphicon {
        font-size: 20px;
    }

    .cards .glyphicon {
        font-size: 75px;
    }

    .cards tbody label {
        display: inline;
        position: relative;
        font-size: 85%;
        font-weight: normal;
        top: -5px;
        left: -3px;
        float: left;
        color: #808080;
    }
    .cards tbody td:nth-child(1) {
        text-align: center;
    }

</style>


    <link rel="stylesheet" type="text/css" href="http://web.archive.org/web/20170605170544cs_/https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.2/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="http://web.archive.org/web/20170605170544cs_/https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap4.min.css"/>
    <link rel="stylesheet" type="text/css" href="http://web.archive.org/web/20170605170544cs_/https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css"/>

    <script type="text/javascript" src="//web.archive.org/web/20170605170544js_/http://code.jquery.com/jquery-1.12.3.js"></script>
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://web.archive.org/web/20170605170544cs_/https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="http://web.archive.org/web/20170605170544cs_/https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="http://web.archive.org/web/20170605170544js_/https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="" crossorigin="anonymous"></script>    

    <script type="text/javascript" src="http://web.archive.org/web/20170605170544js_/https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="http://web.archive.org/web/20170605170544js_/https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="http://web.archive.org/web/20170605170544js_/https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>

    <script type="text/javascript">
        var colors = ["Crimson ", "Cyan ", "DarkBlue ", "DarkCyan ", "DarkGoldenRod ", "DarkGray ", "DarkGrey ", "DarkGreen ", "DarkKhaki ", "DarkMagenta ", "DarkOliveGreen ", "DarkOrange "];
        var sampleData = [{ "name": "Tiger Nixon", "position": "System Architect", "salary": "$320,800", "start_date": "2011/04/25", "office": "Edinburgh", "extn": "5421" }, { "name": "Garrett Winters", "position": "Accountant", "salary": "$170,750", "start_date": "2011/07/25", "office": "Tokyo", "extn": "8422" }, { "name": "Ashton Cox", "position": "Junior Technical Author", "salary": "$86,000", "start_date": "2009/01/12", "office": "San Francisco", "extn": "1562" }, { "name": "Cedric Kelly", "position": "Senior Javascript Developer", "salary": "$433,060", "start_date": "2012/03/29", "office": "Edinburgh", "extn": "6224" }, { "name": "Airi Satou", "position": "Accountant", "salary": "$162,700", "start_date": "2008/11/28", "office": "Tokyo", "extn": "5407" }, { "name": "Brielle Williamson", "position": "Integration Specialist", "salary": "$372,000", "start_date": "2012/12/02", "office": "New York", "extn": "4804" }, { "name": "Herrod Chandler", "position": "Sales Assistant", "salary": "$137,500", "start_date": "2012/08/06", "office": "San Francisco", "extn": "9608" }, { "name": "Rhona Davidson", "position": "Integration Specialist", "salary": "$327,900", "start_date": "2010/10/14", "office": "Tokyo", "extn": "6200" }, { "name": "Colleen Hurst", "position": "Javascript Developer", "salary": "$205,500", "start_date": "2009/09/15", "office": "San Francisco", "extn": "2360" }, { "name": "Sonya Frost", "position": "Software Engineer", "salary": "$103,600", "start_date": "2008/12/13", "office": "Edinburgh", "extn": "1667" }, { "name": "Jena Gaines", "position": "Office Manager", "salary": "$90,560", "start_date": "2008/12/19", "office": "London", "extn": "3814" }, { "name": "Quinn Flynn", "position": "Support Lead", "salary": "$342,000", "start_date": "2013/03/03", "office": "Edinburgh", "extn": "9497" }, { "name": "Charde Marshall", "position": "Regional Director", "salary": "$470,600", "start_date": "2008/10/16", "office": "San Francisco", "extn": "6741" }, { "name": "Haley Kennedy", "position": "Senior Marketing Designer", "salary": "$313,500", "start_date": "2012/12/18", "office": "London", "extn": "3597" }, { "name": "Tatyana Fitzpatrick", "position": "Regional Director", "salary": "$385,750", "start_date": "2010/03/17", "office": "London", "extn": "1965" }, { "name": "Michael Silva", "position": "Marketing Designer", "salary": "$198,500", "start_date": "2012/11/27", "office": "London", "extn": "1581" }, { "name": "Paul Byrd", "position": "Chief Financial Officer (CFO)", "salary": "$725,000", "start_date": "2010/06/09", "office": "New York", "extn": "3059" }, { "name": "Gloria Little", "position": "Systems Administrator", "salary": "$237,500", "start_date": "2009/04/10", "office": "New York", "extn": "1721" }, { "name": "Bradley Greer", "position": "Software Engineer", "salary": "$132,000", "start_date": "2012/10/13", "office": "London", "extn": "2558" }, { "name": "Dai Rios", "position": "Personnel Lead", "salary": "$217,500", "start_date": "2012/09/26", "office": "Edinburgh", "extn": "2290" }, { "name": "Jenette Caldwell", "position": "Development Lead", "salary": "$345,000", "start_date": "2011/09/03", "office": "New York", "extn": "1937" }, { "name": "Yuri Berry", "position": "Chief Marketing Officer (CMO)", "salary": "$675,000", "start_date": "2009/06/25", "office": "New York", "extn": "6154" }, { "name": "Caesar Vance", "position": "Pre-Sales Support", "salary": "$106,450", "start_date": "2011/12/12", "office": "New York", "extn": "8330" }, { "name": "Doris Wilder", "position": "Sales Assistant", "salary": "$85,600", "start_date": "2010/09/20", "office": "Sidney", "extn": "3023" }, { "name": "Angelica Ramos", "position": "Chief Executive Officer (CEO)", "salary": "$1,200,000", "start_date": "2009/10/09", "office": "London", "extn": "5797" }, { "name": "Gavin Joyce", "position": "Developer", "salary": "$92,575", "start_date": "2010/12/22", "office": "Edinburgh", "extn": "8822" }, { "name": "Jennifer Chang", "position": "Regional Director", "salary": "$357,650", "start_date": "2010/11/14", "office": "Singapore", "extn": "9239" }, { "name": "Brenden Wagner", "position": "Software Engineer", "salary": "$206,850", "start_date": "2011/06/07", "office": "San Francisco", "extn": "1314" }, { "name": "Fiona Green", "position": "Chief Operating Officer (COO)", "salary": "$850,000", "start_date": "2010/03/11", "office": "San Francisco", "extn": "2947" }, { "name": "Shou Itou", "position": "Regional Marketing", "salary": "$163,000", "start_date": "2011/08/14", "office": "Tokyo", "extn": "8899" }, { "name": "Michelle House", "position": "Integration Specialist", "salary": "$95,400", "start_date": "2011/06/02", "office": "Sidney", "extn": "2769" }, { "name": "Suki Burks", "position": "Developer", "salary": "$114,500", "start_date": "2009/10/22", "office": "London", "extn": "6832" }, { "name": "Prescott Bartlett", "position": "Technical Author", "salary": "$145,000", "start_date": "2011/05/07", "office": "London", "extn": "3606" }, { "name": "Gavin Cortez", "position": "Team Leader", "salary": "$235,500", "start_date": "2008/10/26", "office": "San Francisco", "extn": "2860" }, { "name": "Martena Mccray", "position": "Post-Sales support", "salary": "$324,050", "start_date": "2011/03/09", "office": "Edinburgh", "extn": "8240" }, { "name": "Unity Butler", "position": "Marketing Designer", "salary": "$85,675", "start_date": "2009/12/09", "office": "San Francisco", "extn": "5384" }, { "name": "Howard Hatfield", "position": "Office Manager", "salary": "$164,500", "start_date": "2008/12/16", "office": "San Francisco", "extn": "7031" }, { "name": "Hope Fuentes", "position": "Secretary", "salary": "$109,850", "start_date": "2010/02/12", "office": "San Francisco", "extn": "6318" }, { "name": "Vivian Harrell", "position": "Financial Controller", "salary": "$452,500", "start_date": "2009/02/14", "office": "San Francisco", "extn": "9422" }, { "name": "Timothy Mooney", "position": "Office Manager", "salary": "$136,200", "start_date": "2008/12/11", "office": "London", "extn": "7580" }, { "name": "Jackson Bradshaw", "position": "Director", "salary": "$645,750", "start_date": "2008/09/26", "office": "New York", "extn": "1042" }, { "name": "Olivia Liang", "position": "Support Engineer", "salary": "$234,500", "start_date": "2011/02/03", "office": "Singapore", "extn": "2120" }, { "name": "Bruno Nash", "position": "Software Engineer", "salary": "$163,500", "start_date": "2011/05/03", "office": "London", "extn": "6222" }, { "name": "Sakura Yamamoto", "position": "Support Engineer", "salary": "$139,575", "start_date": "2009/08/19", "office": "Tokyo", "extn": "9383" }, { "name": "Thor Walton", "position": "Developer", "salary": "$98,540", "start_date": "2013/08/11", "office": "New York", "extn": "8327" }, { "name": "Finn Camacho", "position": "Support Engineer", "salary": "$87,500", "start_date": "2009/07/07", "office": "San Francisco", "extn": "2927" }, { "name": "Serge Baldwin", "position": "Data Coordinator", "salary": "$138,575", "start_date": "2012/04/09", "office": "Singapore", "extn": "8352" }, { "name": "Zenaida Frank", "position": "Software Engineer", "salary": "$125,250", "start_date": "2010/01/04", "office": "New York", "extn": "7439" }, { "name": "Zorita Serrano", "position": "Software Engineer", "salary": "$115,000", "start_date": "2012/06/01", "office": "San Francisco", "extn": "4389" }, { "name": "Jennifer Acosta", "position": "Junior Javascript Developer", "salary": "$75,650", "start_date": "2013/02/01", "office": "Edinburgh", "extn": "3431" }, { "name": "Cara Stevens", "position": "Sales Assistant", "salary": "$145,600", "start_date": "2011/12/06", "office": "New York", "extn": "3990" }, { "name": "Hermione Butler", "position": "Regional Director", "salary": "$356,250", "start_date": "2011/03/21", "office": "London", "extn": "1016" }, { "name": "Lael Greer", "position": "Systems Administrator", "salary": "$103,500", "start_date": "2009/02/27", "office": "London", "extn": "6733" }, { "name": "Jonas Alexander", "position": "Developer", "salary": "$86,500", "start_date": "2010/07/14", "office": "San Francisco", "extn": "8196" }, { "name": "Shad Decker", "position": "Regional Director", "salary": "$183,000", "start_date": "2008/11/13", "office": "Edinburgh", "extn": "6373" }, { "name": "Michael Bruce", "position": "Javascript Developer", "salary": "$183,000", "start_date": "2011/06/27", "office": "Singapore", "extn": "5384" }, { "name": "Donna Snider", "position": "Customer Support", "salary": "$112,000", "start_date": "2011/01/25", "office": "New York", "extn": "4226" }];

        var table = null;

        var colDefs = [
            {   /* created column to show a picture just to make this demo look better */
                orderable: false, data: 'Photo', name: 'Photo', orderable: false, defaultContent: '', title: 'Photo',
                visible: true, className: 'text-center', width: '20px',

                createdCell: function (td, cellData, rowData, row, col) {
                    var $ctl = $('<i class="glyphicon glyphicon-user"></i>').css('color', colors[Math.round(Math.random() * colors.length) + 1])
                    $(td).append($ctl);
                }
            },
            /* I added a label to the column for the field name which will show up in the card display */
            {
                data: "name", name: "name", title: "Name", visible: true,
                render: function (data, type, full, meta) { return '<label>Name:</label>' + data; }
            },
            {
                data: "position", name: "position", title: "Position", visible: true,
                render: function (data, type, full, meta) { return '<label>Position:</label>' + data; }
            },
            {
                data: "salary", name: "salary", title: "Salary", visible: true, class: 'text-right',
                render: function (data, type, full, meta) { return '<label>Salary:</label>' + data; }
            },
            {
                data: "start_date", name: "start_date", title: "Start", visible: true, class: 'text-right',
                render: function (data, type, full, meta) { return '<label>Start:</label>' + data; }
            },
            {
                data: "office", name: "office", title: "Office", visible: true,
                render: function (data, type, full, meta) { return '<label>Office:</label>' + data; }
            },
            {
                data: "extn", name: "extn", title: "Extn", visible: true,
                render: function (data, type, full, meta) { return '<label>Extn:</label>' + data; }
            }
        ];

        $(document).ready(function () {

            table = $('#register').DataTable({
                data: sampleData,
                columns: colDefs,
                pagingType: 'full_numbers',
                lengthMenu: [3, 6, 9, 12, 15, 18, 20],
                pageLength: 6,
                select: 'single'
            })
            .on('select', function (e, dt, type, indexes) {
                var rowData = table.rows(indexes).data().toArray();
                $('.alert').html('rowData: ' + JSON.stringify(rowData));
            })

            $('#btToggleDisplay').on('click', function () {
                $("#register").toggleClass('cards')
                $("#register thead").toggle()
            })

            $('body').on('click', '#register', function () {
                var tr = $(this).closest('tr');
                var rowData = table.row(this).data()
                console.log('rowData:', rowData)


            })


        });// END - $(document).ready
    </script>
    
 
</head>
<body><!-- BEGIN WAYBACK TOOLBAR INSERT -->
<script type="text/javascript" src="/_static/js/timestamp.js" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/graph-calc.js" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/auto-complete.js" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/toolbar.js" charset="utf-8"></script>
<style type="text/css">
body {
  margin-top:0 !important;
  padding-top:0 !important;
  /*min-width:800px !important;*/
}
.wb-autocomplete-suggestions {
    text-align: left; cursor: default; border: 1px solid #ccc; border-top: 0; background: #fff; box-shadow: -1px 1px 3px rgba(0,0,0,.1);
    position: absolute; display: none; z-index: 2147483647; max-height: 254px; overflow: hidden; overflow-y: auto; box-sizing: border-box;
}
.wb-autocomplete-suggestion { position: relative; padding: 0 .6em; line-height: 23px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-size: 1.02em; color: #333; }
.wb-autocomplete-suggestion b { font-weight: bold; }
.wb-autocomplete-suggestion.selected { background: #f0f0f0; }
</style>
<div id="wm-ipp-base" lang="en" style="display:none;direction:ltr;">
<div id="wm-ipp" style="position:fixed;left:0;top:0;right:0;">
<div id="wm-ipp-inside">
  <div style="position:relative;">
    <div id="wm-logo" style="float:left;width:130px;padding-top:10px;">
      <a href="/web/" title="Wayback Machine home page"><img src="/_static/images/toolbar/wayback-toolbar-logo.png" alt="Wayback Machine" width="110" height="39" border="0" /></a>
    </div>
    <div class="r" style="float:right;">
      <div id="wm-btns" style="text-align:right;height:25px;">
                  <div id="wm-save-snapshot-success">success</div>
          <div id="wm-save-snapshot-fail">fail</div>
          <a href="#"
             onclick="__wm.saveSnapshot('http://azguys.com/datatables/index.html', '20170605170544')"
             title="Share via My Web Archive"
             id="wm-save-snapshot-open"
          >
            <span class="iconochive-web"></span>
          </a>
          <a href="https://archive.org/account/login.php"
             title="Sign In"
             id="wm-sign-in"
          >
            <span class="iconochive-person"></span>
          </a>
          <span id="wm-save-snapshot-in-progress" class="iconochive-web"></span>
        	<a href="http://faq.web.archive.org/" title="Get some help using the Wayback Machine" style="top:-6px;"><span class="iconochive-question" style="color:rgb(87,186,244);font-size:160%;"></span></a>
	<a id="wm-tb-close" href="#close" onclick="__wm.h(event);return false;" style="top:-2px;" title="Close the toolbar"><span class="iconochive-remove-circle" style="color:#888888;font-size:240%;"></span></a>
      </div>
      <div id="wm-share" style="text-align:right;">
	<a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=http://web.archive.org/web/20170605170544/http://azguys.com:80/datatables/index.html', '', 'height=400,width=600'); return false;" title="Share on Facebook" style="margin-right:5px;" target="_blank"><span class="iconochive-facebook" style="color:#3b5998;font-size:160%;"></span></a>
	<a href="#" onclick="window.open('https://twitter.com/intent/tweet?text=http://web.archive.org/web/20170605170544/http://azguys.com:80/datatables/index.html&amp;via=internetarchive', '', 'height=400,width=600'); return false;" title="Share on Twitter" style="margin-right:5px;" target="_blank"><span class="iconochive-twitter" style="color:#1dcaff;font-size:160%;"></span></a>
      </div>
    </div>
    <table class="c" style="">
      <tbody>
	<tr>
	  <td class="u" colspan="2">
	    <form target="_top" method="get" action="/web/submit" name="wmtb" id="wmtb"><input type="text" name="url" id="wmtbURL" value="http://azguys.com/datatables/index.html" onfocus="this.focus();this.select();" /><input type="hidden" name="type" value="replay" /><input type="hidden" name="date" value="20170605170544" /><input type="submit" value="Go" /></form>
	  </td>
	  <td class="n" rowspan="2" style="width:110px;">
	    <table>
	      <tbody>
		<!-- NEXT/PREV MONTH NAV AND MONTH INDICATOR -->
		<tr class="m">
		  <td class="b" nowrap="nowrap"><a href="http://web.archive.org/web/20170505080228/http://azguys.com:80/datatables/index.html" title="05 May 2017"><strong>May</strong></a></td>
		  <td class="c" id="displayMonthEl" title="You are here: 17:05:44 Jun 05, 2017">JUN</td>
		  <td class="f" nowrap="nowrap"><a href="http://web.archive.org/web/20170708232546/http://azguys.com:80/datatables/index.html" title="08 Jul 2017"><strong>Jul</strong></a></td>
		</tr>
		<!-- NEXT/PREV CAPTURE NAV AND DAY OF MONTH INDICATOR -->
		<tr class="d">
		  <td class="b" nowrap="nowrap"><a href="http://web.archive.org/web/20170505080228/http://azguys.com:80/datatables/index.html" title="08:02:28 May 05, 2017"><img src="/_static/images/toolbar/wm_tb_prv_on.png" alt="Previous capture" width="14" height="16" border="0" /></a></td>
		  <td class="c" id="displayDayEl" style="width:34px;font-size:24px;white-space:nowrap;" title="You are here: 17:05:44 Jun 05, 2017">05</td>
		  <td class="f" nowrap="nowrap"><a href="http://web.archive.org/web/20170708232546/http://azguys.com:80/datatables/index.html" title="23:25:46 Jul 08, 2017"><img src="/_static/images/toolbar/wm_tb_nxt_on.png" alt="Next capture" width="14" height="16" border="0" /></a></td>
		</tr>
		<!-- NEXT/PREV YEAR NAV AND YEAR INDICATOR -->
		<tr class="y">
		  <td class="b" nowrap="nowrap">2016</td>
		  <td class="c" id="displayYearEl" title="You are here: 17:05:44 Jun 05, 2017">2017</td>
		  <td class="f" nowrap="nowrap"><a href="http://web.archive.org/web/20180820173137/http://azguys.com:80/datatables/index.html" title="20 Aug 2018"><strong>2018</strong></a></td>
		</tr>
	      </tbody>
	    </table>
	  </td>
	</tr>
	<tr>
	  <td class="s">
	    	    <div id="wm-nav-captures">
	      	      <a class="t" href="/web/20170605170544*/http://azguys.com/datatables/index.html" title="See a list of every capture for this URL">22 captures</a>
	      <div class="r" title="Timespan for captures of this URL">20 Mar 2017 - 01 May 2019</div>
	      </div>
	  </td>
	  <td class="k">
	    <a href="" id="wm-graph-anchor">
	      <div id="wm-ipp-sparkline" title="Explore captures for this URL" style="position: relative">
		<canvas id="wm-sparkline-canvas" width="600" height="27" border="0"></canvas>
	      </div>
	    </a>
	  </td>
	</tr>
      </tbody>
    </table>
    <div style="position:absolute;bottom:0;right:2px;text-align:right;">
      <a id="wm-expand" class="wm-btn wm-closed" href="#expand" onclick="__wm.ex(event);return false;"><span id="wm-expand-icon" class="iconochive-down-solid"></span> <span style="font-size:80%">About this capture</span></a>
    </div>
  </div>
    <div id="wm-capinfo" style="border-top:1px solid #777;display:none; overflow: hidden">
            <div style="background-color:#666;color:#fff;font-weight:bold;text-align:center">COLLECTED BY</div>
    <div style="padding:3px;position:relative" id="wm-collected-by-content">
            <div style="display:inline-block;vertical-align:top;width:50%;">
			<span class="c-logo" style="background-image:url(https://archive.org/services/img/alexacrawls);"></span>
		Organization: <a style="color:#33f;" href="https://archive.org/details/alexacrawls" target="_new"><span class="wm-title">Alexa Crawls</span></a>
		<div style="max-height:75px;overflow:hidden;position:relative;">
	  <div style="position:absolute;top:0;left:0;width:100%;height:75px;background:linear-gradient(to bottom,rgba(255,255,255,0) 0%,rgba(255,255,255,0) 90%,rgba(255,255,255,255) 100%);"></div>
	  Starting in 1996, <a href="http://www.alexa.com/">Alexa Internet</a> has been donating their crawl data to the Internet Archive.  Flowing in every day, these data are added to the <a href="http://web.archive.org/">Wayback Machine</a> after an embargo period.
	</div>
	      </div>
      <div style="display:inline-block;vertical-align:top;width:49%;">
			<span class="c-logo" style="background-image:url(https://archive.org/services/img/alexacrawls)"></span>
		<div>Collection: <a style="color:#33f;" href="https://archive.org/details/alexacrawls" target="_new"><span class="wm-title">Alexa Crawls</span></a></div>
		<div style="max-height:75px;overflow:hidden;position:relative;">
	  <div style="position:absolute;top:0;left:0;width:100%;height:75px;background:linear-gradient(to bottom,rgba(255,255,255,0) 0%,rgba(255,255,255,0) 90%,rgba(255,255,255,255) 100%);"></div>
	  Starting in 1996, <a href="http://www.alexa.com/">Alexa Internet</a> has been donating their crawl data to the Internet Archive.  Flowing in every day, these data are added to the <a href="http://web.archive.org/">Wayback Machine</a> after an embargo period.
	</div>
	      </div>
    </div>
    <div style="background-color:#666;color:#fff;font-weight:bold;text-align:center" title="Timestamps for the elements of this page">TIMESTAMPS</div>
    <div>
      <div id="wm-capresources" style="margin:0 5px 5px 5px;max-height:250px;overflow-y:scroll !important"></div>
      <div id="wm-capresources-loading" style="text-align:left;margin:0 20px 5px 5px;display:none"><img src="/_static/images/loading.gif" alt="loading" /></div>
    </div>
  </div></div></div></div><script type="text/javascript">
__wm.bt(600,27,25,2,"web","http://azguys.com/datatables/index.html","2017-06-05",1996,"/_static/",['css/banner-styles.css','css/iconochive.css']);
</script>
<!-- END WAYBACK TOOLBAR INSERT -->
    <br>
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <h3>DataTables as Card View:</h3>
                <ul>
                    <li>Simple CSS allows the table to switch from normal table layout view to a card type view.</li>
                    <li>No hiding of the datatable and creating a secondary card type display.</li>
                    <li>Retains all datatable API interoperability, paging and filtering, row select.</li>
                </ul>

            </div>
            <div class="col-sm-12">
                <button id="btToggleDisplay" class="btn btn-info">Toggle table and card view</button>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-sm-12">

                <table id="register" class="table table-bordered compact dataTable no-footer" cellspacing="0" style="width: 100%;">
                </table>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-success"></div>
            </div>
        </div>
    </div><!--END - container-fluid-->

</body>
</html><!--
     FILE ARCHIVED ON 17:05:44 Jun 05, 2017 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 11:37:27 Nov 15, 2019.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  exclusion.robots.policy: 0.164
  load_resource: 169.633
  esindex: 0.012
  PetaboxLoader3.resolve: 143.464
  captures_list: 138.3
  PetaboxLoader3.datanode: 54.238 (4)
  CDXLines.iter: 11.013 (3)
  LoadShardBlock: 120.784 (3)
  exclusion.robots: 0.177
  RedisCDXSource: 3.617
-->