<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        

        <<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/bootstrap.min.css')}}">

        <!-- icon css include -->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/fontawesome-all.css')}}">
        
        <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/jquery.mCustomScrollbar.min.css')}}">


        

        <!-- custom css include -->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/css/style.css')}}">

        
        <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="{{asset('css/hover.dataTables.min.css')}}">
        {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> --}}





        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

        {{-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> --}}
        <script type="text/javascript" src="{{asset('css/jqueryDatatable2.min.js')}}"></script>
        <script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        
        <link type="text/css" rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}">
        {{-- <link type="text/css" rel="stylesheet" href="{{asset('css/bulma.css')}}"> --}}

        <script type="text/javascript" src="{{asset('css/sweetalert2.min.js')}}"></script>



    </head>

    
    <body>

 
        <!-- backtotop - start -->
        <div id="thetop" class="thetop"></div>
        <div class='backtotop'>
            <a href="#thetop" class='scroll'>
                <i class="fas fa-angle-double-up"></i>
            </a>
        </div>
        <!-- backtotop - end -->

        <!-- preloader - start -->
        {{-- <div id="preloader"></div> --}}
        <!-- preloader - end -->
        <style>
            .modal-open {
                
                overflow: scroll;
                width: 100%;
                padding-right: 0!important;
            }
        </style>

 


        <!-- header-section - start
        ================================================== -->
                <header id="header-section" class="header-section default-header-section auto-hide-header clearfix">
            
            <!-- header-bottom - start -->
            <div class="header-bottom" >
                <div class="container" >
                    <div class="row" style="margin-right: : -1px; margin-left: -1px;">

                        <!-- site-logo-wrapper - start -->
                        <div class="col-lg-3" style="padding-left: 0px;">
                            <div class="site-logo-wrapper">
                                <a href="index-1.html" class="logo">
                                        <h3  class="title" style="color: white; margin-top: 8px; font-weight: 700;">
                                        KOTAK EVENT

                                    </h3>
                                    {{-- <img src="assets/assets/images/1.site-logo.png" alt="logo_not_found"> --}}
                                </a>
                            </div>
                        </div>
                        <!-- site-logo-wrapper - end -->

                        <!-- mainmenu-wrapper - start -->
                        <div class="col-lg-9">
                            <div class="mainmenu-wrapper">
                                <div class="row">                                                       
                                    <!-- menu-item-list - start -->
                                    <div class="col-lg-12" style="padding-right: 0px; float: right;">
                                        <div class="menu-item-list ul-li clearfix">
                                            <ul>
                                                <li >
                                                    <a href="/">Home</a>
                                                </li>
                                                <li >
                                                    <a href="/cari">Cari Event</a>
                                                </li>
                                                <li>
                                                    <a href="/buat-acara">Buat Event</a>
                                                </li>
                                                <li class="active"><a href="/acara-saya">Event Saya</a></li>
                                                <li><a <a href="/undangan-saya">Tiket</a></li>
                                                
                                                <li class="menu-item-has-children">
                                                    
                                                    <a href="" style="color: white;"><i style="color: white;" class="fas fa-user"></i> {{Auth::user()->name}}</a>
                                                    <ul class="sub-menu">
                                                        
                                                        <li>
                                                            <a  style="" href="{{    route('logout') }}"
                                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">{{('Logout')}}
                                                            
                                                        </a>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>

                                                    </li>
                                                        
                                                    </ul>                                               
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                    <!-- menu-item-list - end -->                                   
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <!-- mainmenu-wrapper - end -->

                    </div>
                </div>
            </div>
            <!-- header-bottom - end -->
        </header>

        
        <!-- header-section - end
        ================================================== -->
        <!-- altranative-header - start
        ================================================== -->
                @include('layouts.altranative-header')
        <!-- altranative-header - end
        ================================================== -->

        

        <!-- cari-event-section - start
        ================================================== -->
        <!-- booking-section - start
        ================================================== -->
         
        <div class="container" >
         <div>
                
                <p></p>
                <a style="padding-top: 2em;" href="{{ url('/')}}"><p>Home ></p></a>
                <a href="/acara-saya"><p>Event ></p></a>
                <a href="{{route('data.tamu',['id'=>$id_event])}}"><p>Data Tamu {{$event->nama}}</p></a>
                {{-- <a href="{{ url('/acara/{id}')}}">Event 1</a> --}}
            </div>   
            {{-- <div align="right" style="padding-top: 10px;">
            <a href="{{route('acara.saya')}}">
                <button style="color: white; text-align: right;"  type="button" class="btn btn-info"><i class="fas fa-angle-left"></i> Back</button>
            </a>
            </div> --}}
            <div class="">
                
                <div class="">
                    <div>
                        <form method="POST" action="{{URL::to('downloadFormat')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="file_name" value="data-tamu.xlsx">
                            <button  type="submit" class="btn btn-success btn-sm" align="left"><i class="fas fa-download"></i> Format</button>
                        </form>
                    </div>
                    <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        
                        
                        <br>
                        <div class="input-group">
                              
                            <input type="file" name="file" class="form-control" required="">
                            <input type="hidden" name="id_event" value="{{$id_event}}">
                            <span class="input-group-btn">
                                <button  class="btn btn-primary">Import Data Tamu</button>
                            </span>
                        </div>                                                

                        {{-- <a class="btn btn-warning" href="{{ route('export') }}">Export User Data</a> --}}
                    </form>
                </div>

            </div>

        </div>

        <br>
        
        <style>
            .dtr-data{
                display: inline-block;
                float: right;
            }
        </style>
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <div align="center"><strong>{{ $message }}!</strong></div>
        </div>
        @endif


  <!-- Modal -->
  
  <div class="modal" id="modaldata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header border-bottom-0">
            <h4 class="modal-title" id="exampleModalLabel">Data Tamu</h4>

        </div>
        <input type="hidden" name="data_id" value="">
        
        <div class="modal-body">
            <div class="form-group">
                <label for="nama_tamu">Nama</label>
                <input type="text" name="nama_tamu" class="form-control" aria-describedby="emailHelp" required="">



            </div>
            <div class="form-group">
                <label for="email_tamu">Email</label>
                <input type="email" name="email_tamu" class="form-control"  aria-describedby="emailHelp" required="" > 
            </div>
            <div class="form-group">
                <label for="alamat_tamu">Alamat</label>
                <input type="text" name="alamat_tamu" class="form-control"  aria-describedby="emailHelp" required="">
            </div>
            <div class="form-group">
                <label for="kategori">Kategori</label>
                <select name="kategori" required="">

                    @foreach(\App\Category::select('id','id_event', 'nama_kategori')->where('id_event',$id_event)->get() as $kat) 
                   <option value="{{$kat->id}}">{{$kat->nama_kategori}}</option>
                   @endforeach

                </select>
            </div>



        </div>
        <div class="modal-footer border-top-0 d-flex justify-content-right">

            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" id="button" onclick="saveAction(this)" class="btn btn-info" data-dismiss="modal">Simpan</button>

        </div>

    </div>
</div>
</div>



  <div class="modal" id="modalkategori" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header border-bottom-0">
            <h4 class="modal-title" id="exampleModalLabel">Kategori</h4>

        </div>
        <input type="hidden" name="data_id" value="">
        
        <div class="modal-body">
            <div class="form-group">
                <label for="nama_tamu">Nama Kategori</label>
                <input type="text" name="nama_kategori" class="form-control" aria-describedby="emailHelp" required="">
            </div>
        </div>
        <div class="modal-footer border-top-0 d-flex justify-content-right">

            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" id="button" onclick="saveActionKategori(this)" class="btn btn-info" data-dismiss="modal">Simpan</button>

        </div>

    </div>
</div>
</div>
<!-- Modal -->



         {{--    <div class="container" align="right">
                <a href="{{route('email.tamu',['id'=>$id_event])}}">
                    <button style="color: white;"  type="button" class="btn btn-danger btn-sm"><i class="fas fa-file-pdf"></i> Kirim Undangan</button>
                </a>
            </div> --}}

        <section id="main" class="wrapper " style="padding-top: 10px;">
                <div class="container">
                    
                    <header class="major special" style="background-color: #000000;">
                        <h2 align="center"style="font-family: comic sans; text-transform: uppercase; color: white;">{{$event->nama}}</h2>



                    </header>
                    <div align="left">

                    <button style="color: white; background-color: #000000;" data-dismiss="modal" onclick="reset(this)" data-target="#modalkategori" data-toggle="modal"  type="button" class="btn-sm"><i class="fas fa-plus"></i> Kategori</button>   
                    <button style="color: white; background-color: #000000;" data-dismiss="modal" onclick="reset(this)" data-target="#modaldata" data-toggle="modal"  type="button" class="btn-sm"><i class="fas fa-plus"></i> Data Tamu</button>                    
                    </div>

                    <div>
                        <table id="primary_table" class="table hover" >
                            <thead>
                                <div align="right">
                                    <a href="{{route('download.peserta',['id'=>$id_event])}}">
                                        <button style="color: white;"  type="button" class="btn btn-success btn-sm"><i class="fas fa-download"></i> PDF Tamu</button>
                                    </a>
                                </div>
                            <tr align="center" style="color: black;">
                                {{-- <th>Foto</th> --}}
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Alamat</th>
                                <th>Kategori</th>
                                <th>Action</th>
                            </tr>
                            </thead>                            
                        </table>
                    </div>
                </div>
            </section>
        <!-- booking-section - end
        ================================================== -->

        <style>
            td{
                text-align: center;
                color: black;
                font-size: 13px;
            }
        </style>
        <script>
            var primary_table = $('#primary_table').DataTable({
        responsive: true,                
        processing: true,
        serverSide: true,
        ajax: {
            url:  'datatamu-list?id_event={{$id_event}}',
            type: 'get'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'nama_tamu', name: 'nama_tamu' },
            { data: 'email_tamu', name: 'email_tamu' },
            { data: 'alamat_tamu', name: 'alamat_tamu' },
            { data: 'nama_kategori', name: 'nama_kategori' },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data){
                    if(data.tiket == 'Terkirim')
                    return '<a data-target="#modaldata" data-toggle="modal" onclick="editActionPopUp(this)"  data-id="'+data.id+'" data-content="'+data.content+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="fas fa-edit"></i>'+
                    '    </span>'+
                    '    <span>Edit  .</span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-danger is-outlined" onclick="deleteAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="fas fa-window-close"></i>'+
                    '    </span>'+
                    '    <span style="color:red;">Delete   .</span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-danger is-outlined" >'+                    
                    '    <Button class="btn btn-success btn-sm" disabled><i class="fas fa-check-square"></i></Button>'+
                    '</a>';
                    else{
                        return '<a data-target="#modaldata" data-toggle="modal" onclick="editActionPopUp(this)"  data-id="'+data.id+'" data-content="'+data.content+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="fas fa-edit"></i>'+
                    '    </span>'+
                    '    <span>Edit   .</span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-danger is-outlined" onclick="deleteAction(this)" data-id="'+data.id+'">'+
                    '    <span class="icon is-small">'+
                    '        <i class="fas fa-window-close"></i>'+
                    '    </span>'+
                    '    <span style="color:red;">Delete   .</span>'+
                    '</a>'+
                    '<a class="button is-small is-rounded is-danger is-outlined" onclick="kirimAction(this)" data-id="'+data.id+'">'+                    
                    '    <button class="btn btn-primary btn-sm">Kirim Tiket</button>'+
                    '</a>';
                    }
                    
                    
                    
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();


        function editActionPopUp(element){


        var item = $(element);
        var id = item.attr('data-id');
        var data = JSON.parse(item.attr('data-content'));
        
        $('input[name=nama_tamu]').val(data.nama_tamu);
        $('input[name=email_tamu]').val(data.email_tamu);
        $('input[name=alamat_tamu]').val(data.alamat_tamu);
        $('select[name=kategori').val(data.id_kategori);
        $('input[name=data_id]').val(id);
        
        
        
        
        
    }
        function deleteAction(element){
        var item = $(element);
        
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }

        swal({
            title: 'Delete Data',
            text: "Apakah Anda yakin menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "get",
                        url: 'datatamu-delete',
                        data: {
                            id: item.attr('data-id')
                        },
                        success: function (result) {
                            if(result.status == 200){
                                swal('Sukses!', result.message, 'success')
                                primary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                        complete: function() {
                            item.removeClass('is-loading');
                        }
                    });
                }else{
                    item.removeClass('is-loading');
                }
        });
    }

      function kirimAction(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "get",
                    url: 'datatamu-kirim-tiket',
                    data: {
                        id: item.attr('data-id')
                    },
                   success: function (result) {
                            if(result.status == 200){
                                swal('Sukses!', result.message, 'success')
                                primary_table.ajax.reload(null, false);
                            }else{
                                swal('Oops', result.message, 'error')
                            }
                        },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }

    function saveAction(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "get",
                    url: 'datatamu-update',
                    data: {
                        nama_tamu: $('input[name=nama_tamu]').val(),
                        email_tamu: $('input[name=email_tamu]').val(),
                        alamat_tamu: $('input[name=alamat_tamu]').val(),
                        data_id: $('input[name=data_id]').val(),
                        kategori: $('select[name=kategori]').val(),
                        id_event:{{$id_event}},
                    },
                    success: function (result) {
                        if(result.status == 200){
                            $('input[name=nama_tamu]').val();
                            $('input[name=email_tamu]').val();
                            $('input[name=alamat_tamu]').val();
                            swal('Sukses!', result.message, 'success')
                            primary_table.ajax.reload(null, false);
                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }

       function saveActionKategori(element){
        var item = $(element);
        if(item.hasClass('is-loading')){
            return false;
        }else{
            item.addClass('is-loading');
        }
        
                $.ajax({
                    type: "get",
                    url: 'datatamu-add-kategori',
                    data: {
                        nama_kategori: $('input[name=nama_kategori]').val(),                        
                        id_event:{{$id_event}},
                    },
                    success: function (result) {
                        if(result.status == 200){
                             
                            
                            
                            window.location.reload();                            
                            swal('Good job!', result.message, 'success')

                        }else{
                            swal('Oops', result.message, 'error')
                        }
                    },
                    complete: function() {
                        item.removeClass('is-loading');
                    }
                });
    }


    function reset(element){

        $(':input').val('');

    }



     $(document).on('click', '.tabs li', function(){
        var item = $(this);
        var target = item.attr('data-target');
        $('.tabs li').each(function() {
            $(this).removeClass('is-active');
            $($(this).attr('data-target')).hide();
        });

        item.addClass('is-active');
        $(target).show();
    });
   

        </script>   
      




        <!-- cari-event-section - end
        ================================================== -->



        <!-- fraimwork - jquery include -->
<script src="{{asset('assets/assets/js/bootstrap.min.js')}}"></script>

       
        




            <script src="{{asset('assets/assets/js/atc.min.js')}}"></script>

        <!-- others jquery include -->
        <script src="{{asset('assets/assets/js/jquery.magnific-popup.min.js')}}"></script>
        
        
        <script src="{{asset('assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    

        <!-- custom jquery include -->
        <script src="{{asset('assets/assets/js/custom.js')}}"></script>


    </body>
</html>