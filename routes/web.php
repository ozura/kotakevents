<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Event;
use Carbon\Carbon;

// if (env('APP_ENV') === 'production') {
//     // URL::forceScheme('https');


// function redirectTohttps() {

// if($_SERVER['HTTPS']!='on') {

// $redirect= 'https://'.$_SERVER[‘HTTP_HOST’].$_SERVER[‘REQUEST_URI’];

// header('Location:$redirect'); } 


// }
// }
Route::get('/', function () {

    $hadir = \Carbon\Carbon::now();
    // dd($hadir);

    // $top3 = Cache::store('file')->get('top3');
    $top3 = [21,27,24];
    
    foreach ($top3 as $key=>$top){
        $data[$key] = Event::find($top);
    }
    $top8 = Event::limit(6)->where('status','Sudah Dikonfirmasi')->whereNull('kode_unik')->where('jenis_event','open')->orderBy('id','desc')->get();
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
    // $week = Event::whereBetween('jadwal_mulai', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->orderBy('jadwal_mulai','ASC')->get();
    // $seven = Event::where('status','Sudah Dikonfirmasi')->whereNull('kode_unik')->where('jadwal_mulai','>',date('Y-m-d', strtotime('+7 days')))->orderBy('jadwal_mulai','ASC')->get();

    $date = \Carbon\Carbon::today()->addDays(7);
    $today = \Carbon\Carbon::today();
    $seven = Event::where('jadwal_mulai', '<=', $date)->where('jadwal_mulai', '>=', $today)->where('status','Sudah Dikonfirmasi')->whereNull('kode_unik')->where('jenis_event','open')->orderBy('jadwal_mulai','ASC')->limit(7)->get();
    // dd($seven);
    // dd($top8);
    
    if(!Auth::user()){
        return view('index-1')->with('top3',$data)->with('top8',$top8)->with('seven',$seven);
    }elseif(Auth::user()->email=='adebwahyu@gmail.com'){
        return redirect('dashboard-admin');
    }else{
        return view('index-1')->with('top3',$data)->with('top8',$top8)->with('seven',$seven);
    }
    
    
})->name('login');

Route::get('sitemap', function(){
    SitemapGenerator::create('https://kotakevents.com/')->writeToFile('sitemap.xml');
    return 'sitemap created';
});


Route::get('/key',function () {
    Cache::put('top3', array(1,2,3));
});
Route::get('/cari-event', 'DataTamuController@commonListCari');
Route::get('/cari','HomeController@cari')->name('cari');
Route::get('/galery/{id}','HomeController@galery')->name('galery');
Route::post('/galery/{id}','HomeController@galeryPost')->name('galery.post');
Route::post('/cari','HomeController@cariPost')->name('cari');
Route::get('/cari/{kategori}/{kunci}','HomeController@cariData')->name('cari.data');
Route::post('/verif-email','HomeController@verifEmail')->name('verif.email');
Route::get('/buat-acara','HomeController@buatAcaraForm')->name('buat.acara');
Route::get('/acara-saya','HomeController@acaraSaya')->name('acara.saya');
Route::get('/edit-acara/{id}','HomeController@editAcara')->name('edit.acara');
Route::post('/daftar-acara','HomeController@daftarAcara')->name('daftar.acara');
Route::post('/edit-acara-post','HomeController@editAcaraPost')->name('edit.acara.post');
// Route::get('/edit-acara',function () {
//     return redirect()->route('acara.saya');
// });
Route::get('/undangan-saya','HomeController@undanganSaya')->name('undangan.saya');
Route::get('/pengajuan/{id}','HomeController@pengajuanSaya')->name('daftar.pengajuan.saya');
Route::get('/pengajuan-konfirmasi/{id}','HomeController@pengajuanKonfirmasiSaya')->name('konfirmasi.pengajuan.saya');
Route::get('/pengajuan-tolak/{id}','HomeController@pengajuanTolakSaya')->name('tolak.pengajuan.saya');
Route::get('/nonaktfkan-acara/{id}','HomeController@nonaktifkanAcara')->name('nonaktifkan.acara');
Route::get('/aktifkan-acara/{id}','HomeController@aktifkanAcara')->name('aktifkan.acara');
Route::get('/acara/{id}','HomeController@detailAcara')->name('detail.acara');
Route::get('/listhadir/{id}','HomeController@listHadir')->name('list.hadir');
Route::post('/buat-acara','HomeController@buatAcara')->name('buat.acara');
Route::post('/login','Auth\LoginController@login')->name('login');
Route::post('/register','Auth\RegisterController@register')->name('register');
Route::post('/logout','Auth\LoginController@logout')->name('logout');
Route::get('/logout','Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard-admin','HomeController@adminDashboard')->name('dashboard.admin');
Route::get('/list-acara-pengajuan','HomeController@adminPengajuan')->name('pengajuan.admin');
Route::get('/kelola-event','HomeController@adminEvent')->name('admin.event');
Route::get('/konfirmasi-acara-pengajuan/{id}','HomeController@adminKonfirmasiPengajuan')->name('konfirmasi.pengajuan.admin');
Route::get('/tolak-acara-pengajuan/{id}','HomeController@adminTolakPengajuan')->name('tolak.pengajuan.admin');
Route::get('/list-acara-terkonfirmasi','HomeController@adminTerkonfirmasi')->name('terkonfirmasi.admin');
Route::get('/list-acara-ditolak','HomeController@adminDitolak')->name('ditolak.admin');
Route::get('/list-acara-selesai','HomeController@adminSelesai')->name('selesai.admin');
Route::get('/hapus-acara-selesai/{id}','HomeController@adminHapusSelesai')->name('hapus.selesai.admin');

Route::post('/uploadbukti','HomeController@uploadBukti')->name('upload.bukti');
Route::post('/kritik','HomeController@kritik')->name('kritik');



Route::get('/aktifkan','HomeController@aktifkan')->name('aktifkan');
Route::get('/nonaktifkan','HomeController@nonaktifkan')->name('nonaktifkan');

Route::get('/tpdf/{ev}/{us}/{sc}','HomeController@tpdf')->name('vdownload');
Route::post('/viewpdf','HomeController@pdf')->name('download');


Route::post('/downloadFormat', 'HomeController@getDownload');

Route::get('export', 'HomeController@export')->name('export');
Route::post('import', 'HomeController@import')->name('import');
Route::get('/emailpdftamu/{id}','HomeController@emailpdf')->name('email.tamu');
Route::get('/kirimtiket/{id}','HomeController@kirimtiket')->name('kirim.tiket');
Route::get('/pesertapdftamu/{id}','HomeController@pesertapdf')->name('download.peserta');
// Route::get('/pesertapdftamu/{id}','HomeController@pesertapdf')->name('download.peserta');




// Route::get('/datatamu/{id}','HomeController@datatamu')->name('data.tamu');

Route::post('/editdata','DataTamuController@datatamu');

Route::get('/petugas', 'DataTamuController@indexPetugas');
Route::get('/petugas-list', 'DataTamuController@commonListPetugas');
Route::get('/petugas-update', 'DataTamuController@actionSavePetugas');
Route::get('/petugas-delete', 'DataTamuController@actionDeletePetugas');


Route::get('/datatamu', 'DataTamuController@indexList')->name('data.tamu');
Route::get('/datatamu-list', 'DataTamuController@commonList');
Route::get('/datatamu-delete', 'DataTamuController@actionDelete');
Route::get('/datatamu2-delete', 'DataTamuController@actionDelete');
Route::get('/datatamu-update', 'DataTamuController@actionSave');
Route::get('/datatamu-add-kategori', 'DataTamuController@actionSaveKategori');
Route::get('/datatamu-kirim-tiket', 'DataTamuController@actionKirimTiket');


Route::get('/pengajuan/{id}','HomeController@pengajuanSaya')->name('daftar.pengajuan.saya');

Route::get('/datatamu2', 'DataTamuController@indexList2')->name('data.tamu2');
Route::get('/daftarhadir', 'DataTamuController@indexListHadir')->name('data.hadir');
Route::get('/datatamu-list2', 'DataTamuController@commonList2');
Route::get('/datatamu-list3', 'DataTamuController@commonList3');
Route::get('/daftarhadir-list', 'DataTamuController@commonListHadir');

Route::get('/event', 'DataTamuController@indexListEvent')->name('event');
Route::get('/event-list', 'DataTamuController@commonListEvent');
Route::get('/tiket-list', 'DataTamuController@commonListTiket');

Route::get('/event-tutup', 'DataTamuController@tutupEvent');
Route::get('/event-buka', 'DataTamuController@bukaEvent');



Route::get('/datatamu-delete2', 'DataTamuController@actionDelete2');
Route::get('/datatamu-konfirmasi', 'DataTamuController@konfirmasi');
Route::get('/datatamu-tolak', 'DataTamuController@tolak');


Route::get('/admin-list-event', 'DataTamuController@commonListAdmin');
Route::get('/admin-status-event', 'DataTamuController@commonListAdmin2');
Route::get('/admin-selesai-event', 'DataTamuController@commonListAdmin3');


Route::get('/admin-konfirmasi', 'DataTamuController@adminKonfirmasi');
Route::get('/admin-tolak', 'DataTamuController@adminTolak');
















