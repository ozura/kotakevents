<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('alamat');
            $table->string('email',100)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('kode_verif');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->dateTime('jadwal_mulai');
            $table->dateTime('jadwal_selesai');
            $table->text('deskripsi');
            $table->string('lokasi');
            $table->string('tipe');
            $table->string('foto');
            $table->string('pendaftaran')->default('Aktif');
            $table->string('status')->default('Belum Dikonfirmasi');
            $table->string('jenis_daftar');
            $table->string('kode_unik')->nullable();
            $table->string('bank')->nullable();
            $table->string('norek')->nullable();
            $table->string('atasnama')->nullable();
            $table->string('jumlah_bayar')->nullable();
            $table->unsignedBigInteger('id_user');
            $table->timestamps();

            $table->foreign('id_user')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
        Schema::create('galeries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('file');
            $table->string('tipe');
            $table->unsignedBigInteger('id_event');
            $table->timestamps();

            $table->foreign('id_event')
                ->references('id')->on('events')
                ->onDelete('cascade');
        });
        Schema::create('attendances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bukti_pembayaran')->nullable();
            $table->string('status');
            $table->integer('hadir');
            $table->unsignedBigInteger('id_event');
            $table->unsignedBigInteger('id_pembuat');
            $table->unsignedBigInteger('id_pendatang');
            $table->timestamps();

            $table->foreign('id_event')
                ->references('id')->on('events')
                ->onDelete('cascade');
            $table->foreign('id_pembuat')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('id_pendatang')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
        Schema::dropIfExists('galeries');
        Schema::dropIfExists('events');
        Schema::dropIfExists('users');
    }
}
