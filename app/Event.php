<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    protected $fillable = [
        'nama','jadwal_mulai','jadwal_selesai','deskripsi',
        'lokasi','tipe','jenis_daftar','kode_unik','bank',
        'norek','atasnama','jumlah_bayar','id_user','status','foto','pendaftaran','jenis_event'
    ];
    protected $dates = ['jadwal_mulai','jadwal_selesai'];
    
}
