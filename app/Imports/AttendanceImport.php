<?php

namespace App\Imports;
// use Illuminate\Validation\Rule;
// use Maatwebsite\Excel\Concerns\Importable;
// use Maatwebsite\Excel\Concerns\WithValidation;

use App\Attendance;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;



class AttendanceImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
  private $data; 

    public function __construct(array $data = [])
    {
        $this->data = $data; 

    }

    public function model(array $row)
    { 
        $data=$this->data;

        $d = $data['id_event'];
        // $id_scan = md5(sha1($row['pendatang'].$id));
        
             $x = Attendance::where('email_tamu',$row['email'])->where('id_event',$d)->first();
             
             // dd($row['email']);
             // dd($row);
             if (empty($x)) {
                    return new Attendance([
                        'status'=> 'Import Tamu',
                        'hadir'=>0,
                        'id_event'=>$d,
                        'id_pembuat'=>Auth::user()->id,
                        'id_pendatang'=>7,
                        'id_scan'=> md5(sha1($row['email'].$d)),
                        'nama_tamu'=>$row['nama'],
                        'email_tamu'=>$row['email'],
                        'alamat_tamu'=>$row['alamat'],
                        'kategori_tamu'=>$row['kategori'],
                        'id_kategori'=>1

                    ]);
                }  
        
       
    }

    // public function rules(): array
    // {
    //     return [
    //         'email' => Rule::unique('attendances', 'email_tamu'), // Table name, field in your db
    //     ];
    // }

    // public function customValidationMessages()
    // {
    //     return [
    //         'email_tamu.unique' => 'Custom message',
    //     ];
    // }

    // public function model(array $data)
    // {
    //     foreach($data as $row) {
    //        $data = Mahasiswa::find($row['nim']);
    //        if (empty($data)) {
    //           return new Mahasiswa([
    //                  'nim' => $row['nim'],
    //                  'slug' => str_slug($row['nim']),
    //                  ...
    //                  ]);
    //        } 
    //    }
    // }
}
