<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table='attendances';
    protected $fillable = [
        'bukti_pembayaran','status','hadir','id_event','id_pembuat','id_pendatang','waktu_hadir','id_scan','nama_tamu','email_tamu','alamat_tamu','id_kategori','tiket','kategori_tamu'
    ];
}
