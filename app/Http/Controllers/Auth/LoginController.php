<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;
use App\User;
use Hash;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo = '/home';
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // dd('masuk');
        $this->middleware('guest')->except('logout');
    }
    protected function sendFailedLoginResponse(Request $request)
    {
        // dd($request);
        
        Hash::make($request['password']);
        $model = User::where('email', $request->email)->first();
        if($model == null){
            Toastr::error('Login Gagal','LOGIN');
            return redirect()->back()->with('id', 11);    
        }

        // if (Hash::check($request->password, $model->password, [])) {}
        Toastr::error('Password Salah','LOGIN GAGAL');

             return redirect()->back()->with('id', 10);
        

        
        
       

        // return redirect()->route('login');
    // return redirect()->to('/')->with(['error_code', 5]); 
       
    }
}
