<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Helpers\GlobalFunction;
use App\Post;
// use App\Production;
use Yajra\Datatables\Datatables;
use App\Event;
use App\Attendance;
use App\Category;
use App\User;
use Auth;
use DB;
use Session;
use Hash;
use PDF;
use Illuminate\Support\Facades\Mail;

class DataTamuController extends Controller{
    
    public function indexList(Request $request){

        $id = $request->id;
        
        $event = Event::where('id',$id)->first();
        
        return view('data-tamu')->with('event',$event)->with('id_event',$id);
    }

    public function indexPetugas(Request $request){

        $id = $request->id;
        $event = Event::where('id',$id)->first();
        $petugas = User::where('id_registrasi',$id)->first();
        
        return view('petugas-registrasi')->with('petugas',$petugas)->with('id_event',$id)->with('event',$event);
    }

     public function indexList2(Request $request){
        $id = $request->id;

          $jumlah = Attendance::where('id_event',$id)->count();
        $jumlah1 = Attendance::where('id_event',$id)->where('status','Sudah Dikonfirmasi')->orWhere('status','Import Tamu')->where('id_event',$id)->count();
        $events = Attendance::join('users','attendances.id_pendatang','=','users.id')
            ->where('id_event',$id)
            ->select('attendances.id','name','email','alamat','status','bukti_pembayaran','attendances.nama_tamu','attendances.email_tamu')
            ->get();
            // dd($events);
            $pendatang = 7;
            $acara = Event::where('id',$id)->first();
        return view('pengajuan-pendaftar-1',compact('events','acara'))->with('jumlah',$jumlah)->with('jumlah1',$jumlah1)->with('id_event',$id)->with('pendatang',$pendatang);
    }

    public function indexListHadir(Request $request){
        $id = $request->id;

          
            // dd($events);
            $acara = Event::where('id',$id)->first();
        return view('daftar-hadir',compact('acara'));
    }
    public function indexListEvent(Request $request){

         $events = Event::where('id_user',Auth::user()->id)->orderBy('id','desc')->get();
        
        return view('acara-saya-1',compact('events'));
    }
    /* API */
    public function commonList(Request $request){

        $id_event = $request->id_event;
        
        $list_data = Attendance::join('users','attendances.id_pendatang','=','users.id')
        ->where('attendances.id_event',$id_event)
        ->where('status','Import Tamu')
        ->join('category','attendances.id_kategori','=','category.id')
        ->select('attendances.id','name','email','alamat','status','bukti_pembayaran','attendances.nama_tamu','attendances.email_tamu','attendances.alamat_tamu','attendances.kategori_tamu','attendances.id_kategori','category.nama_kategori','attendances.tiket')
        ->orderBy('attendances.id','DESC')
        ->get();

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->id,
                        'content' => $item,
                        'nama' =>$item->nama_tamu,
                        'tiket'=>$item->tiket
                    );

                    return $data;
                })            
                ->editColumn('nama_kategori', function($item){
                    if($item->id_kategori == 1){
                        return $item->kategori_tamu;
                        
                    }else{
                       return $item->nama_kategori;
                    }
                }) 
                ->make(true);
    }
     public function commonListPetugas(Request $request){

        $id_event = $request->id_event;
        
        
        $list_data = User::where('id_registrasi',$id_event)->get();

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->id,
                        'content' => $item,
                        'nama' =>$item->nama_tamu
                    );
                    return $data;
                })                
                ->make(true);
    }
     public function commonList2(Request $request){
        // dd('halo');
        $id_event = $request->id_acara;
        $list_data = Attendance::join('users','attendances.id_pendatang','=','users.id')
            ->join('category','attendances.id_kategori','=','category.id')
            ->where('attendances.id_event',$id_event)
            ->where('status','Belum Dikonfirmasi')
            ->orWhere('status','Menunggu Pembayaran')
            ->where('attendances.id_event',$id_event)
            ->select('attendances.id','name','email','alamat','status','bukti_pembayaran','attendances.nama_tamu','attendances.email_tamu','attendances.id_pendatang','category.nama_kategori')
            ->get();

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->id,
                        'content' => $item,
                        'status' =>$item->status,
                        'src' => url('fotoupload').'/'.$item->bukti_pembayaran,
                        
                    );
                    return $data;
                })
                ->editColumn('nama_tamu', function($item){
                    if($item->id_pendatang == 7){
                        return $item->nama_tamu;
                        
                    }else{
                       return $item->name;
                    }
                })
                ->editColumn('email_tamu', function($item){
                    if($item->id_pendatang == 7){
                        return $item->email_tamu;
                        
                    }else{
                       return $item->email;
                    }
                })

                ->make(true);
    }


     public function commonList3(Request $request){

        $id_event = $request->id_acara;

        $list_data = Attendance::join('users','attendances.id_pendatang','=','users.id')
            ->join('category','attendances.id_kategori','=','category.id')
            ->where('attendances.id_event',$id_event)                   
            ->where(function($q) {
                $q->where('status','Import Tamu')
              ->orWhere('status', 'Sudah Dikonfirmasi');
          })
            ->select('attendances.id','name','email','alamat','status','bukti_pembayaran','attendances.nama_tamu','attendances.email_tamu','attendances.id_pendatang','attendances.alamat_tamu','attendances.kategori_tamu','attendances.id_kategori','category.nama_kategori')
            ->orderBy('attendances.id','DESC')
            ->get();

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->id,
                        'content' => $item,
                        'status' =>$item->status
                        
                    );
                    return $data;
                })
                ->editColumn('nama_tamu', function($item){
                    if($item->id_pendatang == 7){
                        return $item->nama_tamu;
                        
                    }else{
                       return $item->name;
                    }
                })
                ->editColumn('email_tamu', function($item){
                    if($item->id_pendatang == 7){
                        return $item->email_tamu;
                        
                    }else{
                       return $item->email;
                    }
                })
                ->editColumn('alamat_tamu', function($item){
                    if($item->id_pendatang == 7){
                        return $item->alamat_tamu;
                        
                    }else{
                       return $item->alamat;
                    }
                })
                 ->editColumn('nama_kategori', function($item){
                    if($item->id_kategori == 1){
                        return $item->kategori_tamu;
                        
                    }else{
                       return $item->nama_kategori;
                    }
                }) 
                  ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->id,
                        'content' => $item,
                        'nama' =>$item->nama_tamu,
                        'id_pendatang' =>$item->id_pendatang,
                    );
                    return $data;
                })  


                ->make(true);
    }

    public function commonListAdmin(Request $request){
        
        $list_data = Event::join('users','events.id_user','=','users.id')
            ->where('status','Belum Dikonfirmasi')
            ->select('users.name','events.nama','events.jadwal_mulai','events.tipe','events.jenis_daftar','events.id')
            ->get();

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'content' => $item,  
                        'id'=>$item->id,     
                    );
                    return $data;
                })
                ->addColumn('id', function($item){
                    $data = array(
                        
                        'id'=>$item->id,                   
                    );
                    return $data;
                })         

                ->make(true);
    }

    public function commonListAdmin2(Request $request){

        $list_data = Event::join('users','events.id_user','=','users.id')        
        ->select('users.name','events.nama','events.jadwal_mulai','events.tipe','events.jenis_daftar','events.id','events.status')
        ->get();

        return Datatables::of($list_data)

        ->addColumn('action', function($item){
            $data = array(
                
                'id'=>$item->id,  
                'status'=>$item->status,
            );
            return $data;
        })
        ->addColumn('id', function($item){
            $data = array(

                'id'=>$item->id,                   
            );
            return $data;
        })         

        ->make(true);
    }

    public function commonListAdmin3(Request $request){

        $list_data = Event::join('users','events.id_user','=','users.id')        
            ->where('status','Sudah Dikonfirmasi')
            ->where('jadwal_selesai','<=',\Carbon\Carbon::now())
            ->select('users.name','events.nama','events.jadwal_mulai','events.jadwal_selesai','events.id')
            ->get();

        return Datatables::of($list_data)

        ->addColumn('action', function($item){
            $data = array(                
                'id'=>$item->id,  
                'status'=>$item->status,
            );
            return $data;
        })
        ->editColumn('jadwal_mulai', function($item){

            $jadwal_mulai = date("d M Y H:i",strtotime($item->jadwal_mulai));
            return $jadwal_mulai;


        })
        ->editColumn('jadwal_selesai', function($item){

            $jadwal_selesai = date("d M Y H:i",strtotime($item->jadwal_selesai));
            return $jadwal_selesai;


        })        

        ->make(true);
    }

    public function commonListHadir(Request $request){

        $id_event = $request->id_acara;

        $list_data = Attendance::join('users','attendances.id_pendatang','=','users.id')
            ->join('category','attendances.id_kategori','=','category.id')
            ->where('attendances.id_event',$id_event)
            ->where('attendances.hadir',1)                   
            ->where(function($q) {
                $q->where('status','Import Tamu')
              ->orWhere('status', 'Sudah Dikonfirmasi');
          })
            ->select('attendances.id','name','email','alamat','status','bukti_pembayaran','attendances.nama_tamu','attendances.email_tamu','attendances.id_pendatang','attendances.alamat_tamu','attendances.kategori_tamu','attendances.id_kategori','category.nama_kategori','attendances.waktu_hadir')
            ->get();

        return Datatables::of($list_data)
                
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->id,
                        'content' => $item,
                        'status' =>$item->status
                        
                    );
                    return $data;
                })
                ->editColumn('nama_tamu', function($item){
                    if($item->id_pendatang == 7){
                        return $item->nama_tamu;
                        
                    }else{
                       return $item->name;
                    }
                })
                ->editColumn('email_tamu', function($item){
                    if($item->id_pendatang == 7){
                        return $item->email_tamu;
                        
                    }else{
                       return $item->email;
                    }
                })
                ->editColumn('alamat_tamu', function($item){
                    if($item->id_pendatang == 7){
                        return $item->alamat_tamu;
                        
                    }else{
                       return $item->alamat;
                    }
                })
                ->editColumn('waktu_hadir', function($item){
                    
                    $waktu_hadir = date("H:i",strtotime($item->waktu_hadir));
                    return $waktu_hadir;
                    
                                        
                })
                ->editColumn('nama_kategori', function($item){
                    if($item->id_kategori == 1){
                        return $item->kategori_tamu;
                        
                    }else{
                       return $item->nama_kategori;
                    }
                }) 


                ->make(true);
    }


     public function commonListEvent(Request $request){

       // dd('halo');
        $list_data = Event::where('id_user',Auth::user()->id)->orderBy('id','desc')->get();

        return Datatables::of($list_data)
                ->addColumn('nama_event', function($item){
                    $data = array(
                        'nama_event' =>$item->nama,
                        
                    );
                    return $data;
                })      
                ->addColumn('image', function($item){
                    $data = array(
                        'src' => url('fotoupload').'/'.$item->foto,
                        'nama' => $item->nama,
                        'id'    =>$item->id,

                    );
                    return $data;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' =>$item->id,
                        'status' =>$item->status,
                        'pendaftaran' => $item->pendaftaran,  
                        'jenis_event' => $item->jenis_event       
                    );
                    return $data;
                })      
                ->addColumn('pendaftaran', function($item){
                    $data = array(        
                        'pendaftaran' => $item->pendaftaran,          
                        'id' => $item->id,      
                        'status' => $item->status,
                    );
                    return $data;
                })     
                ->make(true);
    }

    public function commonListTiket(Request $request){

       // dd('halo');
        $list_data = Attendance::join('events','attendances.id_event','=','events.id')
            ->where('id_pendatang',Auth::user()->id)
            ->select('attendances.id as id_kehadiran','events.foto',
                'events.jadwal_mulai','events.jadwal_selesai','events.lokasi','attendances.status',
                'events.nama','id_event','jenis_daftar','attendances.id_scan')            
            ->get();

        return Datatables::of($list_data)
                ->addColumn('nama_event', function($item){
                    $data = array(
                        'nama_event' =>$item->nama,
                        
                    );
                    return $data;
                })      
                ->addColumn('image', function($item){
                    $data = array(
                        'src' => url('fotoupload').'/'.$item->foto,
                        'nama' => $item->nama,
                        'id'    =>$item->id_event,

                    );
                    return $data;
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' =>$item->id_event,
                        'status' =>$item->status,
                        'jadwal_mulai' =>date("d-M-Y",strtotime($item->jadwal_mulai)),
                        'waktu' =>date("H:i",strtotime($item->jadwal_mulai)),
                        'lokasi' =>$item->lokasi,
                        'id_event'=>$item->id_event,
                        'id_scan'=>$item->id_scan,
                    );
                    return $data;
                })                      
                ->make(true);
    }
         public function commonListCari(Request $request){
            \Carbon\Carbon::setLocale('id');
       // dd('halo');
        $list_data = Event::where('tipe','public')->where('status','Sudah Dikonfirmasi')->where('jenis_event','open')->get();

        return Datatables::of($list_data)
                ->addColumn('gambar', function($item){
                    $data = array(
                        'nama_event' =>$item->nama,
                        'hari' =>$item->jadwal_mulai->format('d'),
                        'bulan' =>date("M",strtotime($item->jadwal_mulai)),
                        'src' => url('fotoupload').'/'.$item->foto,
                        
                    );
                    return $data;
                })                      
                ->addColumn('action', function($item){
                    
                    $data = array(
                        'id' =>$item->id,
                        'status' =>$item->status,
                        'lokasi' =>$item->lokasi,
                        'waktu' =>date("H:i",strtotime($item->jadwal_mulai)),
                        'deskripsi' =>$item->deskripsi,
                        'jenis_daftar' =>$item->jenis_daftar,
                        'nama' => $item->nama,
                    );
                    return $data;
                })                                      
                ->make(true);
    }
    public function konfirmasi(Request $request){
        $input = (object) $request->input();
        $item = Attendance::find($input->id);
        Attendance::where('id',$input->id)->update([
                'status'=>'Sudah Dikonfirmasi'                
            ]);
        $redirect = false;
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Konfirmasi Berhasil' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
    public function adminKonfirmasi(Request $request){
        $input = (object) $request->input();
        $item = Event::find($input->id);
        Event::where('id',$input->id)->update([
                'status'=>'Sudah Dikonfirmasi'                
            ]);
        $redirect = false;
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Konfirmasi Berhasil' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
      public function adminTolak(Request $request){
        $input = (object) $request->input();
        $item = Event::find($input->id);
        Event::where('id',$input->id)->update([
                'status'=>'Ditolak'                
            ]);
        $redirect = false;
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Konfirmasi Ditolak' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
     public function tolak(Request $request){
        $input = (object) $request->input();
        $item = Attendance::find($input->id);
        Attendance::where('id',$input->id)->update([
                'status'=>'Ditolak'                
            ]);
        $redirect = false;
        
        if($item->save()){
            return ['status' => 200, 'message' => 'Konfirmasi Ditolak' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
    public function tutupEvent(Request $request){
        $input = (object) $request->input();
        $item = Event::find($input->id);
        Event::where('id',$input->id)->update([
                'pendaftaran'=>'Non Aktif'                
            ]);
        $redirect = false;
        
        if($item->save()){
            return ['status' => 200, 'message' => 'PENDAFTARAN DITUTUP' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }

    public function bukaEvent(Request $request){
        $input = (object) $request->input();
        $item = Event::find($input->id);
        Event::where('id',$input->id)->update([
                'pendaftaran'=>'Aktif'                
            ]);
        $redirect = false;
        
        if($item->save()){
            return ['status' => 200, 'message' => 'PENDAFTARAN DIBUKA' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Operation error'];
        }
    }
    public function actionSave(Request $request){
        $input = (object) $request->input();

        $x = Attendance::where('email_tamu',$input->email_tamu)->where('id_event',$input->id_event)->first();
        
        if(empty($x)){

            $item=1;
            if(!empty($input->kategori)){
                $kategori = $input->kategori;
            }else{
                $kategori = 2;
            }
            Attendance::create([
                        'status'=> 'Import Tamu',
                        'hadir'=>0,
                        'id_event'=>$input->id_event,
                        'id_pembuat'=>Auth::user()->id,
                        'id_pendatang'=>7,
                        'id_scan'=> md5(sha1($input->email_tamu.$input->id_event)),
                        'nama_tamu'=>$input->nama_tamu,
                        'email_tamu'=>$input->email_tamu,
                        'alamat_tamu'=>$input->alamat_tamu,
                        'id_kategori'=>$kategori
            ]);

            $redirect = false;
        }elseif($item = Attendance::find($input->data_id))
        {                Attendance::where('id',$input->data_id)->update([
                'nama_tamu'=>$input->nama_tamu,
                'email_tamu'=>$input->email_tamu,
                'alamat_tamu'=>$input->alamat_tamu,
                'id_kategori'=>$input->kategori,                   
            ]);
                $item=1;
                $redirect = false;
            }else{
                $item=0;
            }
        
        
        
        
        
        
        if($item==1){
            return ['status' => 200, 'message' => 'Data berhasil disimpan!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Email Sudah Terdaftar'];
        }
    }

    public function actionSavePetugas(Request $request){
        $input = (object) $request->input();

        $x = User::where('email',$input->email)->where('id_registrasi',$input->id_event)->first();
        
        if(empty($x)){

            $item=1;
            
            User::create([
                        'name'=>$input->nama,
                        'alamat'=>$input->alamat,
                        'email'=>$input->email,
                        'id_registrasi'=>$input->id_event,                
                        'password'=>Hash::make($input->password),
                        
            ]);

            $redirect = false;
        }elseif($item = User::find($input->data_id))
        {                User::where('id',$input->data_id)->update([
                        'name'=>$input->nama,
                        'alamat'=>$input->alamat,
                        'email'=>$input->email,
                        'id_registrasi'=>$input->id_event,                
                        'password'=>Hash::make($input->password),
            ]);
                $item=1;
                $redirect = false;
            }else{
                $item=0;
            }
        
        
        
        
        
        
        if($item==1){
            return ['status' => 200, 'message' => 'Data berhasil disimpan!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Email Sudah Terdaftar'];
        }
    }
     public function actionSaveKategori(Request $request){
        $input = (object) $request->input();

        
        Category::create([
                'nama_kategori'=>$input->nama_kategori,
                'id_event'=>$input->id_event                                  
            ]);
                $item=1;
                $redirect = false;
        
        
        
        
        
        
        if($item==1){
            return ['status' => 200, 'message' => 'Kategori Berhasil Ditambahkan!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Eror'];
        }
    }

      public function actionKirimTiket(Request $request){
        $input = (object) $request->input();

        
        $event = Attendance::where('attendances.id',$input->id)
            ->join('events','attendances.id_event','=','events.id')
            ->where('attendances.status','Import Tamu')            
            ->orderBy('nama_tamu','ASC')
            ->get()
            ->toArray();
            // dd($event);
        $idevent = $event[0]['id_event'];
        // dd($idevent);
            // dd($event[1]['email_tamu']);
            
            

        $event2 = Event::where('id',$idevent)->first();

        for($i=0; $i<count($event);$i++){
        
        $data['nama_event'] = $event2->nama;
        
        $data['nama_user'] = $event[$i]['nama_tamu'];
        $data['id_scan'] = $event[$i]['id_scan'];
        $data['email_tamu'] = $event[$i]['email_tamu'];
        $data['jadwal_mulai'] = $event2->jadwal_mulai;
        $data['jadwal_selesai'] = $event2->jadwal_selesai;
        $data['lokasi'] = $event2->lokasi;
        $data['foto'] = $event2->foto;
        // dd($data); 

         
        $customPaper = array(0,0,327.00,263.80);            
        $pdf = PDF::loadView('download',compact('data'))->setPaper($customPaper, 'landscape');

        // Mail::to($user->email)->send()->attachData($pdf->output(), 'download.pdf');
        // $info = event::find($ev);

        
        Mail::send('simple', compact('data','i'), function($message)use($data,$i,$pdf) {       
            // dd($data);
        $message->to($data['email_tamu'],'Adeb')->subject('Undangan');

        $message->from('kotakevents@gmail.com','kotakevents');

        $message->attachData($pdf->output(), 'undangan.pdf');

    });
        }

         Attendance::where('id',$input->id)->update([
                        'tiket'=>'Terkirim',
            ]);
                $item=1;
                $redirect = false;
        
        
        
        
        
        
        if($item==1){
            return ['status' => 200, 'message' => 'Tiket Berhasil Dikirim!' , 'redirect' => $redirect];
        }else{
            return ['status' => 201, 'message' => 'Eror'];
        }
    }


    
     public function actionDelete(Request $request){
        $input = (object) $request->input();
        // dd($input);
        if(!empty($input->id)){
            if($item = Attendance::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Data berhasil dihapus'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }

    public function actionDeletePetugas(Request $request){
        $input = (object) $request->input();
        // dd($input);
        if(!empty($input->id)){
            if($item = User::find($input->id)){
                $item->delete();
                return ['status' => 200, 'message' => 'Data berhasil dihapus'];
            }
        }
        return ['status' => 201, 'message' => 'Operation error'];

    }
}