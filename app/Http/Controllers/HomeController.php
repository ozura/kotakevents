<?php

namespace App\Http\Controllers;

use App\Galery;
use App\Mail\KonfirmasiEmail;
use App\Mail\KritikEmail;
use App\Mail\TolakEmail;
use Illuminate\Http\Request;
// use Flash;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Event;
use App\Category;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use App\Attendance;
use Kamaln7\Toastr\Facades\Toastr;
use PDF;
use Illuminate\Support\Facades\Crypt;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\AttendanceImport;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $event,$attendance;
    public function __construct(Event $event,Attendance $attendance)
    {
        $this->middleware('auth')->except(['detailAcara','cariPost','cari','cariData']);
        $this->event = $event;
        $this->attendance = $attendance;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function checkUser(){
        if(Auth::user()->email!='adebwahyu@gmail.com'){
            return true;
        }
        return false;
    }
    public function index()
    {
        if(Auth::user()->email=='adebwahyu@gmail.com'){
                return redirect()->route('dashboard.admin');
            }
        elseif(Auth::user()){
             Toastr::success('Login Berhasil','LOGIN');
             
            // notify()->success("Success notification test","Success","topRight");
            return redirect()->back();
            }
            else{
             return redirect()->route('dashboard.admin');
            }
            // flash()->success('Success!', 'You logged in');

             



        
    }
    public function verifEmail(Request $request){
        $data = User::where('id',Auth::user()->id)->first();
        if($request['kode_verif']===$data['kode_verif']){
            User::where('id',Auth::user()->id)->update([
                'email_verified_at'=>\Carbon\Carbon::now(),
                'kode_verif'=>''
            ]);
            return redirect()->route('home');
        }
        else
            return redirect()->back()->with('message','Kode Verifikasi Salah');
    }
    public function buatAcaraForm(){
        if(!$this->checkUser()){
            return redirect()->route('home');
        }
        return view('buat-acara-1');
    }
    public function buatAcara(Request $request){
        if(!$this->checkUser()){
            return redirect()->route('home');
        }
        
        
        // dd($request);
        // dd($request['jadwal_mulai']);
        $image = $request->file('foto');
        $name = time().'.'.$image->getClientOriginalExtension();
        // $image->storeAs('/gambar/.', $name);
        // Event::make($image)->resize(300, 300)->save( public_path('/fotoupload/' . $name ));
        // $filename = time().'.'.request()->img->getClientOriginalExtension();
        $image->move(public_path('fotoupload'), $name);
        // dd($request);
        if($request->jenis_event ==  'close'){
        $data = $this->event->create([
            'nama' => $request['nama'],
            'jadwal_mulai'=> $request['jadwal_mulai'],
            'jadwal_selesai'=> $request['jadwal_selesai'],
            'deskripsi'=> $request['deskripsi'],
            'lokasi'=> $request['lokasi'],                        
            'foto'=>$name,
            'jumlah_bayar'=> $request['jumlah_bayar'],
            'id_user'=> Auth::user()->id,
            'jenis_event'=>$request['jenis_event']
        ]);
        }else{
        $data = $this->event->create([
            'nama' => $request['nama'],
            'jadwal_mulai'=> $request['jadwal_mulai'],
            'jadwal_selesai'=> $request['jadwal_selesai'],
            'deskripsi'=> $request['deskripsi'],
            'lokasi'=> $request['lokasi'],
            'tipe'=> $request['tipe'],
            'jenis_daftar'=> $request['jenis_daftar'],
            'kode_unik'=> $request['kode_unik'],
            'bank'=> $request['bank'],
            'norek'=> $request['norek'],
            'atasnama'=> $request['atasnama'],
            'foto'=>$name,
            'jumlah_bayar'=> $request['jumlah_bayar'],
            'id_user'=> Auth::user()->id,
            'jenis_event'=>$request['jenis_event']
        ]);
        }

        $e = Event::where('id_user',Auth::user()->id)->orderBy('created_at', 'desc')->first();
        $r = $e->id;

        $kategori =$request->name;
        $x =count($request->name);
        
        if(!empty($kategori)){
        for ($i=0; $i <$x ; $i++) { 
            Category::create([
            'nama_kategori' => $kategori[$i],
            'id_event'=> $r,
            
        ]);    
        }
        }
        return redirect()->route('detail.acara',['id'=>$data->id]);
    }

     public function uploadBukti(Request $request){
        if(!$this->checkUser()){
            return redirect()->route('home');
        }
        
        $id = $request->idA;
        
        $image = $request->file('bukti');
        
        $name = time().'.'.$image->getClientOriginalExtension();
        // $image->storeAs('/gambar/.', $name);
        // Event::make($image)->resize(300, 300)->save( public_path('/fotoupload/' . $name ));
        // $filename = time().'.'.request()->img->getClientOriginalExtension();
        $image->move(public_path('fotoupload'), $name);
        Attendance::where('id',$id)->update([
            'bukti_pembayaran'=>$name,
            'status'=>'Belum Dikonfirmasi',
        ]);
        // dd('halo');
        Toastr::success('Upload Bukti Pembayaran Berhasil','UPLOAD');
        return redirect()->back();
    }
    public function detailAcara($id){

        $attendance=null;
        $detail = Event::find($id);
        $sekarang = \Carbon\Carbon::now();
        if($detail->jadwal_selesai<=$sekarang){
            $pesan= 'selesai';
        }else{
            $pesan='ada';
        }
        // dd($pesan);

        $jumlah = Attendance::where('id_event',$id)->count();
        $jumlah1 = Attendance::where('id_event',$id)->where('status','Sudah Dikonfirmasi')->orWhere('status','Import Tamu')->where('id_event',$id)->count();
        // dd($jumlah1);
        $pemilik = false;
        $galery = null;
        $jumlah_galeri = Galery::where('id_event',$id)->count();
        if($jumlah_galeri>0){
            $galery = Galery::where('id_event',$id)->get();
        }
        if(Auth::check()){
            if($detail->id_user==Auth::user()->id){
                $pemilik = 'pemilik';
            }else{
                $attendance = $this->attendance->where('id_pendatang',Auth::user()->id)->where('id_event',$id)->count();
                if($attendance>0){
                    $attendance = $this->attendance->where('id_pendatang',Auth::user()->id)->where('id_event',$id)->first();
                    $pemilik = $attendance->status;
                }else{
                    $pemilik = 1;
                   
                }
            }
        } 
        // dd($detail->jenis_daftar);
        return view('detail-1')->with('detail',$detail)->with('pemilik',$pemilik)->with('jumlah',$jumlah)->with('jumlah1',$jumlah1)->with('galery',$galery)->with('attendance',$attendance)->with('pesan',$pesan);
    }
    public function cari(){
        // $detail = Event::where('tipe','public')->where('status','Sudah Dikonfirmasi')->orderBy('id','desc')->paginate(3);
        // $jumlah = 0.5;        
        // return view('cari2')->with('cari',$detail)->with('jumlah',$jumlah);


        return view('cari2');
    }
    public function cariPost(Request $request){

        return redirect()->route('cari.data',['kategori'=>$request['kategori'],'kunci'=>$request['kunci']]);
    }
    public function cariData($kategori,$kunci){
        
        if($kategori=='public')
            $detail = Event::when($kunci, function ($query) use ($kunci) {
                $query->where('nama', 'like', "%{$kunci}%")->Where('tipe', 'public')->Where('status','Sudah Dikonfirmasi');
            })->paginate(10);
        else
            $detail = Event::when($kunci, function ($query) use ($kunci) {
                $query->Where('kode_unik',$kunci )->Where('tipe', 'private')->Where('status','Sudah Dikonfirmasi');
            })->paginate(10);

        $jumlah=count($detail);
        
        return view('cari-1')->with('cari',$detail)->with('jumlah',$jumlah);
    }
    public function acaraSaya(){
        if(!$this->checkUser()){
            return redirect()->route('home');
        }
        $events = $this->event->where('id_user',Auth::user()->id)->orderBy('id','desc')->get();
        // dd($events);
        return view('acara-saya-1',compact('events'));

    }

    public function nonaktifkanAcara($id){
        $events = $this->event->find($id)->update([
            'pendaftaran'=>'Nonaktif'
        ]);
        return redirect()->back();
        

    }
       public function aktifkanAcara($id){
        // dd($id);
        $events = $this->event->find($id)->update([
            'pendaftaran'=>'Aktif'
        ]);
        return redirect()->back();
        
    }
    


    public function aktifkan(Request $request){
        // dd($id);
        
        $id_event = Request::get('id_event');
        $events = $this->event->find($id_event)->update([
            'pendaftaran'=>'Aktif'
        ]);
        return 200;
        
    }

    public function nonaktifkan(Request $request){
        // dd($id);
        $id_event = Request::get('id_event');
        $events = $this->event->find($id_event)->update([
            'pendaftaran'=>'Aktif'
        ]);
        return 200;
        
    }













    public function editAcara($id){
        if(!$this->checkUser()){
            return redirect()->route('home');
        }
        // dd($id);
        $detail = Event::find($id);
        // dd($detail->jenis_event);
        return view('edit-acara-1')->with('detail',$detail);
    }
    public function editAcaraPost(Request $request){
        // dd($request);
           if($request->jenis_event ==  'close'){
          $this->event->find($request['id'])->update([
            'nama' => $request['name'],
            'jadwal_mulai'=> $request['jadwal_mulai'],
            'jadwal_selesai'=> $request['jadwal_selesai'],
            'deskripsi'=> $request['deskripsi'],
            'lokasi'=> $request['lokasi'],                                    
            'jumlah_bayar'=> $request['jumlah_bayar'],            
            'jenis_event'=>$request['jenis_event']
        ]);
        }else{
        $this->event->find($request['id'])->update([
            'nama' => $request['name'],
            'jadwal_mulai'=> $request['jadwal_mulai'],
            'jadwal_selesai'=> $request['jadwal_selesai'],
            'deskripsi'=> $request['deskripsi'],
            'lokasi'=> $request['lokasi'],
            'tipe'=> $request['tipe'],
            'jenis_daftar'=> $request['jenis_daftar'],
            'kode_unik'=> $request['kode_unik'],
            'bank'=> $request['bank'],
            'norek'=> $request['norek'],
            'atasnama'=> $request['atasnama'],
            'jumlah_bayar'=> $request['jumlah_bayar'],
            'jenis_event'=>$request['jenis_event']
        ]);
        }
      

       
        return redirect()->back();
    }
    public function undanganSaya(){
        if(!$this->checkUser()){
            return redirect()->route('home');
        }
        $events = $this->attendance
            ->join('events','attendances.id_event','=','events.id')
            ->where('id_pendatang',Auth::user()->id)
            ->select('attendances.id as id_kehadiran','events.foto',
                'events.jadwal_mulai','events.jadwal_selesai','events.lokasi','attendances.status',
                'events.nama','id_event','jenis_daftar','attendances.id_scan')            
            ->get();
            // dd($events);
        return view('undangan-saya-1')->with('events',$events);
    }
    public function pengajuanSaya($id){
        if(!$this->checkUser()){
            return redirect()->route('home');
        }
        $jumlah = Attendance::where('id_event',$id)->count();
        $jumlah1 = Attendance::where('id_event',$id)->where('status','Sudah Dikonfirmasi')->orWhere('status','Import Tamu')->where('id_event',$id)->count();
        $events = $this->attendance
            ->join('users','attendances.id_pendatang','=','users.id')
            ->where('id_event',$id)
            ->select('attendances.id','name','email','alamat','status','bukti_pembayaran','attendances.nama_tamu','attendances.email_tamu')
            ->get();
            // dd($events);
        
            $acara = Event::where('id',$id)->first();
        return view('pengajuan-pendaftar-1',compact('events','acara'))->with('jumlah',$jumlah)->with('jumlah1',$jumlah1);
    }
      public function listHadir($id){
        if(!$this->checkUser()){
            return redirect()->route('home');
        }
        $events = $this->attendance
            ->join('users','attendances.id_pendatang','=','users.id')
            ->where('id_event',$id)
            ->select('attendances.id','name','email','alamat','status','waktu_hadir','attendances.nama_tamu','attendances.email_tamu','attendances.alamat_tamu')
            ->get();
        $acara = Event::where('id',$id)->first();
        return view('list-hadir-1',compact('events','acara'));
    }
    public function pengajuanKonfirmasiSaya($id){
        $this->attendance->find($id)->update([
            'status'=>'Sudah Dikonfirmasi'
        ]);
        Toastr::success('konfirmasi berhasil','Konfirmasi');
        return response()->json('success');
        
    }
    public function kritik(Request $request){
        if(!$this->checkUser()){
            return redirect()->route('home');
        }
        
        $nama = $request->name;
        $mail = $request->email;
        $pesan = $request->message;
        // dd($mail);
        $objDemo = new \stdClass();
        $objDemo->pesan = $pesan;
        $objDemo->email = $mail;
        $objDemo->nama = $nama;
        Mail::to('adebwahyu@gmail.com')->send(new KritikEmail($objDemo));
        // dd('halo');
        Toastr::success('Message Berhasil Dikirim','Message');
        return redirect()->back();
    }
    public function pengajuanTolakSaya($id){
        if(!$this->checkUser()){
            return redirect()->route('home');
        }
        $attendance = $this->attendance->find($id);
        $event = $this->event->find($attendance->id_event);
        $user = User::find($attendance->id_pendatang);
        $objDemo = new \stdClass();
        $objDemo->foto = $event->foto;
        $hail = Mail::to($user->email)->send(new TolakEmail($objDemo));
        $this->attendance->find($id)->update([
            'status'=>'Ditolak'
        ]);
        return response()->json('sukses');
    }
    public function daftarAcara(Request $request){
        // dd($request);
        $kat= $request->kategori;
        if(!empty($kat)){
            $kategori = $kat;
        }else{
            $kategori = 1;
        }
        $id_scan = md5(sha1(Auth::user()->id.$request['id']));
        // dd($id_scan);
        // $nama = User::where('id',Auth::user()->id)->value('name');
        // $nama2 = Event::where('id',$request['id'])->value('nama');
        // $kat = Category::where('id',1)->value('nama_kategori');
        // dd($event);
        // $id_scan = Crypt::encryptString();
        // dd($id_scan);
        if(!$this->checkUser()){
            return redirect()->route('home');
        }
        $event = $this->event->find($request['id']);
        if($event->jenis_daftar=='bayar'){
            $data = $this->attendance->create([
                'status'=>'Menunggu Pembayaran',
                'hadir'=>0,
                'id_event'=>$request['id'],
                'id_pembuat'=>$event->id_user,
                'id_pendatang'=>Auth::user()->id,
                'id_scan'=> $id_scan,
                'id_kategori'=> $kategori
            ]);
        }elseif($event->jenis_daftar=='memilih'){
            $data = $this->attendance->create([
                'status'=>'Belum Dikonfirmasi',
                'hadir'=>0,
                'id_event'=>$request['id'],
                'id_pembuat'=>$event->id_user,
                'id_pendatang'=>Auth::user()->id,
                'id_scan'=> $id_scan,
                'id_kategori'=> $kategori
            ]);
        }else{
            $data = $this->attendance->create([
                'status'=>'Sudah Dikonfirmasi',
                'hadir'=>9,
                'id_event'=>$request['id'],
                'id_pembuat'=>$event->id_user,
                'id_pendatang'=>Auth::user()->id,
                'id_scan'=> $id_scan,
                'id_kategori'=> $kategori
            ]);

            //             $id = $data->id;
            // $attendance = $this->attendance->find($id);
            // // dd($attendance);
            // // dd('halo');

            // $event = $this->event->find($attendance->id_event);
            // $user = User::find($attendance->id_pendatang);
            // $objDemo = new \stdClass();
            // $objDemo->foto = $event->foto;
            // $objDemo->name = $user->name;
            // $objDemo->id = $event->id;
            // $objDemo->attendance = $attendance->id;
            // $objDemo->id_scan = $attendance->id_scan;
            // Mail::to($user->email)->send(new KonfirmasiEmail($objDemo));
            // $this->attendance->find($id)->update([
            //     'status'=>'Sudah Dikonfirmasi'
            // ]);
        }
        // return redirect()->route('undangan.saya');
        return redirect()->back();
        Toastr::success('pengajuan pendaftaran telah dikirim','DAFTAR');
        
    }
    public function galery($id){
        $detail= Event::where('id',$id)->first();
        return view('galery-1')->with('id',$id)->with('detail',$detail);
    }
    public function galeryPost($id,Request $request){
        $image = $request->file('file');
        $name = time().'.'.$image->getClientOriginalExtension();
        // $image->storeAs('public', $name);
        $image->move(public_path('fotoupload'), $name);
        Galery::create([
            'file'=>$name,
            'tipe'=>'gambar',
            'id_event'=>$id
        ]);
        return response()->json('sukses',200);
    }


    public function adminDashboard(){
        if($this->checkUser()){
            return redirect()->route('acara.saya');
        }
        $data1 = $this->event
            ->where('status','Belum Dikonfirmasi')
            ->count();
        $data2 = $this->event
            ->where('status','Sudah Dikonfirmasi')
            ->count();
        $data3 = $this->event
            ->where('status','Ditolak')
            ->count();
        return view('admin-dashboard-1')->with('data',array($data1,$data2,$data3));
    }
    public function adminPengajuan(){
        if($this->checkUser()){
            return redirect()->route('acara.saya');
        }
        $data = $this->event
            ->join('users','events.id_user','=','users.id')
            ->where('status','Belum Dikonfirmasi')
            ->select('users.name','events.nama','events.jadwal_mulai','events.tipe','events.jenis_daftar','events.id')
            ->get();
        return view('admin-pengajuan-1')->with('data',$data);
    }
    public function adminEvent(){
        if($this->checkUser()){
            return redirect()->route('acara.saya');
        }
        $data = $this->event
            ->join('users','events.id_user','=','users.id')
            ->where('status','Belum Dikonfirmasi')
            ->select('users.name','events.nama','events.jadwal_mulai','events.tipe','events.jenis_daftar','events.id')
            ->get();
        return view('kelola-event')->with('data',$data);
    }
    public function adminTerkonfirmasi(){
        if($this->checkUser()){
            return redirect()->route('acara.saya');
        }
        $data = $this->event
            ->where('status','Sudah Dikonfirmasi')
            ->where('jadwal_mulai','>=',\Carbon\Carbon::now())
            ->get();
        $hasil = null;
        foreach ($data as $key=>$subdata){
            $jumlah1 = $this->attendance->where('id_event',$subdata->id)->count();
            $jumlah2 = $this->attendance->where('id_event',$subdata->id)->where('status','Sudah Dikonfirmasi')->count();
            
            $pembuat = user::where('id', $subdata->id_user)->value('name');
            // dd($pembuat);
            $hasil[$key] = array('nama'=>$subdata->nama,'jumlah_pendaftar'=>$jumlah1,'jumlah_terkonfirmasi'=>$jumlah2,'pembuat'=>$pembuat);
        }
        return view('admin-terkonfirmasi-1')->with('hasil',$hasil);
    }
    public function adminDitolak(){
        if($this->checkUser()){
            return redirect()->route('acara.saya');
        }
        $data = $this->event
            ->join('users','events.id_user','=','users.id')
            ->where('status','Ditolak')
            ->select('users.name','events.nama')
            ->get();
            // dd($data);
        return view('admin-ditolak-1')->with('events',$data);
    }
    public function adminSelesai(){
        if($this->checkUser()){
            return redirect()->route('acara.saya');
        }
        $data = $this->event
            ->where('status','Sudah Dikonfirmasi')
            ->where('jadwal_selesai','<=',\Carbon\Carbon::now())
            ->get();
            // dd($data);
        $hasil = null;
        foreach ($data as $key=>$subdata){
            $jumlah1 = $this->attendance->where('id_event',$subdata->id)->count();
            $jumlah2 = $this->attendance->where('id_event',$subdata->id)->where('status','Sudah Dikonfirmasi')->count();
            $jumlah3 = $this->attendance->where('id_event',$subdata->id)->where('status','Sudah Dikonfirmasi')->where('hadir',1)->count();
            $hasil[$key] = array('nama'=>$subdata->nama,'jumlah_pendaftar'=>$jumlah1,'jumlah_terkonfirmasi'=>$jumlah2,'jumlah_hadir'=>$jumlah3,'id'=>$subdata->id);
        }
        // dd($hasil);
        return view('admin-selesai-1')->with('hasil',$hasil);
    }
    public function adminHapusSelesai($id){
        $this->event->find($id)->delete();
        return response()->json('success');
    }
    public function adminKonfirmasiPengajuan($id){
        $this->event->find($id)->update([
            'status' => 'Sudah Dikonfirmasi'
        ]);
        Toastr::success('konfirmasi berhasil','Konfirmasi');
        return response()->json('success');
    }
    public function adminTolakPengajuan($id){
        $this->event->find($id)->update([
            'status'=>'Ditolak'
        ]);
        return response()->json('success');
    }

    // public function tpdf(){
    //     if(!$this->checkUser()){
    //         return redirect()->route('home');
    //     }
    //     $events = $this->attendance
    //         ->join('events','attendances.id_event','=','events.id')
    //         ->where('id_pendatang',Auth::user()->id)
    //         ->select('attendances.id as id_kehadiran','events.foto',
    //             'events.jadwal_mulai','events.jadwal_selesai','attendances.status',
    //             'events.nama','id_event','jenis_daftar')
    //         ->get();
    //     return view('viewpdf')->with('events',$events);
    // }
    public function tpdf($ev,$us,$sc){
        
        
        $event = Event::where('id',$ev)->first();
        $user = User::where('id',$us)->first();
        $id_scan = $sc;


        return view('download')->with('event',$event)->with('user',$user)->with('id_scan',$id_scan);
    }
    public function pdf(Request $request){
        $ev = $request->ev;
        $sc = $request->sc;
        $us = $request->us;
        $event = Event::where('id',$ev)->first();
        // $user = User::where('id',$us)->first();

            $user = Attendance::join('users','attendances.id_pendatang','=','users.id')
            ->where('id_scan',$sc)
            ->select('attendances.id_scan',
                'users.name')->first();


        $data['nama_event'] = $event->nama;
        $data['nama_user'] = $user->name;
        $data['id_scan'] = $user->id_scan;
        $data['jadwal_mulai'] = $event->jadwal_mulai;
        $data['jadwal_selesai'] = $event->jadwal_selesai;
        $data['lokasi'] = $event->lokasi;
        $data['foto'] = $event->foto;
        // dd($data);
        

        $customPaper = array(0,0,327.00,263.80);            
        $pdf = PDF::loadView('download',compact('data'))->setPaper($customPaper, 'landscape');

        // Mail::to($user->email)->send()->attachData($pdf->output(), 'download.pdf');
        // $info = event::find($ev);

        
    //     Mail::send('download', compact('data'), function($message)use($data,$pdf) {       

    //     $message->to('adibkuncoro8757@gmail.com','Adeb')->subject('Undangan');

    //     $message->from('kotakevents@gmail.com','kotakevents');

    //     $message->attachData($pdf->output(), 'filename.pdf');

    // });

        return $pdf->download('undangan.pdf');
    }

    public function datatamu(Request $request){
        // dd($id)_event;
        $id = $request->id_event;
        $event = Event::where('id',$id)->first();
        $events = $this->attendance
        ->join('users','attendances.id_pendatang','=','users.id')
        ->where('id_event',$id)
        ->where('status','Import Tamu')
        ->select('attendances.id','name','email','alamat','status','bukti_pembayaran','attendances.nama_tamu','attendances.email_tamu','attendances.alamat_tamu')
        ->get();
        return view('data-tamu')->with('event',$event)->with('id_event',$id)->with('events',$events);
    }
    public function getDownload(Request $request) {
        $file_name=$request->file_name;

        $file_path = public_path('fotoupload/'.$file_name);
        return response()->download($file_path);
    }


      public function importExportView()
    {
       return view('import');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function import(Request $request) 
    {
        $data = [
            'id_event' => $request->id_event, 
    
        ];

        Excel::import(new AttendanceImport($data),request()->file('file'));
           
        return back();
    }

        public function emailpdf($id){
        
        $event = $this->attendance
            ->join('events','attendances.id_event','=','events.id')
            ->where('attendances.status','Import Tamu')
            ->where('id_event',$id)
            ->orderBy('nama_tamu','ASC')
            ->get()
            ->toArray();


            // dd($event[1]['email_tamu']);
            
            

        $event2 = Event::where('id',$id)->first();

        for($i=0; $i<count($event);$i++){
        
        $data['nama_event'] = $event2->nama;
        
        $data['nama_user'] = $event[$i]['nama_tamu'];
        $data['id_scan'] = $event[$i]['id_scan'];
        $data['email_tamu'] = $event[$i]['email_tamu'];
        $data['jadwal_mulai'] = $event2->jadwal_mulai;
        $data['jadwal_selesai'] = $event2->jadwal_selesai;
        $data['lokasi'] = $event2->lokasi;
        $data['foto'] = $event2->foto;
        // dd($data); 

         
        $customPaper = array(0,0,327.00,263.80);            
        $pdf = PDF::loadView('download',compact('data'))->setPaper($customPaper, 'landscape');

        // Mail::to($user->email)->send()->attachData($pdf->output(), 'download.pdf');
        // $info = event::find($ev);

        
        Mail::send('simple', compact('data','i'), function($message)use($data,$i,$pdf) {       
            // dd($data);
        $message->to($data['email_tamu'],'Adeb')->subject('Undangan');

        $message->from('kotakevents@gmail.com','kotakevents');

        $message->attachData($pdf->output(), 'undangan.pdf');

    });
        }

        return redirect()->back()->with(['success' => 'Undangan Terkirim']);
      
    }

    public function kirimtiket($id){
        
        $event = $this->attendance
            ->where('attendances.id',$id)
            ->join('events','attendances.id_event','=','events.id')
            ->where('attendances.status','Import Tamu')            
            ->orderBy('nama_tamu','ASC')
            ->get()
            ->toArray();
            // dd($event);
        $idevent = $event[0]['id_event'];
        // dd($idevent);
            // dd($event[1]['email_tamu']);
            
            

        $event2 = Event::where('id',$idevent)->first();

        for($i=0; $i<count($event);$i++){
        
        $data['nama_event'] = $event2->nama;
        
        $data['nama_user'] = $event[$i]['nama_tamu'];
        $data['id_scan'] = $event[$i]['id_scan'];
        $data['email_tamu'] = $event[$i]['email_tamu'];
        $data['jadwal_mulai'] = $event2->jadwal_mulai;
        $data['jadwal_selesai'] = $event2->jadwal_selesai;
        $data['lokasi'] = $event2->lokasi;
        $data['foto'] = $event2->foto;
        // dd($data); 

         
        $customPaper = array(0,0,327.00,263.80);            
        $pdf = PDF::loadView('download',compact('data'))->setPaper($customPaper, 'landscape');

        // Mail::to($user->email)->send()->attachData($pdf->output(), 'download.pdf');
        // $info = event::find($ev);

        
        Mail::send('simple', compact('data','i'), function($message)use($data,$i,$pdf) {       
            // dd($data);
        $message->to($data['email_tamu'],'Adeb')->subject('Undangan');

        $message->from('kotakevents@gmail.com','kotakevents');

        $message->attachData($pdf->output(), 'undangan.pdf');

    });
        }

        return redirect()->back()->with(['success' => 'Undangan Terkirim']);
      
    }

     public function pesertapdf($id){
        $id_event = $id;
        $event = Event::where('id',$id)->first();

         $data = Attendance::join('users','attendances.id_pendatang','=','users.id')
            ->join('category','attendances.id_kategori','=','category.id')
            ->where('attendances.id_event',$id_event)         
            ->where(function($q) {
                $q->where('status','Import Tamu')
              ->orWhere('status', 'Sudah Dikonfirmasi');
          })
            ->select('attendances.id','name','email','alamat','status','bukti_pembayaran','attendances.nama_tamu','attendances.email_tamu','attendances.kategori_tamu','attendances.id_pendatang','attendances.alamat_tamu','attendances.id_kategori','nama_kategori')
            ->orderBy('nama_tamu','asc')
            ->get();
        // dd($data);
        
        $pdf = PDF::loadView('peserta_pdf',compact('data','event'))->setPaper('a4', 'potrait');
        // $pdf = PDF::loadview('peserta_pdf',['data'=>$data]);
        // return $pdf->stream();

        return $pdf->download('tamu_pdf.pdf');
    }

 
  
}
