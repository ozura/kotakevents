<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Event;
use Hash;
use Carbon\Carbon;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;
// use Tymon\JWTAuth\Contracts\JWTSubject;
use JWTAuth;
use Auth;

class AuthController extends Controller
{
    //
     public function login(Request $request)
    {
        return $this->guardWithValidation($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ], [], function() use ($request) {
            $user = User::where('email', $request->email)->whereNotNull('id_registrasi');

           
            
            if ($user->count() > 0) {
                $id_event = User::where('email', $request->email)->value('id_registrasi');
                $event = Event::where('id',$id_event)->first();
                $sekarang = \Carbon\Carbon::now();
                $mulai = $event->jadwal_mulai;
                $selesai = $event->jadwal_selesai;

                if($sekarang < $selesai && $sekarang > $mulai){
                $user = $user->first();
                if (Hash::check($request->password, $user->password)) {
                	// dd($user);
                    //$token = hash('sha256', $user->id.date('U').config('app.key'));
                    $credentials = $request->only(['email', 'password']);
                    $token = JWTAuth::attempt($credentials, $this->generateCustomClaims());
                    $tokenExpirationTimestamp = JWTAuth::setToken($token)->getPayload()->get('exp');
                    $tokenExpiration = Carbon::createFromTimestamp($tokenExpirationTimestamp)->toDateTimeString();
                    $user->token_login_mobile_kadaluarsa = $tokenExpiration;
                    $user->token_login_mobile = $token;
                    $user->save();
                    
                    
                    $result = [
                        'id'=>$user->id,
                        'token' => $token,
                        // 'full_name'=>$user->full_name,
                        'email'=>$user->email,
                        // 'username'=>$user->username,
                    ];

                    
                   
                    // return $this->success($result);
                    echo $result['token'];
                } else {
                    return 'gagal';
                }
            }else{
                return 'belum';
            }
            } 


            else {
                return 'gagal';
            }
        });
    }

    public function logout(Request $request)
    {
        // $user = $request->user();
        // $user->token_login_api = NULL;
        // $user->save();
        User::where('id',Auth::id())->update([
            'token_firebase'=>NULL,
            'token_login_mobile' => NULL,
            'token_login_mobile_kadaluarsa' => NULL,
        ]);
        // return $this->success('Logout sukses.');
        return $this->success('Logout sukses.');
    }
    protected function generateCustomClaims() {
        $expirationTimestamp = Carbon::now()->addYears(15)->timestamp;
        return array(
            'exp' => $expirationTimestamp
        );
    }
    public function changePassword(Request $request){
        $user=User::where('id',Auth::id())->first();
        if($request->old_password=='' || $request->old_password==NULL){
            return $this->failure('Password lama tidak boleh kosong.');
        }
        else{
            if (Hash::check($request->old_password, $user->password)){
                if($request->new_password=='' || $request->new_password==NULL){
                    return $this->failure('Password baru tidak boleh kosong.');
                }
                else{
                   
                    $user->password = $request->new_password;
                    $user->token_login_mobile_kadaluarsa = NULL;
                    $user->token_login_mobile = NULL;
                    $user->save();
                    return $this->success('Berhasil Ubah Password');
                }
            }
            else{
                return $this->failure('Password lama tidak sesuai.');
            }
        }
        
    }

}
