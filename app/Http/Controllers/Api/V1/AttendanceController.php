<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\Attendance;
use App\User;
use App\Category;
use Auth;
use Carbon\Carbon;

class AttendanceController extends Controller
{
    
	public function insert(Request $request){

		$nama=$request->nama;
		$email=$request->email;
        $alamat=$request->alamat;
        $token=$request->token;
        $id_event = User::where('token_login_mobile',$token)->value('id_registrasi');
        $id_user = User::where('token_login_mobile',$token)->value('id');
        $waktu_hadir = \Carbon\Carbon::now();
		 Attendance::create([
                        'status'=> 'Import Tamu',
                        'hadir'=>1,
                        'id_event'=>$id_event,
                        'id_pembuat'=>$id_user,
                        'id_pendatang'=>7,                                               
                        'id_kategori'=>1,
                        'nama_tamu'=>$nama,
                        'email_tamu'=>$email,
                        'alamat_tamu'=>$alamat,
                        'waktu_hadir'=> $waktu_hadir

            ]);

         $response['value'] = 1;
         $response['message'] = 'Data Berhasil Ditambahkan';
         echo json_encode($response);

		return $this->success('Berhasil Dibuat');
	}
    
	public function getListEvent(Request $request){
    	
    	$user = User::where('token_login_mobile',$request->token)->value('id_registrasi');

    	// $id = $user->id;

    	$event = Attendance::join('users','attendances.id_pendatang','=','users.id')
        ->where('attendances.id_event',$user)
        ->where('hadir',1)
        ->join('category','attendances.id_kategori','=','category.id')
        ->select('name','attendances.nama_tamu','category.nama_kategori','attendances.kategori_tamu','attendances.id_kategori','attendances.status')
        ->orderBy('attendances.id','DESC')
        ->get();

        
        $x = count($event);
        if($x != 0){
        for ($i=0; $i < $x ; $i++) { 
            if($event[$i]['status'] == 'Import Tamu'){
                $data[$i]['nama_tamu']= $event[$i]['nama_tamu'];
                if($event[$i]['id_kategori'] == 1 ){
                    $data[$i]['id_kategori']= $event[$i]['kategori_tamu'];
                }else{
                    $data[$i]['id_kategori']= $event[$i]['nama_kategori'];
                }
            }else{
                $data[$i]['nama_tamu']= $event[$i]['name'];
                if($event[$i]['id_kategori'] == 1 ){
                    $data[$i]['id_kategori']= $event[$i]['kategori_tamu'];
                }else{
                    $data[$i]['id_kategori']= $event[$i]['nama_kategori'];
                }
            }
        }

        $response['value'] = 1;
         $response['result'] = $data;
          echo json_encode($response);
    	// return $this->success($event);
        }

    }


  //   public function scanEvent(Request $request){
  //   	// dd($request);
  //   	$idAttendance=json_decode($request->id_attendance);
		// $waktuKehadiran=json_decode($request->waktu_kehadiran);
		// $token=($request->token);
		

		
		
		// for($i=0; $i<sizeof($idAttendance); $i++){
		// 		Attendance::where('id',$idAttendance[$i])->update([
		// 			'hadir'=>1,
		// 			'waktu_hadir'=> $waktuKehadiran[$i],
		// 		]);
		// }
		// return $this->success('Data Berhasil Diupdate');
		
  //   }

    public function scan(Request $request){

		$id_scan=$request->id_scan;
		$token = $request->token;

		$i = User::where('token_login_mobile',$token)->value('id_registrasi');
	

		// $id_scan=json_decode($request->id_scan);
		$id_event = Attendance::where('id_scan',$id_scan)->value('id_event');
		if($i == $id_event){
		$data['nama'] =' ';
		$data['name'] = ' ';
		$data['kategori'] = ' ';
		$flag = Attendance::where('id_scan',$id_scan)->value('waktu_hadir');
		$flag2 = date("H:i",strtotime($flag));
		$waktu_hadir = \Carbon\Carbon::now();

		$data = Attendance::join('users','attendances.id_pendatang','=','users.id')
            ->join('category','attendances.id_kategori','=','category.id')
            ->join('events','events.id','=','attendances.id_event')            
            ->where('attendances.id_scan',$id_scan)                               
            ->select('attendances.id','users.name','attendances.nama_tamu','attendances.email_tamu','attendances.id_pendatang','attendances.alamat_tamu','attendances.kategori_tamu','attendances.id_kategori','category.nama_kategori','attendances.waktu_hadir','events.nama')
            ->first();

         if (!empty($id_event) && empty($flag)) {
		        Attendance::where('id_scan',$id_scan)->update([
							'hadir'=>1,					
							'waktu_hadir'=> $waktu_hadir,
						]);

		        $hadir = Attendance::where('id_event',$i)->where('hadir',1)->get();
				$jhadir = count($hadir);

		         
		         $response['message'] = 'Hadir!';
		         $response['namaevent'] = $data['nama'];
                 if(!empty($data['nama_tamu'])){
                 $response['namapeserta'] = $data['nama_tamu'];
                 
                 }else{
		         $response['namapeserta'] = $data['name'];
                 
                 }

                 if($data['id_kategori'] == 1){
                    $response['kategori'] = $data['kategori_tamu'];
                }else{
                 $response['kategori'] = $data['nama_kategori'];   
                }
		         
		         $response['hadir'] = 'Jumlah Hadir : '.$jhadir;
		         echo json_encode($response);
         }elseif (!empty($flag)) {
         	$hadir = Attendance::where('id_event',$i)->where('hadir',1)->get();
			$jhadir = count($hadir);
         	$response['namaevent'] = $data['nama'];
		    if(!empty($data['nama_tamu'])){
                 $response['namapeserta'] = $data['nama_tamu'];
                 }else{
                 $response['namapeserta'] = $data['name'];
                 }
         	$response['kategori'] = 'Hadir pukul '.$flag2.' WIB';
         	$response['pesan'] = 'Gagal!';
         	$response['hadir'] = 'Jumlah Hadir : '.$jhadir;
         	echo json_encode($response);
         }

         }else{
         	$hadir = Attendance::where('id_event',$i)->where('hadir',1)->get();
			$jhadir = count($hadir);
         	$response['namapeserta'] = 'Data Tidak Ditemukan';
         	$response['pesan'] = 'Gagal!';
         	
         	echo json_encode($response);
         }
		

		
	}


	public function search(Request $request){
		$user = User::where('token_login_mobile',$request->token)->value('id_registrasi');

    	// $id = $user->id;
		$kunci = $request->search;
    	$event = Attendance::join('users','attendances.id_pendatang','=','users.id')
    	->join('category','attendances.id_kategori','=','category.id')
        ->where('attendances.id_event',$user)
        ->where('hadir',1)
        ->where('name', 'like', "%{$kunci}%")
        ->orWhere('nama_tamu', 'like', "%{$kunci}%")
        ->where('attendances.id_event',$user)
        ->where('hadir',1)
        ->select('name','attendances.nama_tamu','category.nama_kategori','attendances.kategori_tamu','attendances.id_kategori','attendances.status')
        ->orderBy('attendances.id','DESC')
        ->get();

        
        $x = count($event);
        if($x != 0){
          for ($i=0; $i < $x ; $i++) { 
            if($event[$i]['status'] == 'Import Tamu'){
                $data[$i]['nama_tamu']= $event[$i]['nama_tamu'];
                if($event[$i]['id_kategori'] == 1 ){
                    $data[$i]['id_kategori']= $event[$i]['kategori_tamu'];
                }else{
                    $data[$i]['id_kategori']= $event[$i]['nama_kategori'];
                }
            }else{
                $data[$i]['nama_tamu']= $event[$i]['name'];
                if($event[$i]['id_kategori'] == 1 ){
                    $data[$i]['id_kategori']= $event[$i]['kategori_tamu'];
                }else{
                    $data[$i]['id_kategori']= $event[$i]['nama_kategori'];
                }
            }
        }
        
        $response['value'] = 1;
         $response['result'] = $data;
          echo json_encode($response);
    	// return $this->success($event);
        }
	}

}
