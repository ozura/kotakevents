<?php

/*
 * This file is part of jwt-auth. Overrided for custom functionality.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Auth;

class GetHakAkses extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $allowedHakAkses = array_slice(func_get_args(), 2);

        $request->session()->regenerate();

        $temp           = $this->getHakAkses(Auth::user()->nip, true);
        
        $hakAkses       = $temp['hakAkses'];
        $mainHakAkses   = $temp['mainHakAkses'];

        $request->session()->flash('hak_akses', $hakAkses);
        $request->session()->flash('main_hak_akses', $mainHakAkses);

        if(!empty($allowedHakAkses))
        {
            foreach($allowedHakAkses as $item)
                if(in_array($item, session()->get('hak_akses') ))
                    return $next($request);
            
            return redirect()->route('forbidden');
        }
        else
            return $next($request);
    }
}